import createError from 'http-errors'
import express, {NextFunction, Request, Response} from 'express'
import router from "./v2/routes/index";
import cors from 'cors';

import * as dotenv from "dotenv";
import morganMiddleware from "./v2/middleware/morganMiddleware";
import Logger from "./v2/logger/logger";
import databaseConnect from "./v2/db/db";

dotenv.config();
const app = express();

app.use(cors())
app.use(morganMiddleware);
app.use(express.json());
app.use(express.urlencoded({extended: false}));

const port = process.env.PORT || 3001; // default port to listen

// app.use(auth);
app.get('/', (req, res, next) => {
    res.send({
        body: req.header('Authorization')
    });
});


// forward to APIs
app.use('/api/v2', router);

// catch 404 and forward to error handler
app.use((req: Request, res: Response, next: NextFunction) => {
    next(createError(404));
});


// error handler
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.json({
        message: err.message
    })
    Logger.error(err.message);
});

databaseConnect().then(() => {
    Logger.info('connected to db')
}).catch(err => {
    Logger.error(`connection to db failed: ${err}`)
})

// start the Express server
app.listen(port, () => {
    Logger.info(`server started at http://localhost:${port}`);
});
