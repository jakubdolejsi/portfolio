import {IController} from "../interfaces/controllers/IController";
import {getEmailFromRequest} from "./UserController";
import {Error, Success} from "../interfaces/response/response";
import {Performance, StatsService} from "../services/StatsService";
import {PortfolioService} from "../services/PortfolioService";
import {IPortfolioItem} from "../models/PortfolioModel";
import Logger from "../logger/logger";

interface Quote {
    ticker: string,
    weight: number
}


const GetAlStatsByUser: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)

    // const result = await StatsService.GetAllStats(email)
    const result = await StatsService.OverallStats(email)
    return Success(res, result)
}


const GetPerformance: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const portfolioModels = await StatsService.GetPerformance(email)
    const firstItem = portfolioModels[0].assets[0]
    const secondItem = portfolioModels[0].assets[1]
    const merged = await StatsService.ComputeAndMergePerformance(firstItem, secondItem)
    if (!merged) return Error(res, "Merging was not successful", 500)

    return Success(res, merged)
}

const GetPortfolioPerformance: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {portfolioName} = req.params

    const performance = await StatsService.GetPortfolioPerformance(email, portfolioName)
    if (!performance) return Error(res, `Portfolio with ${portfolioName} does not exists`, 404)
    return Success(res, performance)
}


const GetAssetPerformance: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {portfolioName, ticker} = req.params

    const portfolioModel = await PortfolioService.GetPortfolioByName(email, portfolioName)
    if (!portfolioModel) return Error(res, `Portfolio ${portfolioName} does not exists`, 500)

    const assetItem = await PortfolioService.GetAssetInPortfolio(portfolioModel, ticker)
    if (!assetItem) return Error(res, `Asset ${ticker} does not exist in ${portfolioModel.name}`, 500)

    const performance = await StatsService.GetAssetPerformance(assetItem)
    if (!performance) return Error(res, `Performance data not available`, 500)

    return Success(res, performance)
}

const GetAssetPerformanceWithoutCashInflow: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {portfolioName, ticker} = req.params

    const portfolioModel = await PortfolioService.GetPortfolioByName(email, portfolioName)
    if (!portfolioModel) return Error(res, `Portfolio ${portfolioName} does not exists`, 500)

    const assetItem = await PortfolioService.GetAssetInPortfolio(portfolioModel, ticker)
    if (!assetItem) return Error(res, `Asset ${ticker} does not exist in ${portfolioModel.name}`, 500)

    const performance = await StatsService.GetAssetPerformanceWithoutCashInflow(assetItem)
    if (!performance) return Error(res, `Performance data not available`, 500)

    return Success(res, performance)
}

const GetPortfolioSectorAllocation: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {portfolioName} = req.params

    const allocation = await StatsService.GetPortfolioSectorAllocation(email, portfolioName)
    if (!allocation) return Error(res, "Allocation cannot be assembled", 500)

    return Success(res, allocation)
}

const GetPortfolioCountryAllocation: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {portfolioName} = req.params

    const allocation = await StatsService.GetPortfolioCountryAllocation(email, portfolioName)
    if (!allocation) return Error(res, "Allocation cannot be assembled", 500)

    return Success(res, allocation)
}

const GetPortfolioIndustryAllocation: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {portfolioName} = req.params

    const allocation = await StatsService.GetPortfolioIndustryAllocation(email, portfolioName)
    if (!allocation) return Error(res, "Allocation cannot be assembled", 500)

    return Success(res, allocation)
}

const GetPortfolioAssetTypeAllocation: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {portfolioName} = req.params

    const allocation = await StatsService.GetPortfolioAssetTypeAllocation(email, portfolioName)
    if (!allocation) return Error(res, "Allocation cannot be assembled", 500)

    return Success(res, allocation)
}

const GetPortfolioAssetAllocation: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {portfolioName} = req.params

    const allocation = await StatsService.GetPortfolioAssetAllocation(email, portfolioName)
    if (!allocation) return Error(res, "Allocation cannot be assembled", 500)

    return Success(res, allocation)
}

const GetQuotesPerformance: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {portfolioName} = req.params
    const quotes: string[] = req.body.quotes

    const portfolio = await PortfolioService.GetPortfolioByName(email, portfolioName)
    const overallPerformance = await StatsService.MergeQuotesPerformance(portfolio, quotes)

    return Success(res, overallPerformance)
}

const GetQuotesPerformanceWithoutCashInflow: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {portfolioName} = req.params
    const quotes: string[] = req.body.quotes

    const portfolio = await PortfolioService.GetPortfolioByName(email, portfolioName)
    const overallPerformance = await StatsService.MergeQuotesPerformance(portfolio, quotes, true)

    return Success(res, overallPerformance)
}


const GetWeightedQuotesPerformanceWithoutCashInflow: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {portfolioName} = req.params
    const quotes: string[] = req.body.quotes
    const wQuotes: Quote[] = req.body.wQuotes

    return Success(res, wQuotes)

}


const CompareQuotes: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {portfolioName} = req.params
    const quotes: string[] = req.body.quotes

    const portfolio = await PortfolioService.GetPortfolioByName(email, portfolioName)
    const allPerformances = await StatsService.CompareQuotesPerformance(portfolio, quotes)

    return Success(res, allPerformances)
}

const CompareQuotesWithoutCashInflow: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {portfolioName} = req.params
    const quotes: string[] = req.body.quotes

    const portfolio = await PortfolioService.GetPortfolioByName(email, portfolioName)
    const allPerformances = await StatsService.CompareQuotesPerformance(portfolio, quotes, true)

    return Success(res, allPerformances)
}

const CompareQuotesWithBenchmark: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {portfolioName} = req.params
    const quotes: string[] = req.body.quotes
    const benchmark: string = req.body.benchmark

    if (!benchmark || quotes.length === 0) return Error(res, "Please fill benchmark", 400)

    const portfolio = await PortfolioService.GetPortfolioByName(email, portfolioName)
    const mergedBuySeries = await StatsService.GetMergedAverageBuySeries(portfolio, quotes)
    const benchmarkAsset: IPortfolioItem = await StatsService.ConstructBenchmark(benchmark, mergedBuySeries)

    const benchmarkPerformance: Performance = {
        performance: await StatsService.GetAssetPerformanceWithoutCashInflow(benchmarkAsset),
        name: benchmark
    }
    const otherPerformance = await StatsService.MergeQuotesPerformance(portfolio, quotes, true)
    otherPerformance.name = 'selected assets'

    const comparablePerformance = await StatsService.MergeBenchmarkWithOther(benchmarkPerformance, otherPerformance)


    return Success(res, comparablePerformance)

}

const CompareQuotesPriceWithBenchmark: IController = async (req, res, next) => {

    const email = getEmailFromRequest(req)
    const {portfolioName} = req.params
    const quotes: string[] = req.body.quotes
    const benchmark: string = req.body.benchmark

    if (!benchmark || quotes.length === 0) return Error(res, "Please fill benchmark", 400)

    const portfolio = await PortfolioService.GetPortfolioByName(email, portfolioName)
    const mergedBuySeries = await StatsService.GetMergedAverageBuySeries(portfolio, quotes)
    const benchmarkAsset: IPortfolioItem = await StatsService.ConstructBenchmark(benchmark, mergedBuySeries)

    const benchmarkPerformance: Performance = {
        performance: await StatsService.GetAssetPerformanceWithoutCashInflow(benchmarkAsset),
        name: benchmark
    }
    const allPerformance = await StatsService.GetQuotesPerformance(portfolio, quotes, true)
    allPerformance.push(benchmarkPerformance)

    const comparablePerformance = await StatsService.MergeListPerformances(allPerformance)


    return Success(res, comparablePerformance)
}


const GetQuotesCorrelation: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {portfolioName} = req.params
    const {from, to, quotes} = req.body
    if (quotes.length === 0) return Error(res, "No quotes provided", 400)

    const portfolio = await PortfolioService.GetPortfolioByName(email, portfolioName)
    const correlation = await StatsService.GetQuotesCorrelation(from, to, quotes, portfolio)

    return Success(res, correlation)
}

const GetQuoteVolatility: IController = async (req, res, next) => {
    const {quote, range, period, method} = req.body
    if (!quote || !range) return Error(res, "Mandatory values must be filled", 400
    )
    const volatility = await StatsService.GetQuoteVolatility(quote, range, period, method)

    return Success(res, volatility)
}

const GetQuotePL: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {portfolioName} = req.params
    const {quotes} = req.body

    const portfolioModel = await PortfolioService.GetPortfolioByName(email, portfolioName)
    const pl = await StatsService.GetQuotesPl(quotes, portfolioModel)

    return Success(res, pl)
}

export const StatsController = {
    GetAlStatsByUser,
    GetPerformance,
    GetPortfolioPerformance,
    GetAssetPerformance,
    GetAssetPerformanceWithoutCashInflow,
    GetPortfolioSectorAllocation,
    GetPortfolioCountryAllocation,
    GetPortfolioIndustryAllocation,
    GetQuotesPerformance,
    GetQuotesPerformanceWithoutCashInflow,
    GetWeightedQuotesPerformanceWithoutCashInflow,
    GetPortfolioAssetTypeAllocation,
    GetPortfolioAssetAllocation,
    CompareQuotes,
    CompareQuotesWithoutCashInflow,
    CompareQuotesWithBenchmark,
    CompareQuotesPriceWithBenchmark,
    GetQuotesCorrelation,
    GetQuoteVolatility,
    GetQuotePL,
}
