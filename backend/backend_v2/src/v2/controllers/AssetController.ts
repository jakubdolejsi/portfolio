import {IController} from "../interfaces/controllers/IController";
import {Error, Success} from '../interfaces/response/response'
import {FinhubService} from "../services/external/FinhubService";
import {YahooFinanceService} from "../services/external/YahooFinanceService";
import {AssetService} from "../services/AssetService";
import {AssetWithChart, IAssetModel} from "../models/AssetModel";
import {getEmailFromRequest} from "./UserController";
import {PortfolioService} from "../services/PortfolioService";

const GetAssetByName: IController = async (req, res, next) => {
    const {name} = req.body
    if (!name) return Error(res, "Name is not filled", 400);

    const company = await FinhubService.GetCompanyByName(name)
    if (!company) return Error(res, "No companies found", 400);

    return Success(res, company);

}

const GetAssetByTicker: IController = async (req, res, next) => {
    const {ticker} = req.params;
    if (!ticker) return Error(res, "Ticker is not filled", 400);

    const asset = await AssetService.GetAssetByTicker(ticker);
    if (!asset) return Error(res, "Asset not found", 404)

    return Success(res, asset);
}

const GetAssetByQuote: IController = async (req, res, next) => {
    const {quote} = req.params;
    if (!quote) return Error(res, "Quote is not filled", 400);

    const company = await YahooFinanceService.GetCompanyByQuote([quote]);
    if (!company) return Error(res, "No company found", 400);

    return Success(res, company);
}

const SearchAssetLive: IController = async (req, res, next) => {
    const {quote} = req.params;
    if (!quote) return Error(res, "Quote is not filled", 400);

    const company = await YahooFinanceService.Search(quote);
    if (!company) return Error(res, "No company found", 400);

    return Success(res, company);
}


const GetAllAssets: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const portoflios = await PortfolioService.GetAllUserPortfolios(email)

    const allAssets: IAssetModel[] = []
    for (const p of portoflios) {
        for (const a of p.assets) {
            if (!allAssets.includes(a.asset)) {
                const asset = await AssetService.GetAssetByName(a.asset._id.toString(), true)
                allAssets.push(asset)
            }
        }
    }
    return Success(res, allAssets)
}

const CreateNewAsset: IController = async (req, res, next) => {

    // const assetType: IFundType = {
    //     family: "FOndik",
    //     price: 0,
    //     pb: 0,
    //     pe: 0,
    //     name: "ETF",
    //     dividend: null,
    //     holdings: [],
    //     ticker: "VOO",
    //     marketCap: 433244,
    //     currency: 'USD'
    // }
    // const asset: IAssetModel = {
    //     name: "Vanguard SP500",
    //     type: assetType,
    // }
    // const createdAsset = await AssetService.CreateNewAsset(asset);
    // if (!createdAsset) return Error(res, "Asset cannot be created", 500)

    return Success(res, 'Should not exists')
}

const RequestAsset: IController = async (req, res, next) => {
    const {ticker} = req.body

    const response = await AssetService.RequestSummaryData(ticker)
    if (!response) return Error(res, "Not data", 400)

    return Success(res, response)
}

const GetAssetHistoricalInfo: IController = async (req, res, next) => {
    // Date: ROK-MESIC-DEN
    const {ticker, date} = req.params

    if (!ticker || !date) return Error(res, "Ticker or Date not filed", 400);

    const response = await AssetService.GetAssetPriceByDate(ticker, date)
    if (!response) return Error(res, "No historical data found", 400);

    return Success(res, response);
}

const GetAssetCompleteHistory: IController = async (req, res, next) => {
    const {ticker} = req.params

    const result = await AssetService.GetCompleteHistory(ticker)
    if (!result) return Error(res, "No historicl data available", 500)

    return Success(res, result)
}

const GetAssetWithCompleteDetails: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)

    const portoflios = await PortfolioService.GetAllUserPortfolios(email)

    const allAssets: AssetWithChart[] = []
    try {
        for (const p of portoflios) {
            for (const a of p.assets) {
                if (!allAssets.find(asset => asset.type.ticker === a.asset.type.ticker)) {
                    const asset = await AssetService.GetAssetWithChart(p, a.asset._id.toString())
                    allAssets.push(asset)
                }
            }
        }
        return Success(res, allAssets)
    } catch (e) {
        return Error(res, e, 500)
    }

}

const GetBenchmark: IController = async (req, res, next) => {
    const {ticker} = req.params
    const benchmark = await AssetService.GetBenchmark(ticker)
    if (!benchmark) return Error(res, `Asset ${ticker} not found`, 404)

    return Success(res, benchmark)
}

export const AssetController = {
    GetAssetByName,
    GetAssetByTicker,
    GetAssetByQuote,
    CreateNewAsset,
    RequestAsset,
    GetAssetHistoricalInfo,
    SearchAssetLive,
    GetAllAssets,
    GetAssetCompleteHistory,
    GetAssetWithCompleteDetails,
    GetBenchmark,
}
