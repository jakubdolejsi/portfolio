import {IController} from "../interfaces/controllers/IController";
import {UserService} from "../services/UserService";
import {Error, Success} from "../interfaces/response/response";
import {IPortfolioModel, PortfolioCurrency} from "../models/PortfolioModel";
import {PortfolioService} from "../services/PortfolioService";
import {getEmailFromRequest} from "./UserController";
import Logger from "../logger/logger";


function capitalize(word: string) {
    return word[0].toUpperCase() + word.substring(1).toLowerCase();
}

const GetUserPortfolio: IController = async (req, res, next) => {
    const {name} = req.params;
    const email = getEmailFromRequest(req)

    const portfolio = await UserService.GetUserPortfolio(email, name);
    if (!portfolio) return Error(res, "not found", 400);

    return Success(res, portfolio);
}

const GetUserPortfolioAsset: IController = async (req, res, next) => {
    const {name, ticker} = req.params;
    const email = getEmailFromRequest(req)

    const user = await UserService.GetUserByPortfolioWithPopulatedAsset(email, name);
    const portfolioModels = user.portfolio
        .filter(p => p.name.toUpperCase() === name.toUpperCase())

    const assetsWithoutHistory = portfolioModels.map(p => p.assets.map(a => {
        if (a.asset.type.ticker.toUpperCase() === ticker.toUpperCase()) {
            a.asset = {
                name: a.asset.name,
                type: a.asset.type,
                _id: a.asset._id,
                category: a.asset.category
            }
            return a
        }
    }))
    if (assetsWithoutHistory[0][0] === undefined) {
        return Error(res, "No assets found", 400)
    }
    return Success(res, assetsWithoutHistory);
}


const AddNewPortfolio: IController = async (req, res, next) => {
    const {portfolioName, currency} = req.body
    const email = getEmailFromRequest(req)

    const portfolio: IPortfolioModel = {
        name: portfolioName,
        balance: 0,
        currency,
        assets: [],
        tradesCount: 0,
    }
    const createdPortfolio = await PortfolioService.CreateNewPortfolio(email, portfolio);
    if (!createdPortfolio) return Error(res, "Portfolio already exists", 400)

    return Success(res, createdPortfolio)
}

const GetAllPortfolios: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)

    const portfolios = await PortfolioService.GetAllUserPortfolios(email);
    return Success(res, portfolios)
}

const GetAllPortfoliosWithHistory: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)

    const portfoliosWithHistory = await PortfolioService.GetAllUserPortfolios(email, true)
    return Success(res, portfoliosWithHistory)
}


const DeletePortfolio: IController = async (req, res, next) => {
    const {newName, oldName} = req.params;
    const email = getEmailFromRequest(req)
    const removedPortfolio = await PortfolioService.DeletePortfolio(email, oldName, newName);
    if (!removedPortfolio) return Error(res, "Portfolio cannot be removed", 500);

    return Success(res, removedPortfolio);
}

const SetPortfolioCurrency: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {name, currency} = req.body

    const updated = await PortfolioService.SetPortfolioCurrency(email, name, currency as PortfolioCurrency)
    if (!updated) return Error(res, "Currency cannot be set", 500)

    return Success(res, updated)
}

export const PortfolioController = {
    GetUserPortfolio,
    AddNewPortfolio,
    GetAllPortfolios,
    DeletePortfolio,
    GetUserPortfolioAsset,
    SetPortfolioCurrency,
    GetAllPortfoliosWithHistory
}