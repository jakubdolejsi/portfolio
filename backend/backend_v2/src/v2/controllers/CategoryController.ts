import {IController} from "../interfaces/controllers/IController";
import {Fake, getEmailFromRequest} from "./UserController";
import {ICategory} from "../models/CategoryModel";
import {Error} from "../interfaces/response/response";
import {AssetService} from "../services/AssetService";
import {UserService} from "../services/UserService";


const AddCategoryToAsset: IController = async (req, res, next) => {
    const category: ICategory = req.body.category
    const ticker: string = req.body.ticker
    const email = getEmailFromRequest(req)

    return Error(res, "adasda", 500);
}

const GetAllUserCategories: IController = async (req, res, next) => {
    return Error(res, "adasda", 500);

}

const UpdateAssetCategory: IController = async (req, res, next) => {
    return Error(res, "adasda", 500);

}
const GetCategoryByName: IController = async (req, res, next) => {
    return Error(res, "adasda", 500);
}


export const CategoryController = {
    AddCategoryToAsset,
    GetAllUserCategories,
    UpdateAssetCategory,
    GetCategoryByName
}