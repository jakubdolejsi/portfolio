import {IController} from "../interfaces/controllers/IController";
import {UserService} from "../services/UserService";
import {Success, Error} from "../interfaces/response/response";
import {Request} from "express";
import Logger from "../logger/logger";
import {IUserModel} from "../interfaces/models/IUser";
import {IUser} from "../models/UserModel";
import {DateService} from "../services/DateService";

export interface IInputUser {
    email: string,
    dateRegistered: Date,
    lastLogin: Date
}

const FakeUser = (): IInputUser => {
    return {
        email: "aa@aa",
        dateRegistered: new Date('08-07-2021'),
        lastLogin: new Date('09-07-2021'),
    }
}

export const getEmailFromRequest = (req: Request) => {
    // @ts-ignore
    return req.user['https://example.com/email']
}


// const GetUserById: IController = async (req, res, next) => {
//     const {id, email} = req.body
//     const user = await UserService.GetUserById(id);
//     if (user == null) {
//         const createdUser = await UserService.CreateUser({id, email})
//         if (createdUser) return Success(res, createdUser);
//
//         return Error(res, "user cannot be created", 400);
//     }
//     return Success(res, {
//         user
//     })
// }

// @ts-ignore
const GetOrCreate: IController = async (req, res, next) => {
    const {email} = getEmailFromRequest(req)
    const userEmail: string = req.body.email

    // @ts-ignore
    const user = await UserService.GetUserByEmail(userEmail);
    Logger.error(`user : ${user}`)
    if (!user) {
        const userToCreate: IUser = {
            dateRegistered: new Date(DateService.GetCurrentDateYMDSync()),
            lastLogin: new Date(DateService.GetCurrentDateYMDSync()),
            email: userEmail,
            trades: [],
            portfolio: [{
                assets: [],
                name: 'Default',
                balance: 0,
                currency: 'USD',
                tradesCount: 0,
                statistics: {
                    performance: 0,
                    tradesCount: 0,
                    assetsCount: 0,
                    assetWithMostShares: {
                        name: '',
                        value: 0
                    },
                    currencies: ["USD"],
                    theLeastExpensiveAsset: {
                        name: '',
                        value: 0
                    },
                    theMostExpensiveAsset: {
                        name: '',
                        value: 0
                    },
                    theMostTradedAsset: {
                        name: '',
                        value: 0
                    },
                    theMostTradedCountry: {
                        name: '',
                        values: [
                            {
                                name: '',
                                value: 0
                            }
                        ]
                    },
                    theMostTradedSector: {
                        name: '',
                        values: [
                            {
                                name: '',
                                value: 0
                            }
                        ]
                    },
                    totalBalanceInUSD: 0
                }
            }]
        }
        Logger.error(`creating user: ${userToCreate}`)
        const createdUser = await UserService.CreateUser(userToCreate)
        if (createdUser) return Success(res, createdUser);

        return Error(res, "user cannot be created", 400);
    }
    return Success(res, {
        user
    })
}

const CreateFakeUser: IController = async (req, res, next) => {
    // const userInfo = FakeUser();
    // if (await UserService.GetUserByEmail(userInfo.email)) return Error(res, "User already exists", 400);
    //
    // const user = await UserService.CreateUser(userInfo)
    //
    // if (!user) return Error(res, "Something went wrong", 500);

    return Success(res, '');
}
const CheckIfUserExists: IController = async (req, res, next) => {
    const {userId} = req.body

    return Success(res, userId)
}
export const UserController = {
    CreateFakeUser,
    CheckIfUserExists,
    GetOrCreate,
}

export const Fake = {
    FakeUser,
}
