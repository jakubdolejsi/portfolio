import {IController} from "../interfaces/controllers/IController";
import {Success, Error} from "../interfaces/response/response";
import {TradeService} from "../services/TradeService";
import {UserService} from "../services/UserService";
import {Fake, getEmailFromRequest} from "./UserController";
import {PortfolioService} from "../services/PortfolioService";

export interface IInputTrade {
    ticker: string,
    amount: number,
    overallPrice?: number,
    pricePerAsset?: number,
    cash?: string,
    date?: string,
    portfolio?: string, // Name of the portfolio to which given trade belongs
    occurring?: number
}

const AddNewTrade: IController = async (req, res, next) => {
    const trade: IInputTrade = req.body;

    const email = getEmailFromRequest(req)

    const user = await UserService.GetUserByEmail(email)
    if (!user) return Error(res, `User with email ${email} not found`, 404);

    const {createdTrade, asset, cron} = await TradeService.AddNewTrade(user, trade)
    if (cron) {
        return Success(res, 'Cron job successfully set')
    }
    if (!createdTrade) return Error(res, "Neco je spatne", 500);

    const portfolio = await PortfolioService.UpdatePortfolio(email, trade.portfolio, createdTrade, asset)
    // return Success(res, createdTrade);
    return Success(res, portfolio);
}


const GetAllTrades: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)

    const trades = await TradeService.GetAllTradesByUser(email)
    return Success(res, trades)
}

const GetTradesByAsset: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {ticker} = req.params

    const trades = await TradeService.GetAllTradesByAsset(email, ticker)
    if (!trades) return Error(res, "No assets found", 400)

    return Success(res, trades)
}

const RemoveAllTradesByAsset: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {ticker} = req.params

    const removed = await TradeService.RemoveTradeByAsset(email, ticker)

    return Success(res, removed)
}

const RemoveTradeById: IController = async (req, res, next) => {
    const email = getEmailFromRequest(req)
    const {id} = req.params

    const removed = await TradeService.RemoveTradeById(email, id)
    if (!removed) return Error(res, "Nothing to remove", 400)

    const result = await TradeService.RemoveTradeFromUser(email, removed)
    return Success(res, {
        result,
        removed
    })
}

export const TradeController = {
    AddNewTrade,
    GetAllTrades,
    GetTradesByAsset,
    RemoveAllTradesByAsset,
    RemoveTradeById,
}
