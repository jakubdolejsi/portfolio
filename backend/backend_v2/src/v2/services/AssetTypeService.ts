import {TopHoldingsHolding} from "yahoo-finance2/dist/cjs/src/modules/quoteSummary-iface";
import {Holding} from "./external/YahooFinanceService";

export type AssetTypeName = 'Equity' | 'Cryptocurrency' | 'ETF'

export type Undefined = 'n/a'

export interface IAssetType {
    name: string, // EQUITY/ETF/Cryptocurrency
    ticker: string,
    marketCap: number | Undefined,
    price: number,
    currency: string
}

export interface ICryptoType extends IAssetType {
    exchange: string
}

interface BasicAssetType extends IAssetType {
    pe: number | Undefined,
    pb: number | Undefined,
    beta: number,
    dividend: number | string,
}

export interface IFundType extends BasicAssetType {
    family: string,
    holdings: Holding[]
}

export interface IStockType extends BasicAssetType {
    sector: string,
    country: string,
    industry: string,
}
