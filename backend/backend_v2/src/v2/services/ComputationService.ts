import {StatsItem} from "../models/PortfolioModel";

const computePerformance = () => {
    return;
}

const getBiggerValue = (arr: StatsItem[]) => {
    let max: StatsItem = {
        value: -10000,
        name: ''
    }
    arr.forEach(a => {
        if (max.value < a.value) {
            max = a
        }
    })
    return max
}

const processValue = (arr: StatsItem[], value: StatsItem) => {
    const found = arr.find(e => e.name === value.name)
    if (found) {
        arr[arr.indexOf(found)].value += value.value;
    } else {
        arr.push(value)
    }
}

const round = (n: number, d: number = 2) => {
    return parseFloat(n.toFixed(d))
}

type ICompute = (y1: number, y2: number) => number

export const ComputeChange: ICompute = (y1, y2) => {
    const relative = round((y2 - y1) / y1)
    const relativePercent = round(relative * 100)
    const absolute = round(y1 * relative, 0)
    const sign = Math.sign(relative)
    return absolute
}

export const ComputationService = {
    computePerformance,
    getBiggerValue,
    processValue,
    ComputeChange,
    round
}