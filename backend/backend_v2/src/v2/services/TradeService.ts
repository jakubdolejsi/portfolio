import {IInputTrade} from "../controllers/TradeController";
import {IInputUser} from "../controllers/UserController";
import {AssetService} from "./AssetService";
import {ITradeModel, TradeModel} from "../models/TradeModel";
import {handleError} from "./index";
import {UserService} from "./UserService";
import Logger from "../logger/logger";
import cron from "node-cron";

const GetTodayDate = () => {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();

    return yyyy + '-' + mm + '-' + dd
}



const AddNewTrade = async (user: IInputUser, trade: IInputTrade) => {
    const asset = await AssetService.GetAssetByTicker(trade.ticker)
    if (!asset) {
        handleError("Asset not found or creation failed")
        return {
            createdTrade: null,
            asset: null
        }
    }

    const tradeToBeCreated: ITradeModel = {
        executionDate: trade.date,
        asset,
        pricePerAsset: trade.pricePerAsset,
        overallPrice: trade.overallPrice,
        amount: trade.amount,
    }

    if (!trade.date) {
        tradeToBeCreated.executionDate = GetTodayDate()
    }
    if (!trade.pricePerAsset) {
        const history = await AssetService.GetAssetPriceByDate(trade.ticker, tradeToBeCreated.executionDate)
        tradeToBeCreated.pricePerAsset = history ? history.open : asset.type.price
        tradeToBeCreated.overallPrice = trade.amount * tradeToBeCreated.pricePerAsset
    }

    if(trade.occurring) { // do cron job
        cron.schedule(`* * ${trade.occurring} * *`, async () => {
            tradeToBeCreated.executionDate = GetTodayDate()
            const history = await AssetService.GetAssetPriceByDate(trade.ticker, tradeToBeCreated.executionDate)
            tradeToBeCreated.pricePerAsset = history ? history.open : asset.type.price
            tradeToBeCreated.overallPrice = trade.amount * tradeToBeCreated.pricePerAsset

            const createdCronJobTrade = await TradeModel.create(tradeToBeCreated)
            if (!createdTrade) return handleError("Trade has not been created successfully")

            const updatedUserByCronjob = await UserService.UpdateUserTrades(user.email, createdTrade)
            if (!updatedUser) return handleError("User trades has not been updated successfully")
        })
        return {
            cron: 'set'
        }

    }

    const createdTrade = await TradeModel.create(tradeToBeCreated)
    if (!createdTrade) return handleError("Trade has not been created successfully")

    const updatedUser = await UserService.UpdateUserTrades(user.email, createdTrade)
    if (!updatedUser) return handleError("User trades has not been updated successfully")

    return {
        createdTrade,
        asset
    }
}

const GetTradeById = async (id: string) => {
    return TradeModel.find({_id: id});
}

const GetAllTradesByUser = async (email: string, history = true) => {
    const trades = await UserService.GetAllTrades(email)
    if (history) return trades

    trades.forEach(trade => {
        trade.asset.history = []
    })
    return trades
}

const GetAllTradesByAsset = async (email: string, ticker: string): Promise<ITradeModel[]> => {
    const trades = await GetAllTradesByUser(email, false)
    const matchTrades: ITradeModel[] = []
    trades.map(trade => {
        if (trade.asset.type.ticker.toUpperCase() === ticker.toUpperCase()) {
            matchTrades.push(trade)
        }
    })
    return matchTrades
}

const RemoveTradeByAsset = async (email: string, ticker: string) => {
    const trades: ITradeModel[] = await GetAllTradesByUser(email, false)
    const idToRemove = new Set<object>();

    trades.map(t => {
        if (t.asset.type.ticker.toUpperCase() === ticker.toUpperCase()) {
            idToRemove.add(t._id)
        }
    })
    for (const id of idToRemove) {
        const removed = await TradeModel.findByIdAndRemove(id)
        Logger.info(removed)
    }
    return idToRemove
}

const RemoveTradeById = async (email: string, id: any): Promise<ITradeModel> => {
    return TradeModel.findByIdAndRemove(id);
}

// todo snizit citac tradeCount v user Portfolio
const RemoveTradeFromUser = async (email: string, removedTrade: ITradeModel) => {
    const user = await UserService.GetUserByEmail(email)
    const filteredTrades = user.trades.filter(trade => {
        return !trade._id.equals(removedTrade._id)
    })
    const a = await UserService.SetUserTrades(email, filteredTrades)
    const b = await RemoveAssetFromUser(email, removedTrade.asset)
    return {
        a,
        b
    }
}

const RemoveAssetFromUser = async (email: string, assetId: any) => {
    const asset = await AssetService.GetAssetById(assetId)
    const trades = await GetAllTradesByAsset(email, asset.type.ticker)
    let assetNeedsToBeRemoved = false
    if (trades.length === 0) {
        assetNeedsToBeRemoved = true
    }
    const user = await UserService.GetUserByEmail(email)

    if (assetNeedsToBeRemoved) {
        user.portfolio.forEach(portfolio => {
            portfolio.assets = portfolio.assets.filter(a => !a.asset._id.equals(assetId))
        })
        return await UserService.UpdateAllPortfolios(email, user.portfolio)
    }

}
export const TradeService = {
    AddNewTrade,
    GetAllTradesByUser,
    GetAllTradesByAsset,
    RemoveTradeByAsset,
    RemoveTradeById,
    RemoveTradeFromUser,
    RemoveAssetFromUser
}
