import {IUser, UserModel} from "../models/UserModel";
import {handleError} from "./index";
import {ITradeModel} from "../models/TradeModel";
import {IPortfolioItem, IPortfolioModel} from "../models/PortfolioModel";
import {cacheManager} from "../cache/cacheManager";

const GetUserById = async (id: string) => {
    return UserModel.findOne({_id: id})
        .then((user: any) => {
            return user;
        })
        .catch((err: any) => {
            return handleError(err);
        })
}

const GetUserByEmail = async (email: string): Promise<IUser> => {
    return UserModel.findOne({email})
        .then((user: any) => {
            return user;
        })
        .catch((err: any) => {
            return handleError(err);
        })
}

const GetAllAssetsInAllPortfolios = async (email: string): Promise<IUser> => {
    const u = await UserModel.findOne({email})
        .populate({
            path: 'portfolio',
            // populate: {
            //     path: 'assets',
            //     populate: {
            //         path: 'asset'
            //     }
            // }
        })
    return u
}

const CreateUser = async (user: IUser) => {
    return UserModel.create(user)
        .then(usr => {
            return usr;
        })
        .catch(err => {
            return handleError(err);
        })
}

const UpdateUserTrades = async (email: string, trade: ITradeModel) => {
    return UserModel.updateOne({email}, {
        $push: {
            trades: trade
        }
    })
        .then(updatedUser => {
            return updatedUser
        })
        .catch(err => {
            return handleError(err)
        })
}

const GetAllTrades = async (email: string): Promise<ITradeModel[]> => {
    const cachedTrades = cacheManager.get<ITradeModel[]>(`trades_${email}`)
    if (cachedTrades) return cachedTrades

    const user = await UserModel.findOne({email})
        .populate({
            path: 'trades',
            populate: {
                path: 'asset'
            }
        })
    if(!user) return []
    return user.trades
}


const GetUserPortfolio = async (email: string, portfolioName: string, history: boolean = false): Promise<IPortfolioModel> => {
    const user: IUser = await UserModel.findOne({email, 'portfolio.name': portfolioName}).populate({
        path: 'portfolio.assets.asset',
        model: 'Asset'
    });
    if (!user) return handleError(`Portfolio ${portfolioName} does not exists`)

    if (!history) {
        user.portfolio.forEach(p => {
            p.assets.forEach(a => {
                a.asset.history = []
            })
        })
    }
    return user.portfolio.find(p => p.name === portfolioName);
}

const RemoveUserPortfolio = async (email: string, portfolioName: string, newPortfolioName: string) => {
    if (!newPortfolioName) {
        newPortfolioName = 'Default';
    }
    let assetsToBeMoved: IPortfolioItem[];
    const user = await GetUserByPortfolioWithPopulatedAsset(email, portfolioName);
    user.portfolio.forEach(p => {
        if (p.name === portfolioName) {
            assetsToBeMoved = p.assets
        }
    })
    const uniqueAssets: IPortfolioItem[] = [];
    user.portfolio.forEach(portfolio => {
        if (portfolio.name === newPortfolioName) { // check if moved asset exists in new portfolio
            assetsToBeMoved.forEach(movedAsset => {
                portfolio.assets.forEach(portfolioItem => {
                    if (portfolioItem.asset.name === movedAsset.asset.name) { // if moved asset already exists in 'new' portfolio
                        portfolioItem.amount = (parseInt(portfolioItem.amount, 10) + parseInt(movedAsset.amount, 10)).toString()
                    } else {
                        uniqueAssets.push(movedAsset)
                    }
                })
                if (portfolio.assets.length === 0) uniqueAssets.push(movedAsset)
            })
        }
    })
    user.portfolio.forEach(portfolio => {
        if (portfolio.name === newPortfolioName) {
            uniqueAssets.forEach(assetToBeAdded => {
                portfolio.assets.push(assetToBeAdded);
            })
        }
    })
    await UpdateWholeUserPortfolio(email, newPortfolioName, user.portfolio)

    await UserModel.updateOne({'portfolio.name': portfolioName}, {
        $pull: {
            'portfolio': {
                'name': portfolioName
            }
        }
    })
}

const UpdateWholeUserPortfolio = async (email: string, portfolioName: string, portfolio: IPortfolioModel[]) => {
    return UserModel.findOneAndUpdate({email, 'portfolio.name': portfolioName}, {
        portfolio
    })
}

const UpdateAllPortfolios = async (email: string, portfolios: IPortfolioModel[]) => {
    return UserModel.findOneAndUpdate({email}, {
        'portfolio': portfolios
    })
}
const GetUserByPortfolioWithPopulatedAsset = async (email: string, portfolioName: string): Promise<IUser> => {
    return UserModel.findOne({email, 'portfolio.name': portfolioName}).populate({
        path: 'portfolio.assets.asset',
        model: 'Asset'
    });
}

const UpdateUserPortfolio = async (email: string, portfolio: IPortfolioModel) => {
    return UserModel.findOneAndUpdate({email}, {
        $addToSet: {
            portfolio
        }
    })
}

const SetUserTrades = async (email: string, trades: ITradeModel[]): Promise<IUser> => {
    return UserModel.findOneAndUpdate({email}, {
        'trades': trades
    })
}


export const UserService = {
    GetUserById,
    GetUserByEmail,
    CreateUser,
    UpdateUserTrades,
    GetAllTrades,
    GetUserPortfolio,
    UpdateUserPortfolio,
    RemoveUserPortfolio,
    GetUserByPortfolioWithPopulatedAsset,
    UpdateWholeUserPortfolio,
    GetAllAssetsInAllPortfolios,
    SetUserTrades,
    UpdateAllPortfolios,
}
