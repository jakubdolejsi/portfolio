import {
    GeneralStatistics,
    IPortfolioItem,
    IPortfolioModel,
    PortfolioCurrency,
    PortfolioStatistics,
    StatsItem
} from "../models/PortfolioModel";
import {PortfolioService} from "./PortfolioService";
import {IFundType, IStockType} from "./AssetTypeService";
import {ComputationService} from './ComputationService'
import {handleError} from "./index";
import {DateService} from "./DateService";
import {UserService} from "./UserService";
import {AssetService} from "./AssetService";
import {TradeService} from "./TradeService";
import {History} from "./external/YahooFinanceService";
import {IAsset} from "../interfaces/models/IAsset";
import {IAssetModel} from "../models/AssetModel";
import Logger from "../logger/logger";

// tslint:disable-next-line:no-var-requires
const calculateCorrelation = require("calculate-correlation");

// tslint:disable-next-line:no-var-requires
const math = require('mathjs')


export interface PerformanceItem {
    date: string,
    value: number
}


export interface Performance {
    name: string,
    performance: PerformanceItem[]
}


export interface ComparableItem {
    date: string,
    keyName: string,
    value: number
}


const GetPerformance = async (email: string) => {
    return await PortfolioService.GetAllUserPortfolios(email, true)
}

const GetPortfolioPerformance = async (email: string, portfolioName: string): Promise<Performance> => {
    const allPortfolio = await PortfolioService.GetAllUserPortfolios(email, true)
    const portfolio = allPortfolio.find(p => p.name === portfolioName)
    if (!portfolio) return handleError(`Portfolio ${portfolioName} does note exists`)
    const testItem = portfolio.assets[1]
    const performance = await GetAssetPerformance(testItem)

    return {
        name: testItem.asset.type.ticker,
        performance
    }
}

// Asset performance computation algorithm
const GetAssetPerformance = async (item: IPortfolioItem) => {
    const startIndex = item.asset.history.findIndex(a => a.date === item.averageBuySeries[0].date)
    const interestingHistory = item.asset.history.slice(startIndex)
    const newHistory: PerformanceItem[] = [{
        date: item.asset.history[startIndex - 1].date,
        value: 0
    }]
    interestingHistory.forEach((history, index) => {
        if (index === 0) return
        const y1 = interestingHistory[index - 1]
        const y2 = interestingHistory[index]
        const absoluteChange = ComputationService.round((((y2.open - y1.open) / y1.open) * newHistory[index - 1].value) + newHistory[index - 1].value, 0)
        const buyPoint = item.averageBuySeries.find(s => s.date === y1.date)
        if (buyPoint) {
            const price = absoluteChange + buyPoint.amountInvested
            newHistory.push({
                date: y1.date,
                value: price
            })
        } else {
            newHistory.push({
                date: y1.date,
                value: absoluteChange
            })
        }
    })
    return newHistory
}

const GetAssetPerformanceWithoutCashInflow = async (item: IPortfolioItem) => {
    let cashInflow = 0
    if (!item) return []

    const startIndex = item.asset.history.findIndex(a => a.date === item.averageBuySeries[0].date)
    const interestingHistory = item.asset.history.slice(startIndex)
    const newHistory: PerformanceItem[] = [{
        date: item.asset.history[startIndex - 1].date,
        value: 0
    }]
    const newHistoryWithoutInflow: PerformanceItem[] = [{
        date: item.asset.history[startIndex - 1].date,
        value: 0
    }]
    interestingHistory.forEach((history, index) => {
        if (index === 0) return
        const y1 = interestingHistory[index - 1]
        const y2 = interestingHistory[index]
        const absoluteChange = ComputationService.round((((y2.open - y1.open) / y1.open) * newHistory[index - 1].value) + newHistory[index - 1].value, 0)
        const buyPoint = item.averageBuySeries.find(s => s.date === y1.date)
        if (buyPoint) {
            cashInflow += buyPoint.amountInvested
            const price = absoluteChange + buyPoint.amountInvested
            newHistory.push({
                date: y1.date,
                value: price
            })
            newHistoryWithoutInflow.push({
                date: y1.date,
                value: ComputationService.round(price - cashInflow, 2)
            })
        } else {
            newHistory.push({
                date: y1.date,
                value: absoluteChange
            })
            newHistoryWithoutInflow.push({
                date: y1.date,
                value: ComputationService.round(absoluteChange - cashInflow, 2)
            })
        }
    })
    // return newHistory
    return newHistoryWithoutInflow
}


const MergeAssetPerformanceWithoutCashInflow = async () => {
    return 'jejda hojda'
}

const _processMerging = (longerPerformanceAsset: PerformanceItem[], shorterPerformanceAsset: PerformanceItem[]) => {
    const newHistory: PerformanceItem[] = []
    longerPerformanceAsset.forEach((performanceItem, index) => {
        if (shorterPerformanceAsset[index]) { // if history already exists in second asset
            const newValue = shorterPerformanceAsset[index].value + performanceItem.value
            newHistory.push({
                date: performanceItem.date,
                value: newValue
            })
        } else { // if history exists only in the first asset
            newHistory.push({
                date: performanceItem.date,
                value: performanceItem.value
            })
        }
    })
    return newHistory
}

interface AverageBuySeriesForBenchmark {
    date: string,
    amount: number
}

const MergeQuotesPerformance = async (portfolio: IPortfolioModel, quotes: string[], cashInflow: boolean = false) => {
    const performances = await GetQuotesPerformance(portfolio, quotes, cashInflow)
    return await StatsService.MergeAllPerformance(performances)
}

const GetQuotesPerformance = async (portfolio: IPortfolioModel, quotes: string[], cashInflow: boolean = false) => {
    const tickers = quotes.map(q => q.toUpperCase())
    const filteredItems = portfolio.assets.filter(a => tickers.includes(a.asset.type.ticker.toUpperCase()))

    return await StatsService.GetItemsPerformance(filteredItems, cashInflow)
}

const MergeAverageBuySeries = async (filteredItems: IPortfolioItem[]) => {
    const mergedAverageBuySeries: AverageBuySeriesForBenchmark[] = []
    filteredItems.forEach(item => {
        item.averageBuySeries.forEach(serie => {
            mergedAverageBuySeries.push({
                date: serie.date,
                amount: serie.sharesBought
            })
        })
    })
    return mergedAverageBuySeries.sort((a, b) => {
        const aa = a.date.split('/').reverse().join('')
        const bb = b.date.split('/').reverse().join('')
        return aa > bb ? 1 : aa < bb ? -1 : 0;
    })
}

const GetMergedAverageBuySeries = async (portfolio: IPortfolioModel, quotes: string[]) => {
    const tickers = quotes.map(q => q.toUpperCase())
    const filteredItems = portfolio.assets.filter(a => tickers.includes(a.asset.type.ticker.toUpperCase()))

    return MergeAverageBuySeries(filteredItems)
}

const ConstructBenchmark = async (ticker: string, averageBuySeries: AverageBuySeriesForBenchmark[]) => {
    const portfolioItem = {} as IPortfolioItem
    portfolioItem.asset = await AssetService.GetAssetByTicker(ticker)
    portfolioItem.averageBuySeries = []
    averageBuySeries.forEach(series => {
        const buyPoint = portfolioItem.asset.history.find(a => a.date === series.date)
        const price = buyPoint.close * series.amount
        portfolioItem.averageBuySeries.push({
            date: series.date,
            sharesBought: series.amount,
            amountInvested: price,
            averageAssetPrice: 0
        })
    })
    return portfolioItem
}

// Asset merging algorithm
const ComputeAndMergePerformance = async (item1: IPortfolioItem, item2: IPortfolioItem) => {
    const performance1 = await GetAssetPerformance(item1)
    const performance2 = await GetAssetPerformance(item2)

    return MergePerformance(performance1, performance2)
}

const MergeAllPerformance = async (performanceItems: Performance[]) => {
    const finalPerformance: Performance = {
        name: 'overall',
        performance: []
    }
    if (performanceItems.length === 1) {
        finalPerformance.performance = performanceItems[0].performance
        return finalPerformance
    }
    performanceItems.forEach((p, i, arr) => {
        if (i === 0) { // first pass
            finalPerformance.performance =
                MergePerformanceSync(arr[i].performance, arr[i + 1].performance)
        } else { // 2nd and other pass
            if (arr[i + 1]) {
                finalPerformance.performance =
                    MergePerformanceSync(finalPerformance.performance, arr[i + 1].performance)
            }
        }
    })
    return finalPerformance
}

const MergePerformance = async (performance1: PerformanceItem[], performance2: PerformanceItem[]) => {
    return _mergePerf(performance1, performance2)

}

const MergePerformanceSync = (performance1: PerformanceItem[], performance2: PerformanceItem[]) => {
    return _mergePerf(performance1, performance2)
}

const _mergePerf = (performance1: PerformanceItem[], performance2: PerformanceItem[]) => {
    if (performance1.length > performance2.length) {
        const difference = performance1.length - performance2.length
        const completedPerformance = DateService.GenerateRemainingValues(difference, performance1, performance2)
        return _processMerging(performance1, completedPerformance)
    } else {
        const difference = performance2.length - performance1.length
        const completedPerformance = DateService.GenerateRemainingValues(difference, performance2, performance1)
        return _processMerging(performance2, completedPerformance)
    }
}


const _generateMissingData = (item: PerformanceItem[], dates: string[]) => {
    const missingPerformance: PerformanceItem[] = []
    dates.forEach(d => {
        missingPerformance.push({
            date: d,
            value: 0
        })
    })
    return missingPerformance.concat(item)
}

/*
   History: [
   {
     date: 01-01-2021,
     zim: 10,
     fb: 10,
     ....
   },
      {
     date: 01-02-2021,
     zim: 14,
     fb: 11,
     ....
   },
      {
     date: 01-03-2021,
     zim: 12,
     fb: 13,
     ....
   },
   ]
 */
// Asset compare algorithm
const CompareQuotesPerformance = async (portfolio: IPortfolioModel, quotes: string[], cashInflow: boolean = false) => {
    if (quotes.length === 0) return []
    const tickers = quotes.map(q => q.toUpperCase())
    const filteredItems = portfolio.assets.filter(a => tickers.includes(a.asset.type.ticker.toUpperCase()))
    const comparableItems = await GetItemsComparable(filteredItems, cashInflow)

    return _compareQuotes(comparableItems)
    // return comparableItems
}

const _compareQuotes = async (comparableItems: any[]) => {
    const longestAsset = {
        n: 0,
        comparableItem: {} as ComparableItem[]
    }
    comparableItems.forEach((i: any) => {
        if (i.length > longestAsset.n) {
            longestAsset.n = i.length
            longestAsset.comparableItem = i
        }
    })
    const merged: any[] = []

    longestAsset.comparableItem.forEach((item, index) => {
        const newItem: { [k: string]: any } = {}

        newItem.date = item.date
        newItem[item.keyName] = item.value
        comparableItems.forEach((p) => {
            const found = p.find((searchItem: any) => searchItem.date === item.date)
            newItem.zero = 0
            if (found) {
                newItem[found.keyName] = found.value
            } else {
                newItem[p[0].keyName] = null
            }
        })
        merged.push(newItem)
    })

    return merged
}

interface ComparableChart {
    ticker: string,
    history: History[]
}

const GetComparedQuotesWithBenchmark = async (portfolio: IPortfolioModel, quotes: string[], benchmark: string): Promise<ComparableChart[]> => {
    if (quotes.length === 0) return []
    const tickers = quotes.map(q => q.toUpperCase())
    const filteredItems = portfolio.assets.filter(a => tickers.includes(a.asset.type.ticker.toUpperCase()))
    const benchmarkAsset = await AssetService.GetAssetByTicker(benchmark)
    const result: ComparableChart[] = []
    const merged: any[] = []

    const assetWithLongestHistory = {
        ticker: '',
        date: await DateService.GetCurrentDateYMD()
    }
    filteredItems.forEach(item => {
        const buyPoint = new Date(item.averageBuySeries[0].date)
        const theOldestItem = new Date(assetWithLongestHistory.date)
        if (buyPoint < theOldestItem) {
            assetWithLongestHistory.date = item.averageBuySeries[0].date
            assetWithLongestHistory.ticker = item.asset.type.ticker
        }
    })

    try {
        filteredItems.forEach(item => {
            const sliceIndex = item.asset.history.findIndex(a => a.date === assetWithLongestHistory.date)

            const benchmarkSliceIndex = benchmarkAsset.history.findIndex(a => a.date === assetWithLongestHistory.date)
            benchmarkAsset.history = benchmarkAsset.history.slice(benchmarkSliceIndex)
            item.asset.history = item.asset.history.slice(sliceIndex)

            item.asset.history.forEach((a, index) => {
                const newItem: { [k: string]: any } = {}
                newItem.date = a.date
                newItem[item.asset.type.ticker] = a.close
                try {
                    newItem[benchmarkAsset.type.ticker] = benchmarkAsset.history[index].close
                } catch (e) {
                    newItem[benchmarkAsset.type.ticker] = benchmarkAsset.history[index - 1].close
                }
                newItem.zero = 0
                merged.push(newItem)
            })
        })
    } catch (e) {
        return merged
    }

    return merged
}


interface IMax {
    len: number,
    p: Performance
}

const MergeBenchmarkWithOther = async (benchmarkPerformance: Performance, otherPerformance: Performance) => {
    const comparableItems = await GetPerformanceComparable([benchmarkPerformance, otherPerformance])
    return _compareQuotes(comparableItems)
}

const MergeListPerformances = async (performances: Performance[]) => {
    const comparableItems = await GetPerformanceComparable(performances)
    return _compareQuotes(comparableItems)
}


const ComputeQuotesComparison = async (performances: Performance[]) => {
    const comparisonHistory: any[] = []
    const max: IMax = {
        len: 0,
        p: {} as Performance
    }
    const overallAssets = performances.length - 1
    performances.forEach(p => {
        if (p.performance.length > max.len) {
            max.p = p
        }
    })
    max.p.performance.forEach((perf, index) => {
        const newItem: { [k: string]: any } = {
            date: perf.date,
        }
        newItem[max.p.name] = perf.value
    })
}

const GetItemsPerformance = async (items: IPortfolioItem[], cashInflow: boolean = false) => {
    const performance: Performance[] = []
    for (const item of items) {
        if (!cashInflow) {
            const perf = await GetAssetPerformance(item)
            performance.push({
                name: item.asset.type.ticker,
                performance: perf
            })
        } else {

            const perf = await GetAssetPerformanceWithoutCashInflow(item)
            performance.push({
                name: item.asset.type.ticker,
                performance: perf
            })
        }
    }
    return performance
}

const GetItemsComparable = async (items: IPortfolioItem[], cashInflow: boolean = false) => {
    const all: any[] = []
    for (const item of items) {
        const comparableItem: ComparableItem[] = []
        if (!cashInflow) {
            const perf = await GetAssetPerformance(item)
            perf.forEach(p => {
                comparableItem.push({
                    date: p.date,
                    keyName: item.asset.type.ticker,
                    value: p.value
                })
            })
            all.push(comparableItem)
        } else {
            const perf = await GetAssetPerformanceWithoutCashInflow(item)
            perf.forEach(p => {
                comparableItem.push({
                    date: p.date,
                    keyName: item.asset.type.ticker,
                    value: p.value
                })
            })
            all.push(comparableItem)
        }
    }
    return all
}

const GetPerformanceComparable = async (performanceList: Performance[]) => {
    const all: any[] = []
    for (const assetPerformance of performanceList) {
        const comparableItem: ComparableItem[] = []
        assetPerformance.performance.forEach(p => {
            comparableItem.push({
                date: p.date,
                keyName: assetPerformance.name,
                value: p.value
            })
        })
        all.push(comparableItem)

    }
    return all
}

const GetTradesStats = async (email: string) => {
    const trades = await TradeService.GetAllTradesByUser(email)
    const dates = trades.map(t => t.executionDate)
    return dates
}


const months = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];


export interface GroupedTrade {
    price: number,
    amount: number,
    month: string
}

const OverallStats = async (email: string) => {
    const portfolioModels = await PortfolioService.GetAllUserPortfolios(email, false)
    const trades = await TradeService.GetAllTradesByUser(email)
    // if (!portfolioModels) return handleError("Portfolio cannot be load")
    const groupedTrades: GroupedTrade[] = []

    months.forEach(month => {
        groupedTrades.push({
            amount: 0,
            price: 0,
            month
        })
    })

    trades.forEach(trade => {
        const currentMonth = months[new Date(trade.executionDate).getMonth()]
        const existingItem = groupedTrades.find(g => g.month === currentMonth)
        existingItem.price += trade.overallPrice
        existingItem.amount += trade.amount
    })

    const generalStats: GeneralStatistics = {
        trades: groupedTrades,
        assetsCount: 0,
        tradesCount: 0,
        totalBalanceInUSD: 0,
        currencies: [] as PortfolioCurrency[],
        performance: 0,
        theMostExpensiveAsset: {
            name: '',
            value: 0
        },
        theLeastExpensiveAsset: {
            name: '',
            value: 1000000
        },
        theMostTradedAsset: {
            name: '',
            value: 0
        },
        assetWithMostShares: {
            name: '',
            value: 0
        },
        theMostTradedSector: {
            name: '',
            values: [] as StatsItem[]
        },
        theMostTradedCountry: {
            name: '',
            values: [] as StatsItem[],
        },
        concreteStats: {
            stats: [] as any[]
        }
    }

    if (!portfolioModels) return generalStats

    portfolioModels.forEach(portfolio => {
        generalStats.assetsCount += portfolio.assets.length
        generalStats.tradesCount += portfolio.tradesCount
        generalStats.totalBalanceInUSD += portfolio.balance
        generalStats.currencies.push(portfolio.currency)

        const concreteStat: PortfolioStatistics = {
            name: portfolio.name,
            assetsCount: portfolio.assets.length,
            tradesCount: portfolio.tradesCount,
            totalBalanceInUSD: portfolio.balance,
            currencies: [portfolio.currency],
            performance: 0,
            theMostExpensiveAsset: {
                name: '',
                value: 0
            },
            theLeastExpensiveAsset: {
                name: '',
                value: 1000000
            },
            theMostTradedAsset: {
                name: '',
                value: 0
            },
            assetWithMostShares: {
                name: '',
                value: 0
            },
            theMostTradedSector: {
                name: '',
                values: [] as StatsItem[]
            },
            theMostTradedCountry: {
                name: 'AA',
                values: [] as StatsItem[],
            },
        }

        portfolio.assets.forEach(portfolioItem => {
            if (portfolioItem.asset.type.price > concreteStat.theMostExpensiveAsset.value) {
                concreteStat.theMostExpensiveAsset.name = portfolioItem.asset.type.ticker
                concreteStat.theMostExpensiveAsset.value = portfolioItem.asset.type.price
            }
            if (portfolioItem.asset.type.price < concreteStat.theLeastExpensiveAsset.value) {
                concreteStat.theLeastExpensiveAsset.name = portfolioItem.asset.type.ticker
                concreteStat.theLeastExpensiveAsset.value = portfolioItem.asset.type.price
            }
            if (portfolioItem.tradesPerAsset > concreteStat.theMostTradedAsset.value) {
                concreteStat.theMostTradedAsset.name = portfolioItem.asset.type.ticker
                concreteStat.theMostTradedAsset.value = portfolioItem.tradesPerAsset
            }
            if (parseInt(portfolioItem.amount, 10) > concreteStat.assetWithMostShares.value) {
                concreteStat.assetWithMostShares.name = portfolioItem.asset.type.ticker
                concreteStat.assetWithMostShares.value = parseInt(portfolioItem.amount, 10)
            }

            // ******************************* general *******************************
            if (portfolioItem.asset.type.price > generalStats.theMostExpensiveAsset.value) {
                generalStats.theMostExpensiveAsset.name = portfolioItem.asset.type.ticker
                generalStats.theMostExpensiveAsset.value = portfolioItem.asset.type.price
            }
            if (portfolioItem.asset.type.price < generalStats.theLeastExpensiveAsset.value) {
                generalStats.theLeastExpensiveAsset.name = portfolioItem.asset.type.ticker
                generalStats.theLeastExpensiveAsset.value = portfolioItem.asset.type.price
            }
            if (portfolioItem.tradesPerAsset > generalStats.theMostTradedAsset.value) {
                generalStats.theMostTradedAsset.name = portfolioItem.asset.type.ticker
                generalStats.theMostTradedAsset.value = portfolioItem.tradesPerAsset
            }
            if (parseInt(portfolioItem.amount, 10) > generalStats.assetWithMostShares.value) {
                generalStats.assetWithMostShares.name = portfolioItem.asset.type.ticker
                generalStats.assetWithMostShares.value = parseInt(portfolioItem.amount, 10)
            }
            const type = portfolioItem.asset.type as IStockType
            if (type.sector) {
                ComputationService.processValue(concreteStat.theMostTradedCountry.values, {
                    name: type.country,
                    value: portfolioItem.totalCashInvested
                })
                ComputationService.processValue(concreteStat.theMostTradedSector.values, {
                    name: type.sector,
                    value: portfolioItem.totalCashInvested
                })

                // ******************************* general *******************************
                ComputationService.processValue(generalStats.theMostTradedCountry.values, {
                    name: type.country,
                    value: portfolioItem.totalCashInvested
                })
                ComputationService.processValue(generalStats.theMostTradedSector.values, {
                    name: type.sector,
                    value: portfolioItem.totalCashInvested
                })
            }
        })
        concreteStat.theMostTradedCountry.name = ComputationService
            .getBiggerValue(concreteStat.theMostTradedCountry.values).name
        concreteStat.theMostTradedSector.name = ComputationService
            .getBiggerValue(concreteStat.theMostTradedSector.values).name

        // ******************************* general *******************************
        generalStats.theMostTradedCountry.name = ComputationService
            .getBiggerValue(generalStats.theMostTradedCountry.values).name
        generalStats.theMostTradedSector.name = ComputationService
            .getBiggerValue(generalStats.theMostTradedSector.values).name

        generalStats.concreteStats.stats.push(concreteStat)
    })
    return generalStats
}

interface Allocation {
    ticker: string, // ticker of asset
    weight: number // asset's wight
}

interface Allocations {
    name: string, // the name of certain allocation (e.g. 'US' if the allocation type is Sector)
    values: Allocation[], // list of all weighted assets
    sum: number, // sum of all values
}

const GetPortfolioSectorAllocation = async (email: string, portfolioName: string) => {
    const portfolio = await UserService.GetUserPortfolio(email, portfolioName)
    if (!portfolio) return handleError(`Portfolio with name ${portfolioName} doesn't exist`)
    const sectors: Allocations[] = []
    for (const assetItem of portfolio.assets) {
        if (assetItem.asset.type.name === 'EQUITY') {
            const assetType = assetItem.asset.type as IStockType
            // potreba zjistit, zde existuje jak setkro, tak i dany asset
            const existingSector = sectors.find(a => a.name === assetType.sector)
            if (existingSector) { // sector exist
                sectors[sectors.findIndex(a => a.name === assetType.sector)].values.push({
                    ticker: assetItem.asset.type.ticker,
                    weight: assetItem.totalCashInvested,
                })
                sectors[sectors.findIndex(a => a.name === assetType.sector)].sum += assetItem.totalCashInvested
            } else { // sector does not exist
                const newSector: Allocations = {
                    name: assetType.sector,
                    values: [{
                        ticker: assetItem.asset.type.ticker,
                        weight: assetItem.totalCashInvested,
                    }],
                    sum: assetItem.totalCashInvested
                }
                sectors.push(newSector)
            }
        } else if (assetItem.asset.type.name === "ETF") {
            // u fondu mozna bude sepcialni sektor "Other", ktery bude obsahovat zbytek
            let holdingsSumWeight = 0
            const assetType = assetItem.asset.type as IFundType
            for (const holding of assetType.holdings) {
                const currentAsset = await AssetService.GetAssetByTicker(holding.ticker)
                if (currentAsset === null) break
                const currentAssetType = currentAsset.type as IStockType
                const existingSector = sectors.find(a => a.name === currentAssetType.sector)
                const relativeWeight = holding.weightPercentage / 100
                holdingsSumWeight += relativeWeight
                if (existingSector) {
                    sectors[sectors.findIndex(a => a.name === currentAssetType.sector)].values.push({
                        ticker: currentAsset.type.ticker,
                        weight: ComputationService.round(relativeWeight * assetItem.totalCashInvested, 0)
                    })
                    sectors[sectors.findIndex(a => a.name === currentAssetType.sector)].sum += ComputationService.round(relativeWeight * assetItem.totalCashInvested, 0)
                } else {
                    const newAssetSector: Allocations = {
                        name: currentAssetType.sector,
                        values: [{
                            ticker: currentAsset.type.ticker,
                            weight: ComputationService.round(relativeWeight * assetItem.totalCashInvested, 0)
                        }],
                        sum: ComputationService.round(relativeWeight * assetItem.totalCashInvested, 0)
                    }
                    sectors.push(newAssetSector)
                }
            }

            const otherWeight = 1 - holdingsSumWeight
            const newOtherSector: Allocations = {
                name: 'Other',
                values: [{
                    ticker: '',
                    weight: ComputationService.round(otherWeight * assetItem.totalCashInvested, 0)
                }],
                sum: ComputationService.round(otherWeight * assetItem.totalCashInvested, 0)
            }
            sectors.push(newOtherSector)
        }
    }
    return sectors
}

const GetPortfolioCountryAllocation = async (email: string, portfolioName: string) => {
    const portfolio = await UserService.GetUserPortfolio(email, portfolioName)
    if (!portfolio) return handleError(`Portfolio with name ${portfolioName} doesn't exist`)
    const countries: Allocations[] = []
    for (const assetItem of portfolio.assets) {
        if (assetItem.asset.type.name === 'EQUITY') {
            const assetType = assetItem.asset.type as IStockType
            // potreba zjistit, zde existuje jak setkro, tak i dany asset
            const existingSector = countries.find(a => a.name === assetType.country)
            if (existingSector) { // sector exist
                countries[countries.findIndex(a => a.name === assetType.country)].values.push({
                    ticker: assetItem.asset.type.ticker,
                    weight: assetItem.totalCashInvested,
                })
                countries[countries.findIndex(a => a.name === assetType.country)].sum += assetItem.totalCashInvested
            } else { // sector does not exist
                const newSector: Allocations = {
                    name: assetType.country,
                    values: [{
                        ticker: assetItem.asset.type.ticker,
                        weight: assetItem.totalCashInvested,
                    }],
                    sum: assetItem.totalCashInvested
                }
                countries.push(newSector)
            }
        } else if (assetItem.asset.type.name === "ETF") {
            // u fondu mozna bude sepcialni sektor "Other", ktery bude obsahovat zbytek
            let holdingsSumWeight = 0
            const assetType = assetItem.asset.type as IFundType
            for (const holding of assetType.holdings) {
                const currentAsset = await AssetService.GetAssetByTicker(holding.ticker)
                if (currentAsset === null) break

                const currentAssetType = currentAsset.type as IStockType
                const existingSector = countries.find(a => a.name === currentAssetType.country)
                const relativeWeight = holding.weightPercentage / 100
                holdingsSumWeight += relativeWeight
                if (existingSector) {
                    countries[countries.findIndex(a => a.name === currentAssetType.country)].values.push({
                        ticker: currentAsset.type.ticker,
                        weight: ComputationService.round(relativeWeight * assetItem.totalCashInvested, 0)
                    })
                    countries[countries.findIndex(a => a.name === currentAssetType.country)].sum += ComputationService.round(relativeWeight * assetItem.totalCashInvested, 0)
                } else {
                    const newAssetSector: Allocations = {
                        name: currentAssetType.country,
                        values: [{
                            ticker: currentAsset.type.ticker,
                            weight: ComputationService.round(relativeWeight * assetItem.totalCashInvested, 0)
                        }],
                        sum: ComputationService.round(relativeWeight * assetItem.totalCashInvested, 0)
                    }
                    countries.push(newAssetSector)
                }
            }

            const otherWeight = 1 - holdingsSumWeight
            const newOtherSector: Allocations = {
                name: 'Other',
                values: [{
                    ticker: '',
                    weight: ComputationService.round(otherWeight * assetItem.totalCashInvested, 0)
                }],
                sum: ComputationService.round(otherWeight * assetItem.totalCashInvested, 0)
            }
            countries.push(newOtherSector)
        }
    }
    return countries
}

const GetPortfolioIndustryAllocation = async (email: string, portfolioName: string) => {
    const portfolio = await UserService.GetUserPortfolio(email, portfolioName)
    if (!portfolio) return handleError(`Portfolio with name ${portfolioName} doesn't exist`)
    const industries: Allocations[] = []
    for (const assetItem of portfolio.assets) {
        if (assetItem.asset.type.name === 'EQUITY') {
            const assetType = assetItem.asset.type as IStockType
            // potreba zjistit, zde existuje jak setkro, tak i dany asset
            const existingSector = industries.find(a => a.name === assetType.industry)
            if (existingSector) { // sector exist
                industries[industries.findIndex(a => a.name === assetType.industry)].values.push({
                    ticker: assetItem.asset.type.ticker,
                    weight: assetItem.totalCashInvested,
                })
                industries[industries.findIndex(a => a.name === assetType.industry)].sum += assetItem.totalCashInvested
            } else { // sector does not exist
                const newSector: Allocations = {
                    name: assetType.industry,
                    values: [{
                        ticker: assetItem.asset.type.ticker,
                        weight: assetItem.totalCashInvested,
                    }],
                    sum: assetItem.totalCashInvested
                }
                industries.push(newSector)
            }
        } else if (assetItem.asset.type.name === "ETF") {
            // u fondu mozna bude sepcialni sektor "Other", ktery bude obsahovat zbytek
            let holdingsSumWeight = 0
            const assetType = assetItem.asset.type as IFundType
            for (const holding of assetType.holdings) {
                const currentAsset = await AssetService.GetAssetByTicker(holding.ticker)
                if (currentAsset === null) break

                const currentAssetType = currentAsset.type as IStockType
                const existingSector = industries.find(a => a.name === currentAssetType.industry)
                const relativeWeight = holding.weightPercentage / 100
                holdingsSumWeight += relativeWeight
                if (existingSector) {
                    industries[industries.findIndex(a => a.name === currentAssetType.industry)].values.push({
                        ticker: currentAsset.type.ticker,
                        weight: ComputationService.round(relativeWeight * assetItem.totalCashInvested, 0)
                    })
                    industries[industries.findIndex(a => a.name === currentAssetType.industry)].sum += ComputationService.round(relativeWeight * assetItem.totalCashInvested, 0)
                } else {
                    const newAssetSector: Allocations = {
                        name: currentAssetType.industry,
                        values: [{
                            ticker: currentAsset.type.ticker,
                            weight: ComputationService.round(relativeWeight * assetItem.totalCashInvested, 0)
                        }],
                        sum: ComputationService.round(relativeWeight * assetItem.totalCashInvested, 0)
                    }
                    industries.push(newAssetSector)
                }
            }

            const otherWeight = 1 - holdingsSumWeight
            const newOtherSector: Allocations = {
                name: 'Other',
                values: [{
                    ticker: '',
                    weight: ComputationService.round(otherWeight * assetItem.totalCashInvested, 0)
                }],
                sum: ComputationService.round(otherWeight * assetItem.totalCashInvested, 0)
            }
            industries.push(newOtherSector)
        }
    }
    return industries
}


const GetPortfolioAssetTypeAllocation = async (email: string, portfolioName: string) => {
    const portfolio = await UserService.GetUserPortfolio(email, portfolioName)
    if (!portfolio) return handleError(`Portfolio with name ${portfolioName} doesn't exist`)
    const assetTypes: Allocations[] = []
    for (const assetItem of portfolio.assets) {
        const assetType = assetItem.asset.type as IStockType
        // potreba zjistit, zde existuje jak setkro, tak i dany asset
        const existingType = assetTypes.find(a => a.name === assetType.name)
        if (existingType) { // sector exist
            assetTypes[assetTypes.findIndex(a => a.name === assetType.name)].values.push({
                ticker: assetItem.asset.type.ticker,
                weight: assetItem.totalCashInvested,
            })
            assetTypes[assetTypes.findIndex(a => a.name === assetType.name)].sum += assetItem.totalCashInvested
        } else { // sector does not exist
            const newSector: Allocations = {
                name: assetType.name,
                values: [{
                    ticker: assetItem.asset.type.ticker,
                    weight: assetItem.totalCashInvested,
                }],
                sum: assetItem.totalCashInvested
            }
            assetTypes.push(newSector)
        }
    }
    return assetTypes
}

const GetPortfolioAssetAllocation = async (email: string, portfolioName: string) => {
    const portfolio = await UserService.GetUserPortfolio(email, portfolioName)
    if (!portfolio) return handleError(`Portfolio with name ${portfolioName} doesn't exist`)
    const assets: Allocations[] = []
    for (const assetItem of portfolio.assets) {
        const assetType = assetItem.asset.type as IStockType
        // potreba zjistit, zde existuje jak setkro, tak i dany asset
        const existingType = assets.find(a => a.name === assetType.ticker)
        if (existingType) { // sector exist
            assets[assets.findIndex(a => a.name === assetType.ticker)].values.push({
                ticker: assetItem.asset.type.ticker,
                weight: assetItem.totalCashInvested,
            })
            assets[assets.findIndex(a => a.name === assetType.ticker)].sum += assetItem.totalCashInvested
        } else { // sector does not exist
            const newSector: Allocations = {
                name: assetType.ticker,
                values: [{
                    ticker: assetItem.asset.type.ticker,
                    weight: assetItem.totalCashInvested,
                }],
                sum: assetItem.totalCashInvested
            }
            assets.push(newSector)
        }
    }
    return assets
}

const CompareQuotesWithBenchmark = async (quotesPerformance: Performance, benchmarkItem: IPortfolioItem) => {
    // const perf = await GetAssetPerformanceWithoutCashInflow(benchmark)
    const startIndex = benchmarkItem.asset.history.findIndex(h => h.date === quotesPerformance.performance[0].date)
    // const benchmarkPerformance = await GetBenchmarkPerformanceWithoutCashInflow(benchmarkItem, startIndex)
    // return benchmarkPerformance
    return {}
}


const MockPerf = async (num: number) => {
    const stockA: Performance = {
        name: 'stockA',
        performance: [
            {
                date: '01-01-2022',
                value: 2
            },
            {
                date: '01-01-2022',
                value: 3
            },
            {
                date: '01-01-2022',
                value: 2
            },
            {
                date: '01-01-2022',
                value: 5
            },
            {
                date: '01-01-2022',
                value: 4
            },
        ]
    }
    const stockB: Performance = {
        name: 'stockB',
        performance: [
            {
                date: '01-01-2022',
                value: 22
            },
            {
                date: '01-01-2022',
                value: 31
            },
            {
                date: '01-01-2022',
                value: 6
            },
            {
                date: '01-01-2022',
                value: 12
            },
            {
                date: '01-01-2022',
                value: 40
            },
        ]
    }
    const stockC: Performance = {
        name: 'stockC',
        performance: [
            {
                date: '01-01-2022',
                value: 13
            },
            {
                date: '01-01-2022',
                value: 4
            },
            {
                date: '01-01-2022',
                value: 3
            },
            {
                date: '01-01-2022',
                value: 14
            },
            {
                date: '01-01-2022',
                value: 11
            },
        ]
    }
    const list = [stockA, stockB, stockC]
    return list.slice(0, num)
}

const _prepare = (perf: Performance) => {
    return perf.performance.map(p => p.value)
}

type CorrelationItem = number[]

interface ICorrelation {
    labels: string[],
    data: CorrelationItem[],
    history: any
}


const GetQuotesCorrelation = async (from: string, to: string, quotes: string[], portfolioModel: IPortfolioModel) => {

    const assetList: IAssetModel[] = []
    for (const quote of quotes) {
        const asset = await AssetService.GetAssetByTicker(quote)
        asset.history = asset.history.slice(-280)
        assetList.push(asset)
    }

    const values = assetList.map(a => a.history.map(h => h.close))
    const labels = assetList.map(a => a.type.ticker)

    const mergedHistory: any[] = []

    assetList[0].history.forEach((historyItem, historyIndex) => {
        const newItem: { [k: string]: any } = {}
        newItem.date = historyItem.date
        newItem.zero = 0
        mergedHistory.push(newItem)
    })
    mergedHistory.forEach((item) => {
        assetList.forEach(asset => {
            asset.history.forEach((historyItem, index) => {
                mergedHistory[index][asset.type.ticker] = historyItem.close
            })
        })
    })


    try {
        const correlationMatrix: any[] = []
        values.forEach((stockDataRow, stockDataRowIndex) => {
            const correlationRow: any[] = []
            values.forEach((_, dateItemIndex) => {
                const res = calculateCorrelation(stockDataRow, values[dateItemIndex])
                if (res === 0 || res === 1) {
                    correlationRow.push(res.toFixed(0))
                } else {
                    correlationRow.push(res.toFixed(3))
                }
            })
            correlationMatrix.push(correlationRow)
        })

        const correlation: ICorrelation = {
            labels,
            data: correlationMatrix,
            history: mergedHistory,
        }
        return correlation

    } catch (e) {
        Logger.error(e)
        return {
            'err': e
        }
    }
}


interface VolatilityItem {
    date: string,
    value: number,

}

interface Volatility {
    quote: string,
    volatility: VolatilityItem[],
    history: VolatilityItem[],
}


const _computeStdDev = (range: number[]) => {
    const mean = range.reduce((a, b) => a + b) / range.length

}

const GetQuoteVolatility = async (quote: string, range: number, period: number, method: string) => {
    const asset = await AssetService.GetAssetByTicker(quote)
    if (!asset) return handleError("Asset not found")
    const finalSlice = -period - range

    asset.history = asset.history.slice(finalSlice) // 10 days sliding window
    const volatility: Volatility = {
        quote,
        volatility: [],
        history: asset.history.map(a => {
            return {
                date: a.date,
                value: a.close
            }
        }),
    }

    for (let i = 0; i < asset.history.length; i += range) {
        const slicedHistory = asset.history
            .slice(i, i + range)
            .map(h => h.close)
        if (!asset.history[i + range]) break

        const date = asset.history[i + range].date
        const stdev = parseFloat(math.std(slicedHistory).toFixed(2))
        volatility.volatility.push({
            date,
            value: stdev
        })
    }

    return volatility
}

const GetQuotesPl = async (quotes: string[], portfolioModel: IPortfolioModel) => {
    const all: any[] = []
    const overallPL = 0
    portfolioModel.assets.forEach(portfolioItem => {
        const currentPL = 0
        const asset = {
            name: portfolioItem.asset.type.ticker,
            // @ts-ignore
            dates: [],
        }
        portfolioItem.averageBuySeries.forEach((abs, index, arr) => {
            asset.dates.push(abs.date)
            if (arr[index + 1]) {
                return
            }
            const y1 = parseInt((abs.amountInvested / abs.sharesBought).toFixed(0), 10)
            const y2 = portfolioItem.asset.history.slice(-1)[0].close
            const pl = (y2 - y1) / y1
        })
        all.push(asset)
    })
    return all
}


export const StatsService = {
    OverallStats,
    GetPerformance,
    GetPortfolioPerformance,
    ComputeAndMergePerformance,
    GetAssetPerformance,
    GetPortfolioCountryAllocation,
    GetPortfolioIndustryAllocation,
    GetPortfolioSectorAllocation,
    GetItemsPerformance,
    MergePerformance,
    MergeAllPerformance,
    GetAssetPerformanceWithoutCashInflow,
    MergeAssetPerformanceWithoutCashInflow,
    MergeQuotesPerformance,
    GetPortfolioAssetTypeAllocation,
    GetPortfolioAssetAllocation,
    CompareQuotesPerformance,
    GetTradesStats,
    CompareQuotesWithBenchmark,
    GetMergedAverageBuySeries,
    ConstructBenchmark,
    MergeBenchmarkWithOther,
    MergeListPerformances,
    GetComparedQuotesWithBenchmark,
    GetQuotesPerformance,
    GetQuotesCorrelation,
    GetQuoteVolatility,
    GetQuotesPl,
}
