import Logger from "../logger/logger";

export const handleError = (err: any): null => {
    Logger.error(err);
    return null;
}

export const print = (data: any) => {
    Logger.info(data);
}