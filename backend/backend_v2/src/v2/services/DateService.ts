import {AverageBuySeries} from "../models/PortfolioModel";
import {PerformanceItem} from "./StatsService";


const _getDate = () => {
    const today = new Date();
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    return date + ' ' + time;
}

const GetCurrentDateYMDSync = () => {
    const today = new Date();
    return today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate()
}

const GetCurrentDateYMD = async () => {
    const today = new Date();
    return today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate()
}

const GetCurrentDate = async () => {
    return _getDate()
}

const GetCurrentDateSync = () => {
    return _getDate()
}

export const GetDate = (d: Date | null): string => {
    if (!d) return '';
    const dd = String(d.getDate()).padStart(2, '0');
    const mm = String(d.getMonth() + 1).padStart(2, '0');
    const yyyy = d.getFullYear();

    return yyyy + '-' + mm + '-' + dd
}

export const GetReversedDate = (d: Date | null): string => {
    if (!d) return '';
    const dd = String(d.getDate()).padStart(2, '0');
    const mm = String(d.getMonth() + 1).padStart(2, '0');
    const yyyy = d.getFullYear();

    return mm + '-' + dd + '-' + yyyy
}

export const SortByStringDate = (averageBuySeries: AverageBuySeries[]) => {
    return averageBuySeries.slice().sort((a, b) => {
        const aa = new Date(b.date)
        const bb = new Date(a.date)
        return bb.getTime() - aa.getTime()
    })
}

// Author: https://stackoverflow.com/questions/4413590/javascript-get-array-of-dates-between-2-dates
const getDaysArray = (start: Date, end: Date) => {
    const arr = []
    for (const dt = new Date(start); dt <= end; dt.setDate(dt.getDate() + 1)) {
        arr.push(new Date(dt));
    }
    return arr;
};


const GenerateDateBetween = async (date1: string, date2: string) => {
    const daylist = getDaysArray(new Date(date1), new Date(date2));
    const datesBetween: string[] = []
    daylist.map((v) => {
        datesBetween.push(v.toISOString().slice(0, 10))
    })
    datesBetween.pop()
    return datesBetween
}

const GenerateRemainingValues = (count: number, source: PerformanceItem[], target: PerformanceItem[]) => {
    const generatedItems: PerformanceItem[] = []
    for (let i = 0; i < count; i++) {
        generatedItems.push({
            date: source[i].date,
            value: 0
        })
    }
    return generatedItems.concat(target)
}

export const DateService = {
    GetCurrentDate,
    GetDate,
    GetReversedDate,
    SortByStringDate,
    GetCurrentDateSync,
    GetCurrentDateYMDSync,
    GetCurrentDateYMD,
    GenerateDateBetween,
    GenerateRemainingValues,
}