import {IAssetModel} from "../models/AssetModel";
import {UserModel} from "../models/UserModel";
import {ITradeModel} from "../models/TradeModel";
import {IPortfolioItem, IPortfolioModel, PortfolioCurrency, PortfolioModel} from "../models/PortfolioModel";
import {UserService} from "./UserService";
import {handleError} from "./index";
import {DateService} from "./DateService";


const SetAverageBuy = (portfolioItem: IPortfolioItem, trade: ITradeModel) => {
    if (!portfolioItem.averageBuySeries) { // first time
        portfolioItem.averageBuySeries = []
    }

    const tradesAlready = portfolioItem.averageBuySeries.length
    if (tradesAlready === 0) {
        portfolioItem.averageBuySeries.push({
            date: DateService.GetDate(new Date(trade.executionDate)),
            averageAssetPrice: parseFloat(trade.pricePerAsset.toFixed(2)),
            amountInvested: trade.overallPrice,
            sharesBought: trade.amount
        })
        return
    }
    const sum = {
        sumAmountInvested: 0,
        sumSharesBought: 0
    }
    portfolioItem.averageBuySeries.forEach(series => {
        sum.sumAmountInvested += series.amountInvested
        sum.sumSharesBought += series.sharesBought
    })
    const mezi = parseFloat(((sum.sumAmountInvested + trade.overallPrice) / (sum.sumSharesBought + trade.amount)).toFixed(2))
    // todo To razeni mozna muze delat bordel, protoze se spocita prumerna cena na zaklade tech predchozich hodnot, ulozi se, a pak se to serai, coz muze
    // todo zamichat tim poradim a tim padem "invalidovat" tu spoctenou prumernou cenu
    portfolioItem.averageBuySeries.push({
        date: DateService.GetDate(new Date(trade.executionDate)),
        averageAssetPrice: mezi,
        amountInvested: trade.overallPrice,
        sharesBought: trade.amount,
    })
    portfolioItem.averageBuySeries = DateService.SortByStringDate(portfolioItem.averageBuySeries)
}

const UpdatePortfolio = async (email: string, portfolioName: string, trade: ITradeModel, asset: IAssetModel) => {
    let portfolio: IPortfolioModel;
    const validUserPortfolio = await UserService.GetUserPortfolio(email, portfolioName);

    if (validUserPortfolio) {
        portfolio = validUserPortfolio
    } else {
        portfolioName = 'Default';
        portfolio = await UserService.GetUserPortfolio(email, portfolioName);
    }
    const assetAlreadyExists = portfolio.assets.find(item => item.asset._id.equals(asset._id));
    if (!assetAlreadyExists) {
        const portfolioItem: IPortfolioItem = {
            asset,
            tradesPerAsset: 1,
            amount: trade.amount.toString(),
            totalCashInvested: trade.overallPrice,
        }
        SetAverageBuy(portfolioItem, trade)

        return UserModel.findOneAndUpdate({email, 'portfolio.name': portfolioName}, {
            $push: {
                'portfolio.$.assets': portfolioItem,
            },
            $inc: {
                'portfolio.$.tradesCount': 1
            }
        })
    }
    assetAlreadyExists.amount = (parseInt(assetAlreadyExists.amount, 10) + trade.amount).toString();

    const user = await UserService.GetUserByPortfolioWithPopulatedAsset(email, portfolioName);
    user.portfolio.forEach(p => {
        if (p.name === portfolioName) {
            p.tradesCount++;
            p.assets.forEach(a => {
                if (a.asset.name === asset.name) {
                    a.tradesPerAsset++;
                    a.amount = assetAlreadyExists.amount
                    a.totalCashInvested += trade.overallPrice
                    SetAverageBuy(a, trade)
                }
            })
        }
    })
    return await UserService.UpdateWholeUserPortfolio(email, portfolioName, user.portfolio);

}

const CreateNewPortfolio = async (email: string, portfolio: IPortfolioModel): Promise<IPortfolioModel> => {
    const existingPortfolio = await UserService.GetUserPortfolio(email, portfolio.name)
    if (existingPortfolio) return handleError("Portfolio exists")

    const createdPortfolio = await PortfolioModel.create(portfolio)
    const updatedUser = await UserService.UpdateUserPortfolio(email, createdPortfolio)
    if (!updatedUser) return handleError("User was not updated successful")

    return createdPortfolio;
}

const GetAllUserPortfolios = async (email: string, history = false): Promise<IPortfolioModel[]> => {
    // const user = await UserService.GetAllAssetsInAllPortfolios(email) // get user by email
    const user = await UserService.GetUserByEmail(email) // get user by email
    if (user == null) {
        return handleError(user)
    }
    const all: IPortfolioModel[] = []
    for (const p of user.portfolio) {
        const currentPortfolio = await UserService.GetUserPortfolio(email, p.name, history)
        all.push(currentPortfolio)
    }
    return all;
}

const GetPortfolioByName = async (email: string, name: string): Promise<IPortfolioModel> => {
    const portfolioModels = await GetAllUserPortfolios(email, true)
    const foundPortfolio = portfolioModels.find(p => p.name === name)
    if (!foundPortfolio) return handleError(`Portfolio with name ${name} not found`)
    return foundPortfolio
}

const GetAssetInPortfolio = async (portfolio: IPortfolioModel, ticker: string) => {
    const found = portfolio.assets.find(a => a.asset.type.ticker.toUpperCase() === ticker.toUpperCase())
    if(!found) return handleError(`Asset with ticker ${ticker} not found in portfolio: ${portfolio.name}`)
    return found
}

const DeletePortfolio = async (email: string, portfolioName: string, newPortfolioName: string): Promise<any> => {
    const portfolioToDelete = await UserService.GetUserPortfolio(email, portfolioName);
    if (!portfolioToDelete) return handleError("Portfolio does not exists")

    await UserService.RemoveUserPortfolio(email, portfolioName, newPortfolioName)
    if (portfolioToDelete.name === "Default") return handleError("Default portfolio cannot be removed")

    return PortfolioModel.deleteOne({_id: portfolioToDelete._id});

}

const SetPortfolioCurrency = async (email: string, name: string, currency: PortfolioCurrency) => {
    const allPortfolios = await GetAllUserPortfolios(email)
    allPortfolios.forEach(portfolio => {
        if (portfolio.name === name) {
            portfolio.currency = currency
        }
    })
    return await UserService.UpdateAllPortfolios(email, allPortfolios)
}


const GetAllAssetsInPortfolio = async (email: string, name: string) => {
    return [] as IAssetModel[]
}
const GetAllTradesInPortfolio = async (email: string, name: string) => {
    return [] as ITradeModel[]
}

export const PortfolioService = {
    UpdatePortfolio,
    CreateNewPortfolio,
    GetAllUserPortfolios,
    DeletePortfolio,
    SetPortfolioCurrency,
    GetAllAssetsInPortfolio,
    GetAllTradesInPortfolio,
    GetPortfolioByName,
    GetAssetInPortfolio
}
