import {AssetModel, AssetWithChart, IAssetModel} from "../models/AssetModel";
import {handleError} from "./index";
import {History, IAssetSummary, YahooFinanceService} from "./external/YahooFinanceService";
import {IAssetType, ICryptoType, IFundType, IStockType} from "./AssetTypeService";
import mongoose from "mongoose";
import Logger from "../logger/logger";
import {IPortfolioModel} from "../models/PortfolioModel";
import {DateService} from "./DateService";
import {cacheManager} from "../cache/cacheManager";


export const allAssetsCacheKey = 'assets_all'

const _assembleAsset = (type: IAssetType, name: string): IAssetModel => {
    return {
        name,
        type,
    }
}

const AssembleNewAsset = async (fetchedAssetData: IAssetSummary) => {
    switch (fetchedAssetData.quoteType) {
        case 'ETF': {
            const assetType: IFundType = {
                family: fetchedAssetData.family,
                price: fetchedAssetData.price,
                name: fetchedAssetData.quoteType,
                holdings: fetchedAssetData.holdings,
                dividend: 'N/A',
                pe: fetchedAssetData.pe,
                pb: fetchedAssetData.pb,
                beta: fetchedAssetData.beta,
                marketCap: fetchedAssetData.marketCap,
                ticker: fetchedAssetData.symbol,
                currency: fetchedAssetData.currency
            }
            return _assembleAsset(assetType, fetchedAssetData.longName)
        }
        case 'CRYPTOCURRENCY': {
            const assetType: ICryptoType = {
                price: fetchedAssetData.price,
                name: fetchedAssetData.quoteType,
                ticker: fetchedAssetData.symbol,
                marketCap: fetchedAssetData.marketCap,
                exchange: fetchedAssetData.exchange,
                currency: fetchedAssetData.currency
            }
            return _assembleAsset(assetType, fetchedAssetData.longName)
        }
        case 'EQUITY': {
            const assetType: IStockType = {
                name: fetchedAssetData.quoteType,
                ticker: fetchedAssetData.symbol,
                price: fetchedAssetData.price,
                marketCap: fetchedAssetData.marketCap ? parseFloat(fetchedAssetData.marketCap.toLocaleString('de-DE')) : 'n/a',
                pe: fetchedAssetData.pe ? parseFloat(fetchedAssetData.pe.toFixed(1)) : 'n/a',
                pb: fetchedAssetData.pb ? parseFloat(fetchedAssetData.pb.toFixed(1)) : 'n/a',
                beta: fetchedAssetData.beta,
                dividend: 'N/A',
                sector: fetchedAssetData.sector,
                country: fetchedAssetData.region,
                industry: fetchedAssetData.industry,
                currency: fetchedAssetData.currency
            }
            return _assembleAsset(assetType, fetchedAssetData.longName)
        }
    }
}

export interface INewAsset {
    name: string,
}


const GetAllAssets = async () => {
    const cachedAssets = cacheManager.get(allAssetsCacheKey)
    if (cachedAssets) return cachedAssets

    const allAssets = AssetModel.find()
        .then((assets: object) => {
            return assets;
        })
        .catch((err: object) => {
            return handleError(err)
        })
    cacheManager.set(allAssetsCacheKey, allAssets)
    return allAssets
}

const GetAssetByTicker = async (ticker: string): Promise<IAssetModel> => {

    const cachedAsset = cacheManager.get<IAssetModel>(`asset_${ticker}`)
    if (cachedAsset) return cachedAsset

    try {
        const existingAsset = await AssetModel.findOne({'type.ticker': ticker.toString().toUpperCase()});
        if (!existingAsset) {
            const fetchedAssetData = await RequestAssetData(ticker)
            const asset = await AssembleNewAsset(fetchedAssetData)
            const history = await GetCompleteHistory(ticker)
            if (history) {
                asset.history = history
            }
            const createdAsset = await CreateNewAsset(asset)
            if (!createdAsset) return handleError("Asset to trade has not been created successfully")
            //
            cacheManager.set(`asset_${ticker}`, createdAsset)
            return createdAsset
        }
        cacheManager.set(`asset_${ticker}`, existingAsset)
        return existingAsset
    } catch (e) {
        return handleError(e)
    }

}

const GetAssetByField = async (field: any): Promise<IAssetModel> => {
    return AssetModel.findOne({field})
        .then((asset) => {
            return asset;
        })
        .catch((err: object) => {
            return handleError(err)
        })
}
// todo nutno updatovat historii
const GetAssetByObjectId = async (assetId: any, history = true): Promise<IAssetModel> => {
    const asset: IAssetModel = await AssetModel.findById(new mongoose.Types.ObjectId(assetId))
    if (history) {
        const today = await DateService.GetCurrentDateYMD()
        if (asset.updated === today) return asset
        const updatedHistory = await GetCompleteHistory(asset.type.ticker)
        return AssetModel.findOneAndUpdate({'type.ticker': asset.type.ticker}, {
            history: updatedHistory
        }, {
            new: true
        });

    }
    return {
        'name': asset.name,
        'type': asset.type
    }

    // return await AssetModel.findById(new mongoose.Types.ObjectId(assetId))
    //     .then((asset) => {
    //         if (history) {
    //             const today = DateService.GetCurrentDateYMDSync()
    //             if(asset.updated === today) return asset
    //             const updatedHistory = GetCompleteHistory(asset.type.ticker)
    //         }
    //         return {
    //             'name': asset.name,
    //             'type': asset.type
    //         }
    //     })
    //     .catch(err => {
    //         return handleError(err);
    //     })
}

const _createNewAsset = async (asset: object) => {
    cacheManager.del(allAssetsCacheKey)
    return AssetModel.create(asset)
        .then(createdAsset => {
            return createdAsset;
        })
        .catch(err => {
            return handleError(err)
        })
}

const CreateNewAsset = async (asset: IAssetModel) => {
    cacheManager.del(allAssetsCacheKey)
    return await _createNewAsset(asset)
}

const RequestAssetData = async (ticker: string): Promise<IAssetSummary> => {
    return await RequestSummaryData(ticker)
}

const RequestSummaryData = async (ticker: string) => {
    return await YahooFinanceService.GetAssetSummary(ticker);
}

const GetAssetPriceByDate = async (ticker: string, period: string) => {
    return await YahooFinanceService.GetHistoricalData(ticker, period);
}

const _fetchHistory = async (ticker: string) => {
    const interval = {
        begin: '01-01-2020',
        ytd: '01-01-20222',
        year: DateService.GetDate(new Date(new Date().setFullYear(new Date().getFullYear() - 1)))
    }
    Logger.info(interval.year)
    Logger.info('fetching history from API')
    const fetchedAssets = await YahooFinanceService.GetCompleteHistoricalData(ticker, interval.begin);
    if (!fetchedAssets) return handleError('No historical data found')

    return fetchedAssets
}

const GetCompleteHistory = async (ticker: string): Promise<History[]> => {
    const asset: IAssetModel = await AssetModel.findOne({'type.ticker': ticker.toString().toUpperCase()})

    if (!asset) {
        return await _fetchHistory(ticker)
    }

    const today = await DateService.GetCurrentDateYMD()
    Logger.info(`today: ${today}, assetUpdated: ${asset.updated}`)
    if (asset.updated === today) {
        return asset.history
    } else {
        const fetchedHistory = await _fetchHistory(ticker)
        const updatedAsset: IAssetModel = await AssetModel.findOneAndUpdate({'type.ticker': ticker}, {
            history: fetchedHistory
        }, {
            new: true
        })
        return updatedAsset.history

    }
    // if (asset.history.length !== 0) {
    //     Logger.info('fetching history from DB')
    //     const lastItem = asset.history.slice(-1)[0]
    //     const lastItemDat = `${lastItem.date.getMonth()}-${lastItem.date.getDay()}-${lastItem.date.getFullYear()}`
    //     const now = new Date()
    //     const nowDate = `${now.getMonth()}-${now.getDay()}-${now.getFullYear()}`
    //     if (lastItemDat === nowDate) {
    //         return asset.history
    //     }
    //     // need to fetch remaining data
    //     Logger.info('Fetching remaining history data')
    //     const fetchRemainingDate = await YahooFinanceService.GetCompleteHistoricalData(ticker, nowDate)
    //     const assetToUpdate: IAssetModel = await AssetModel.findOne({'type.ticker': ticker})
    //     assetToUpdate.history.concat(fetchRemainingDate)
    //     const updatedAsset: IAssetModel = await AssetModel.findOneAndUpdate({'type.ticker': ticker}, {
    //         history: assetToUpdate.history
    //     }, {
    //         new: true
    //     })
    //     return updatedAsset.history
    // }
}

const GetAssetById = async (id: any): Promise<IAssetModel> => {
    return AssetModel.findById(id)
}

const GetAssetWithChart = async (portfolio: IPortfolioModel, id: string): Promise<AssetWithChart> => {
    const asset = await GetAssetByObjectId(id)
    const portfolioAssetData = portfolio.assets.find(a => a.asset.type.ticker === asset.type.ticker)
    if (!asset || !portfolioAssetData) return handleError("Asset or PortfolioAssetData data not found")

    const assetWithChart: AssetWithChart = CreateAssetWithChart()
    assetWithChart.name = asset.name
    assetWithChart.type = asset.type
    assetWithChart._id = asset._id
    assetWithChart.category = asset.category

    const lastSeries = portfolioAssetData.averageBuySeries.slice(-1)[0]
    const lastHistory = asset.history.slice(1).slice(-2)

    const sign = Math.sign(lastHistory[0].open - lastHistory[1].close)
    assetWithChart.chart.trending = sign === 1 ? 'DOWN' : 'UP'

    assetWithChart.chart.buySeries = portfolioAssetData.averageBuySeries
    assetWithChart.chart.history = asset.history
    assetWithChart.chart.history.forEach((h) => {
        // @ts-ignore
        h.average = lastSeries?.averageAssetPrice
        const buyPoint = assetWithChart.chart.buySeries.find(b => b.date === h.date)
        if (buyPoint) {
            // @ts-ignore
            h.buy = h.close
        }
    })

    return assetWithChart

    // return {} as AssetWithChart
}

const CreateAssetWithChart = (): AssetWithChart => {
    return {
        chart: {
            buySeries: [],
            history: [],
            trending: 'UP'
        },
        type: undefined,
        name: undefined,
        _id: undefined,
        category: undefined
    }
}

const GetBenchmark = async (ticker: string) => {
    const benchmark = await YahooFinanceService.FetchBenchmark(ticker)
    if (!benchmark) return handleError("Asset not found")
    benchmark.history = await GetCompleteHistory(ticker)
    return benchmark
}
export const AssetService = {
    GetAllAssets,
    GetAssetByField,
    CreateNewAsset,
    RequestAssetData,
    GetAssetByTicker,
    RequestSummaryData,
    GetAssetPriceByDate,
    GetAssetByName: GetAssetByObjectId,
    GetCompleteHistory,
    GetAssetById,
    GetAssetWithChart,
    GetBenchmark,
}
