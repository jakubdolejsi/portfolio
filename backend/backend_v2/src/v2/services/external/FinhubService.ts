import FinhubAPI from '@stoqey/finnhub';

const apiKey = process.env.FINHUB_API_KEY || 'adad'

const finnhubAPI = new FinhubAPI(apiKey)

export interface IFinHubAsset {
    industry: string,
    marketCap: number,
    url: string
}

const GetCompanyByName = async (name: string) => {
    const data = await finnhubAPI.symbolLookup(name)
    return data;
}

const GetCompanyProfile = async (ticker: string): Promise<IFinHubAsset> => {
    const profile = await finnhubAPI.companyProfile2({symbol: ticker});
    return {
        industry: profile.finnhubIndustry,
        marketCap: profile.marketCapitalization,
        url: profile.weburl
    }
}


export const FinhubService = {
    GetCompanyByName,
    GetCompanyProfile,
}