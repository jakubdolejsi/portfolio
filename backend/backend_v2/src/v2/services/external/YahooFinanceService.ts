import yahooFinance from 'yahoo-finance2';
import {handleError} from "../index";
import {IAssetType} from "../AssetTypeService";
import {AssetService} from "../AssetService";
import {DateService, GetDate} from "../DateService";


export type IYahooAsset = {
    price: number,
    symbol: string,
    displayName: string,
    region: string,
    shortName: string,
    currency: string,
    quoteType: string,
    exchange: string,
}

export type Holding = {
    ticker: string,
    name: string,
    weightPercentage: number,
    asset: IAssetType | {},
}

export type History = {
    open: number,
    close: number,
    volume?: number
    date: string
}


export type IAssetSummary = {
    quoteType: string,
    symbol: string,
    shortName: string,
    longName: string,
    currency?: string,
    region: string,
    industry: string,
    assetName: string,
    exchange: string,
    family?: string,
    holdings?: Holding[],
    price?: number,
    marketCap: number,
    sector: string,
    pb: number,
    pe: number,
    beta: number,
}

interface BenchmarkPerformance {
    beta: number,
    alpha: number,
    sharpe: number,
    stdDev: number,
    rSquared: number,
    meanAnnualReturn: number
}

export interface BenchmarkType {
    ticker: string
}

export interface Benchmark {
    type: BenchmarkType,
    history: History[],
    performance: BenchmarkPerformance
}

// todo Bohuzel to tady musi byt, protoze ty tickery, co mi vyhodi TopHolding list, neexistuji
const TickerMapper = (inputTicker: string) => {
    if (inputTicker.includes('BRK')) {
        return inputTicker.replace('.', '-')
    } else if (inputTicker.includes('00700'))
        return '0700.hk'
    return inputTicker
}

const GetCompanyByTicker = async (ticker: string) => {
    const response = await yahooFinance.search(ticker);
    return response.quotes[0]
}

const GetCompanyByQuote = async (quote: string[]): Promise<IYahooAsset> => {
    // tslint:disable-next-line:no-console
    console.log('calling external API')
    // @ts-ignore
    const quoteResponse = (await yahooFinance.quote(quote.map(q => q.toLowerCase()), {
        fields: [
            'symbol',
            'displayName',
            'region',
            'currency',
            'shortName',
            'quoteType',
            'regularMarketPrice',
        ]
    }, {
        validateResult: false
    }))[0]
    return {
        quoteType: quoteResponse.quoteType,
        symbol: quoteResponse.symbol,
        displayName: quoteResponse.displayName,
        shortName: quoteResponse.shortName,
        currency: quoteResponse.currency,
        region: quoteResponse.region,
        price: quoteResponse.regularMarketPrice,
        exchange: quoteResponse.exchange,
    };
}

const GetAssetSummary = async (ticker: string): Promise<IAssetSummary> => {
    try {
        const result = await yahooFinance.quoteSummary(ticker, {
            modules: [
                "fundProfile", // ano
                "topHoldings", // ano
                "assetProfile",
                "quoteType",
                "summaryDetail",
                "price",
                'defaultKeyStatistics',
                'fundPerformance',
            ]
        }, {
            validateResult: false
        })

        const assetSummary: IAssetSummary = {
            quoteType: result.quoteType.quoteType,
            symbol: result.quoteType.symbol,
            shortName: result.quoteType.shortName,
            longName: result.quoteType.longName ? result.quoteType.longName : result.assetProfile.name,
            currency: result.summaryDetail.currency,
            region: result.assetProfile?.country,
            industry: result.assetProfile?.industry,
            sector: result.assetProfile?.sector,
            assetName: result.assetProfile.name,
            exchange: result.quoteType.exchange,
            price: result.price.regularMarketPrice,
            marketCap: result.price.marketCap,
            pb: result.defaultKeyStatistics?.priceToBook,
            pe: result.summaryDetail?.trailingPE,
            beta: result.summaryDetail.beta || result.fundPerformance.riskOverviewStatistics.riskStatistics[0].beta,
            family: null,
            holdings: null,
        }
        if (assetSummary.quoteType === 'ETF') {
            const holdings: Holding[] = []
            for (const holding of result.topHoldings.holdings) {
                const asset = await AssetService.GetAssetByTicker(TickerMapper(holding.symbol))
                if (!asset) {
                    holdings.push({
                        asset: {},
                        name: holding.holdingName,
                        ticker: holding.symbol,
                        weightPercentage: holding.holdingPercent
                    })
                } else {
                    holdings.push({
                        asset: asset.type,
                        name: asset.name,
                        ticker: holding.symbol,
                        weightPercentage: parseFloat((holding.holdingPercent * 100).toFixed(2)),
                    })
                }
            }
            assetSummary.family = result.fundProfile.family
            assetSummary.holdings = holdings
        }
        return assetSummary

    } catch (e) {
        return handleError(e)
    }
}

const GetHistoricalData = async (ticker: string, date: string): Promise<History> => {
    const queryDate = new Date(date);
    const dayAfter = new Date(date);
    dayAfter.setDate(dayAfter.getDate() + 1)
    try {
        const result = await yahooFinance.historical(ticker.toUpperCase(), {
            period1: queryDate,
            period2: dayAfter,
        });
        return {
            open: parseFloat(result[0].open.toFixed(2)),
            close: parseFloat(result[0].close.toFixed(2)),
            date: DateService.GetDate(new Date(result[0].date)),
        }
    } catch (e) { // weekend
        handleError(e)
    }
}

const GetCompleteHistoricalData = async (ticker: string, from: string): Promise<History[]> => {
    try {
        const result = await yahooFinance.historical(ticker.toUpperCase(), {
            interval: '1d',
            period1: from
        })
        const historyQuotes: History[] = []
        result.map(q => {
            historyQuotes.push({
                date: GetDate(new Date(q.date)),
                open: parseFloat(q.open.toFixed(2)),
                close: parseFloat(q.close.toFixed(2)),
                volume: q.volume
            })
        })
        return historyQuotes
    } catch (e) {
        if (e.name === "FailedYahooValidationError") {
            const historyQuotes: History[] = []
            const historyFields = e.result.indicators.quote[0]
            historyFields.open.map((value: number, index: number) => {
                historyQuotes.push({
                    open: parseFloat(historyFields.open[index].toFixed(2)),
                    close: parseFloat(historyFields.close[index].toFixed(2)),
                    date: GetDate(new Date(historyFields.date[index])), // neexistuje
                    volume: historyFields.volume[index],
                })
            })
            return historyQuotes
        }
        return handleError(e)
    }
}

const Search = async (quote: string) => {
    try {
        return await yahooFinance.search(quote, {
            quotesCount: 5,
        })
    } catch (e) {
        if (e.name === "FailedYahooValidationError") {
            return e.result.quotes
        }
        return handleError(e)
    }
}

const FetchBenchmark = async (ticker: string) => {
    try {
        const result = await yahooFinance.quoteSummary(ticker, {
            modules: [
                'defaultKeyStatistics',
                'fundPerformance',
            ]
        }, {
            validateResult: false
        })
        const benchmark: Benchmark = {
            type: {
                ticker,
            },
            performance: {
                alpha: result.fundPerformance.riskOverviewStatistics.riskStatistics[0].alpha,
                beta: result.fundPerformance.riskOverviewStatistics.riskStatistics[0].beta,
                meanAnnualReturn: result.fundPerformance.riskOverviewStatistics.riskStatistics[0].meanAnnualReturn,
                rSquared: result.fundPerformance.riskOverviewStatistics.riskStatistics[0].rSquared,
                sharpe: result.fundPerformance.riskOverviewStatistics.riskStatistics[0].sharpeRatio,
                stdDev: result.fundPerformance.riskOverviewStatistics.riskStatistics[0].stdDev,
            },
            history: []
        }
        return benchmark
    } catch (e) {
        return handleError(`No data for ${ticker}`)
    }

}

export const YahooFinanceService = {
    GetCompanyByTicker,
    GetCompanyByQuote,
    GetAssetSummary,
    GetHistoricalData,
    Search,
    GetCompleteHistoricalData,
    FetchBenchmark
}
