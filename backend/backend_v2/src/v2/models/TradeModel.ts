import mongoose, {Schema, Types} from "mongoose";
import {IAssetModel} from "./AssetModel";

export interface ITradeModel {
    executionDate: string,
    pricePerAsset: number,
    overallPrice: number,
    asset: IAssetModel,
    amount: number,
    fee?: number,
    _id?: Types.ObjectId,
}

const TradeSchema = new mongoose.Schema<ITradeModel>({
    executionDate: {
        type: Date,
        required: true,
        default: () => Date.now(),
        immutable: true
    },
    fee: {
        type: Number,
        required: false,
    },
    amount: {
        type: Number,
        required: true,
    },
    pricePerAsset: {
        type: Number,
        required: true
    },
    overallPrice: {
        type: Number,
        required: true
    },
    asset: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Asset',
        required: true,
    }
}, {
    collection: 'Trade'
})

export const TradeModel = mongoose.model<ITradeModel>('Trade', TradeSchema);