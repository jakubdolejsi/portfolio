import mongoose  from "mongoose";


const AssetTypeSchema = new mongoose.Schema({
    name: String,
},{
    collection: 'AssetType'
})

export const AssetTypeModel = mongoose.model('AssetType', AssetTypeSchema);