import mongoose from "mongoose";


const StockSchema = new mongoose.Schema({
    name: String,
    ticker: String,
    owner: String,
    pe: Number,
    pb: Number,
    dividend: Number,
    country: String,
    marketCap: String,
    sector: String,
}, {
    collection: 'Stock',
})

export const StockModel = mongoose.model('StockModel', StockSchema);