import mongoose from "mongoose";


const CryptoSchema = new mongoose.Schema({
    name: String,
}, {
    collection: 'Crypto',
})

export const CryptoModel = mongoose.model('CryptoModel', CryptoSchema);