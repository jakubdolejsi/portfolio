import mongoose from "mongoose";


const FundSchema = new mongoose.Schema({
    name: String,
    ticker: String,
    owner: String,
    ter: String,
    sectors: Array,
    countries: Array,
}, {
    collection: 'Fund',
})

export const FundModel = mongoose.model('FundModel', FundSchema);