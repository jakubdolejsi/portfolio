import mongoose, {Types} from "mongoose";
import {IAssetType} from "../services/AssetTypeService";
import {ICategory} from "./CategoryModel";
import {History} from "../services/external/YahooFinanceService";
import Logger from "../logger/logger";
import {DateService} from "../services/DateService";


export interface IAssetModel {
    _id?: Types.ObjectId,
    name: string,
    type: IAssetType,
    category?: ICategory,
    history?: History[],
    updated?: string
}


export interface AssetWithChart {
    _id?: Types.ObjectId,
    name: string,
    type: IAssetType,
    category?: ICategory,
    chart: Chart
}

export type Trending = 'UP' | 'DOWN'

export interface Chart {
    history: History[],
    buySeries: AverageBuySeries[],
    trending: Trending
}

export interface AverageBuySeries {
    date: string, // date of trade
    averageAssetPrice: number, // the price of given asset in that date
    sharesBought: number, // number of shared bought that date
    amountInvested: number // amount money invested in that trade
}


const AssetSchema = new mongoose.Schema<IAssetModel>({
    name: {type: String, required: true},
    type: {type: Object, required: true},
    category: {type: Object, required: false},
    history: {type: [Object], required: false, default: []},
    updated: {type: String, required: false, default: DateService.GetCurrentDateYMDSync},
}, {
    collection: 'Asset'
});

AssetSchema.pre('save', async function (done) {
    const date = await DateService.GetCurrentDateYMD()
    this.set('updated', date)
    Logger.info(`saved: ${date}`)
    done()
})

AssetSchema.pre('updateOne', async function (done) {
    const date = await DateService.GetCurrentDateYMD()
    this.set('updated', date)
    Logger.info(`updated: ${date}`)
    done()
})

export const AssetModel = mongoose.model<IAssetModel>("Asset", AssetSchema)