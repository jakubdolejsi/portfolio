import mongoose, {ObjectId, Types} from "mongoose";
import {ITradeModel} from "./TradeModel";
import {IPortfolioModel} from "./PortfolioModel";

export interface IUser {
    _id?: Types.ObjectId,
    email: string,
    dateRegistered: Date,
    lastLogin: Date,
    trades: ITradeModel[],
    portfolio?: IPortfolioModel[],
    // statistics: Statistics
}



const UserSchema = new mongoose.Schema<IUser>({
    email: {type: String},
    dateRegistered: {type: Date},
    lastLogin: {type: Date},
    trades: [{type: mongoose.Schema.Types.ObjectId, ref: 'Trade',}],
    portfolio: {type: [Object], required: false},
}, {
    collection: 'User'
});

export const UserModel = mongoose.model<IUser>("User", UserSchema)