import {IAssetModel} from "./AssetModel";
import mongoose, {Types} from "mongoose";
import {string} from "joi";
import {ITradeModel} from "./TradeModel";
import {GroupedTrade} from "../services/StatsService";


export type PortfolioCurrency = 'CZK' | 'USD' | 'EUR' // Supported currencies

export interface IPortfolioModel {
    _id?: Types.ObjectId,
    assets: IPortfolioItem[],
    currency?: PortfolioCurrency,
    balance?: number,
    name: string, // the name of a portfolio - user may have multiple portfolios (e.g. crypto, dividend, speculative)
    tradesCount: number,
    statistics?: Statistics
}

export interface IPortfolioItem {
    asset: IAssetModel,     // asset object
    amount: string,         // amount of holding asset,
    totalCashInvested?: number, // total money actually invested in that asset
    averageBuy?: number, // average buy level
    averageBuySeries?: AverageBuySeries[] // list of all buy time series as it were continually updated
    averageCost?: number, // average cost level
    overallCost?: number, // total cost on this assets (poplatky, TER, menny kurs)
    tradesPerAsset?: number // total number of trades of certain asset
}


export interface AverageBuySeries {
    date: string, // date of trade
    averageAssetPrice: number, // the price of given asset in that date
    sharesBought: number, // number of shared bought that date
    amountInvested: number // amount money invested in that trade

}


// export interface Statistics {
//     totalCountOfAssets: number,
//     totalCountOfTrades: number,
//     theMostTradesAsset: string,
//     theMostExpensiveAsset: string,
//     theLeastExpensiveAsset: string,
//     assetWithMostShares: string,
//     totalMoneyTradedInUSD: string,
//     theMostTradedDay: string,
//     theMostTradedMonth: string,
//     theMostTradedSector: string,
//     theMostTradedCountry: string,
// }
// export interface StatisticsPortfolioItem extends Statistics {
//     portfolioName: string
// }
//
// export interface UserStatistics {
//     general: Statistics,
//     portfolios: StatisticsPortfolioItem[]
// }
//
//
export interface StatsItem {
    name: string,
    value: number
}

export interface MultipleStatsItems {
    name: string,
    values: StatsItem[]
}


interface Statistics {
    assetsCount: number,
    tradesCount: number,
    totalBalanceInUSD: number,
    currencies: PortfolioCurrency[],

    theMostExpensiveAsset: StatsItem,
    theLeastExpensiveAsset: StatsItem,

    theMostTradedAsset: StatsItem,

    assetWithMostShares: StatsItem,

    theMostTradedSector: MultipleStatsItems,
    theMostTradedCountry: MultipleStatsItems,

    performance: number
}

export interface PortfolioStatistics extends Statistics {
    name: string
}

export interface ConcreteStat {
    stats: PortfolioStatistics[]
}
export interface GeneralStatistics extends Statistics {
    concreteStats: ConcreteStat,
    trades: GroupedTrade[]
}

const PortfolioSchema = new mongoose.Schema<IPortfolioModel>({
    assets: {type: [Object], required: true},
    balance: {type: Number, required: false, default: 0},
    currency: {type: String, required: false, default: 'EUR'},
    name: {type: String, required: true, default: 'Default'},
    tradesCount: {type: Number, required: true, default: 0},
}, {
    collection: 'Portfolio'
})

export const PortfolioModel = mongoose.model<IPortfolioModel>("Portfolio", PortfolioSchema)
