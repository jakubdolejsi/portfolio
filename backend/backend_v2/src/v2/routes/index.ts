import express from 'express'
import {jwtCheck} from "../middleware/jwtCheck";
import {TradeController} from "../controllers/TradeController";
import {UserController} from "../controllers/UserController";
import {AssetController} from "../controllers/AssetController";
import {CategoryController} from "../controllers/CategoryController";
import {PortfolioController} from "../controllers/PortfolioController";
import {StatsController} from "../controllers/StatsController";

const router = express.Router();

router.use(jwtCheck)

/*
--------------------------------------------------------------------------------
                            Public routes
--------------------------------------------------------------------------------
*/

router.get('/public', (req, res, next) => {
    return res.status(200).send({
        body: "Zravim Vas pekne"
    })
})

router.get('/public/token', (req, res, next) => {
    return res.status(200).send({
        body: "Zravim Vas pekne"
    })
})

/*
--------------------------------------------------------------------------------
                            User routes
--------------------------------------------------------------------------------
*/

// router.get('/user', UserController.GetUserById)
router.post('/user', UserController.GetOrCreate)

router.post('/user/fake', UserController.CreateFakeUser)

/*
--------------------------------------------------------------------------------
                            Trade routes
--------------------------------------------------------------------------------
*/
router.get('/trade', TradeController.GetAllTrades)
router.post('/trade', TradeController.AddNewTrade)
router.get('/trade/:ticker', TradeController.GetTradesByAsset)
router.delete('/trade/:id', TradeController.RemoveTradeById)
router.delete('/trade/asset/:ticker', TradeController.RemoveAllTradesByAsset)

/*
--------------------------------------------------------------------------------
                            Asset routes
--------------------------------------------------------------------------------
*/

router.get('/asset/history/:ticker', AssetController.GetAssetCompleteHistory)
router.get('/asset/history/:ticker/:date', AssetController.GetAssetHistoricalInfo)

router.get('/asset/yahoo', AssetController.RequestAsset)

router.get('/asset', AssetController.GetAllAssets)
router.get('/asset/chart', AssetController.GetAssetWithCompleteDetails)

router.get('/asset/benchmark/:ticker', AssetController.GetBenchmark)

router.get('/asset/name', AssetController.GetAssetByName)
router.get('/asset/ticker/:ticker', AssetController.GetAssetByTicker)
router.get('/asset/quote/:quote', AssetController.GetAssetByQuote)
router.get('/asset/search/:quote', AssetController.SearchAssetLive)
/*
--------------------------------------------------------------------------------
                            Category routes
--------------------------------------------------------------------------------
*/
router.get('/category', CategoryController.GetAllUserCategories)
router.get('/category/name', CategoryController.GetCategoryByName)
router.post('/category', CategoryController.AddCategoryToAsset)
router.put('/category', CategoryController.UpdateAssetCategory)

/*
--------------------------------------------------------------------------------
                            Portfolio routes
--------------------------------------------------------------------------------
*/
router.get('/portfolio', PortfolioController.GetAllPortfolios)
router.get('/portfolio/history', PortfolioController.GetAllPortfoliosWithHistory)
router.get('/portfolio/:name', PortfolioController.GetUserPortfolio)
router.get('/portfolio/:name/:ticker', PortfolioController.GetUserPortfolioAsset)
router.post('/portfolio', PortfolioController.AddNewPortfolio)

router.delete('/portfolio/:oldName/:newName', PortfolioController.DeletePortfolio)
router.delete('/portfolio/:oldName', PortfolioController.DeletePortfolio)
router.put('/portfolio', PortfolioController.SetPortfolioCurrency)


/*
--------------------------------------------------------------------------------
                            Statistics  routes
--------------------------------------------------------------------------------
*/
router.get('/stats', StatsController.GetAlStatsByUser)
router.get('/stats/performance', StatsController.GetPerformance)
router.get('/stats/performance/:portfolioName', StatsController.GetPortfolioPerformance)
router.get('/stats/performance/:portfolioName/asset/:ticker', StatsController.GetAssetPerformance)
router.get('/stats/performance/:portfolioName/asset/relative/:ticker', StatsController.GetAssetPerformanceWithoutCashInflow)

router.post('/stats/performance/:portfolioName/asset', StatsController.GetQuotesPerformance)
router.post('/stats/performance/:portfolioName/nominal/asset', StatsController.GetQuotesPerformanceWithoutCashInflow)
router.post('/stats/performance/:portfolioName/nominal/asset/weighted', StatsController.GetWeightedQuotesPerformanceWithoutCashInflow)

router.get('/stats/allocation/:portfolioName/country', StatsController.GetPortfolioCountryAllocation)
router.get('/stats/allocation/:portfolioName/sector', StatsController.GetPortfolioSectorAllocation)
router.get('/stats/allocation/:portfolioName/industry', StatsController.GetPortfolioIndustryAllocation)

router.get('/stats/allocation/:portfolioName/asset', StatsController.GetPortfolioAssetAllocation)
router.get('/stats/allocation/:portfolioName/type', StatsController.GetPortfolioAssetTypeAllocation)

router.post('/stats/performance/:portfolioName/asset/compare', StatsController.CompareQuotes)
router.post('/stats/performance/:portfolioName/asset/nominal/compare', StatsController.CompareQuotesWithoutCashInflow)


router.post('/stats/benchmark/:portfolioName', StatsController.CompareQuotesWithBenchmark)
router.post('/stats/benchmark/:portfolioName/price', StatsController.CompareQuotesPriceWithBenchmark)

router.post('/stats/correlation/:portfolioName/', StatsController.GetQuotesCorrelation)
router.post('/stats/volatility/:portfolioName/', StatsController.GetQuoteVolatility)

router.post('/stats/pl/:portfolioName/', StatsController.GetQuotePL)



export default router
