import jwt from "express-jwt";
import jwksRsa from "jwks-rsa";
import jwtAuthz from "express-jwt-authz";
import * as dotenv from "dotenv";

dotenv.config();

export const permission = (permisions: string[]) => {
    return jwtAuthz(permisions, {
        customScopeKey: 'permissions',
        checkAllScopes: true,
    });
};

const routesWithoutAuth = [
    '/',
    '/api/v2/public',
    '/api/v2/public/token',
]

export const jwtCheck = jwt({
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: process.env.AUTH0_URI
    }),
    // audience: process.env.AUTH0_AUDIENCE,
    issuer: process.env.AUTH0_ISSUER,
    algorithms: ['RS256']
})
    .unless({
        path: routesWithoutAuth
    });
