export interface RegistrationFrom {
    email?: string
    password?: string
    password2?: string
}

export interface LoginForm {
    email?: string
    password?: string
}
