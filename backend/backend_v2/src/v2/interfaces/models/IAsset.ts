
import { Document, Model } from "mongoose";

type AssetType = 'Stock' | 'ETF' | 'Comodit' | 'Crypto'
type AssetProperty = 'Underlying asset' | 'CFD' | 'Futures'

export interface IAsset {
    type: AssetType,
    name: string,
    property: AssetProperty,
    amount: number,
    ticker?: string
}

export interface IAssetDocument extends IAsset, Document { }

export interface IAssetModel extends Model<IAssetDocument> { }