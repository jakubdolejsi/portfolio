
import { Document, Model } from "mongoose";
import {IAsset} from "./IAsset";

type TradeType = 'Buy' | 'Sell' | 'Short' | 'Long'

type StockExchanges = 'Binance' | 'ChangeInvest' | 'Etoro' | 'Xtb' | 'Lynx' | 'Revolut' | 'Patria' | 'Portu' | 'Interactive Brokers' | 'CoinBase'

type CurrencyType= 'USD' | 'EUR' | 'CZK'

export interface ITrade {
    price: number,
    currency: CurrencyType,
    exchangeRate: number,
    executionDate: Date,
    stockExchange: StockExchanges
    type: TradeType,
    asset: string,
    leverage?: number
    fee?: number | string,
    assetObject?: IAsset
}

export interface ITradeDocument extends ITrade, Document { }

export interface ITradeModel extends Model<ITradeDocument> { }