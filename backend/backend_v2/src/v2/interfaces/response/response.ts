import {NextFunction, Request, Response} from "express";
import Logger from "../../logger/logger";


export const Success = <T>(res: Response, data: T, code: number = 200): Response => {
    const dataToSend = {
        body: data,
        timeStamp: new Date(),
    };
    return res.status(code).send(dataToSend);
}

export const Error = <T>(res: Response, errorMessage: T, code: number): Response => {
    const dataToSend = {
        error: {
            message: errorMessage
        },
        timeStamp: new Date(),
    };
    Logger.error(errorMessage);
    return res.status(code).send(dataToSend);
}

export const InvalidUrl = (req: Request, res: Response, next: NextFunction): Response => {
    return res.status(404).send(
        {
            error: 'Invalid Url',
            timeStamp: new Date(),
        });
}