import {ComputeChange, ComputationService} from "../services/ComputationService";


describe('Computation service test', () => {

    it('compute change should not be null', () => {
        const change = ComputeChange(20, 30)
        expect(change).not.toBeNull()
    })

    it('should compute change correctly', () => {
        const change = ComputeChange(20, 30)
        expect(change).toEqual(10)
    })

    it('round should not be null', () => {
        const num = 10.5
        const rounded = ComputationService.round(num, 0)
        expect(rounded).not.toBeNull()
    })

    it('should round correctly', () => {
        const num = 10.5
        const rounded = ComputationService.round(num, 0)
        expect(rounded).toEqual(11)
    })

    it('should round correctly', () => {
        const num = 10.5
        const rounded = ComputationService.round(num, 1)
        expect(rounded).toEqual(10.5)
    })

    it('should round correctly', () => {
        const num = 10.4
        const rounded = ComputationService.round(num, 0)
        expect(rounded).toEqual(10)
    })

    it('should round correctly', () => {
        const num = 10.6
        const rounded = ComputationService.round(num, 0)
        expect(rounded).toEqual(11)
    })

    it('should round correctly', () => {
        const num = 10.6
        const rounded = ComputationService.round(num, 2)
        expect(rounded).toEqual(10.6)
    })

})
