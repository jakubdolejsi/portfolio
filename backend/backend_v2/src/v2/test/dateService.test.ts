import {DateService} from "../services/DateService";


describe('test date service ', () => {

    it('should load Y-m-d date async', async () => {
        const today = await DateService.GetCurrentDateYMD
        expect(today).not.toBeNull()
    })

    it('should load Y-m-d date sync', () => {
        const today = DateService.GetCurrentDateYMDSync()
        expect(today).not.toBeNull()
    })

    it('should generate date', () => {
        const date = DateService.GetDate(new Date('01-01-202'))
        expect(date).not.toBeNull()
    })

    it('should generate date between async', async () => {
        const from = '01-01-2020'
        const to = '01-03-2020'
        const between = await DateService.GenerateDateBetween(from, to)
        expect(between).not.toBeNull()
    })
    it('should get current date async', async () => {
        const date = await DateService.GetCurrentDate()
        expect(date).not.toBeNull()
    })

    it('should get current date sync', () => {
        const date = DateService.GetCurrentDateSync()
        expect(date).not.toBeNull()
    })

    it('should get reversed date sync', () => {
        const date = DateService.GetReversedDate(new Date('01-01-202'))
        expect(date).not.toBeNull()
    })

})
