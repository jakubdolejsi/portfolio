const fs = require('fs');

const success = () => {
    console.log('Node modules fixed')
}

const prestart = () => {
    const path = 'node_modules/yahoo-finance2/dist/esm/src/lib/validateAndCoerceTypes.d.ts'
    const data = fs
        .readFileSync(path,'utf8')
        .toString()
        .split("\n")

    const ignoreAlready = data[1]
    if(ignoreAlready.includes('@ts-ignore')) return success()

    data.splice(1, 0, "// @ts-ignore")
    const text = data.join("\n")

    fs.writeFileSync(path, text)
    success()
}

prestart()