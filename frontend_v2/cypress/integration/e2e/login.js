require("cypress-xpath");

describe('Login ', () => {
    it('should click on login button', () => {
        cy.login()
    })
    it('Logged in successfully', () => {
        cy.get('#overview').should('exist')

    })
    it('Should contain summary info', () => {
        cy.xpath('//*[@id="root"]/div/div/div/div/div/div[2]/div[1]/div/h5').contains('Summary info')
    })

    it('Should contain activity profile', () => {
        cy.xpath('//*[@id="root"]/div/div/div/div/div/div[2]/div[1]/div/h5').should('exist')
    })

    it('Should contain activity profile', () => {
        cy.xpath('//*[@id="root"]/div/div/div/div/div/div[2]/div[1]/div/h5').should('exist')
    })

    it('should render home page navbar', () => {
        cy.get('#home-navbar').should('exist')
    });

    it('should render trade navbar', () => {
        cy.get('#trade-navbar').should('exist')
    });

    it('should render asset navbar', () => {
        cy.get('#asset-navbar').should('exist')
    });

    it('should render portfolio navbar', () => {
        cy.get('#portfolio-navbar').should('exist')
    });

    it('should render tools navbar', () => {
        cy.get('#tools-navbar').should('exist')
    });

})
