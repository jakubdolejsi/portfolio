describe('render home page ', () => {
    it('should render welcome page', () => {
        cy.visit('/')
        cy.get('.App-header').should('exist')
    })

    it('should render home page navbar', () => {
        cy.visit('/')
        cy.get('#home-navbar').should('exist')
    });

    it('should render news navbar', () => {
        cy.visit('/')
        cy.get('#news-navbar').should('exist')
    });

    it('should render tutorial navbar', () => {
        cy.visit('/')
        cy.get('#tutorial-navbar').should('exist')
    });

    it('should render about navbar', () => {
        cy.visit('/')
        cy.get('#about-navbar').should('exist')
    });

    it('login button should exists', () => {
        cy.visit('/')
        cy.get('#login-button').should('exist')
    })

})
