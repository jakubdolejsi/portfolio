require("cypress-xpath");

describe('Assst page tests', () => {
    it('should login first', () => {
        cy.login()
    })

    it('Should render asset page', () => {
        cy.visit('/auth/asset')
    })

    it('should render title', () => {
        cy.xpath('//*[@id="root"]/div/div/div/div/div/div/div[2]/div[1]/h4').should('exist')
    })

    it('should render asset detail Card', () => {
        cy.xpath('//*[@id="root"]/div/div/div/div/div/div/div[2]/div[2]/div[2]/div[1]').should('exist')
    })

    it('should render asset list Card', () => {
        cy.xpath('//*[@id="root"]/div/div/div/div/div/div/div[2]/div[2]/div[1]').should('exist')
    })

    it('should render portfolio tab bar', () => {
        cy.xpath('//*[@id="root"]/div/div/div/div/div/div/div[1]').should('exist')
    })


})
