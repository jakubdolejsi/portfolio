require("cypress-xpath");

describe('trade page tests', () => {

    it('should login first', () => {
        cy.login()
    })

    it('should visit trade page', () => {
        cy.visit('/auth/trade')
    })

    it('should render trade page', () => {
        cy.xpath('//*[@id="root"]/div/div/div/div/div/div[1]/h4').contains('All trades')
    })

    it('should render add new trade button', () => {
        cy.xpath('//*[@id="root"]/div/div/div/div/div/div[2]/div[1]/button').should('exist')
    })

    it('should click on add new trade button', () => {
        cy.xpath('//*[@id="root"]/div/div/div/div/div/div[2]/div[1]/button').click()
    })

    it('should render modal', () => {
        cy.xpath('/html/body/div[2]/div[3]').should('exist')
    })

    it('Modal should have submit button', () => {
        cy.xpath('/html/body/div[2]/div[3]/div[3]/div[3]/div/button').should('exist')
    })

    it('Modal should have cancel button', () => {
        cy.xpath('/html/body/div[2]/div[3]/div[3]/div[2]/div/button').should('exist')
    })

    it('Should close modal', () => {
        cy.xpath('/html/body/div[2]/div[3]/div[3]/div[2]/div/button').click()
    })

})
