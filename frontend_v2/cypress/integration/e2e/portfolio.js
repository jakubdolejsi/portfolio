require("cypress-xpath");


describe('Portfolio page tests', () => {

    it('Should login first', () => {
        cy.login()
    })

    it('Should render portfolio page', () => {
        cy.visit('/auth/portfolio')
    })

    it('should render title', () => {
        cy.xpath('//*[@id="simple-tabpanel-0"]/div/div/div[1]/h4').should('exist')
    })

    it('should render title', () => {
        cy.xpath('//*[@id="simple-tabpanel-0"]/div/div/div[1]/h4').should('exist')
    })

    it('should render tab bar', () => {
        cy.xpath('//*[@id="root"]/div/div/div/div/div/div/div[2]/div[1]').should('exist')
    })

    it('should render title', () => {
        cy.xpath('//*[@id="simple-tabpanel-0"]/div/div/div[1]/h4').should('exist')
    })

    it('should portfolio value', () => {
        cy.xpath('//*[@id="simple-tabpanel-0"]/div/div/div[1]/div[2]/div[1]/div').should('exist')
    })

    it('should portfolio details', () => {
        cy.xpath('//*[@id="simple-tabpanel-0"]/div/div/div[1]/div[2]/div[2]/div/div/div[1]/h5').should('exist')
    })

    it('should render action buttons', () => {
        cy.xpath('//*[@id="simple-tabpanel-0"]/div/div/div[1]/div[3]').should('exist')
    })

})


describe('Validate tabs', () => {

    it('Should render Overall tab', () => {
        cy.xpath('//*[@id="simple-tab-0"]').should('exist')
    })

    it('Should render Asset allocation tab', () => {
        cy.xpath('//*[@id="simple-tab-1"]').should('exist')
    })

    it('Should render Asset type tab', () => {
        cy.xpath('//*[@id="simple-tab-2"]').should('exist')
    })

    it('Should render country allocation tab', () => {
        cy.xpath('//*[@id="simple-tab-3"]').should('exist')
    })

    it('Should render sector allocation tab', () => {
        cy.xpath('//*[@id="simple-tab-4"]').should('exist')
    })

    it('Should render industry allocation tab', () => {
        cy.xpath('//*[@id="simple-tab-5"]').should('exist')
    })

})


describe('Validate asset allocation tab', () => {
    it('should navigate tot he asset allocation tab', () => {
        cy.xpath('//*[@id="simple-tab-1"]').click()
    })

    it('should render asset allocation heading', function () {
        cy.xpath('//*[@id="left"]/div[1]/div/h5').contains('Asset allocation')
    });

    it('should render asset value allocation chart', function () {
        cy.xpath('//*[@id="left"]/div[2]/div/h6').should('exist')
    });

    it('should render assets pl chart', function () {
        cy.xpath('//*[@id="left"]/div[1]/div/h5').should('exist')
    });

    it('should render asset list', function () {
        cy.xpath('//*[@id="left"]/div[1]/div/h5').should('exist')
    });

    it('should render allocation details card', function () {
        cy.xpath('//*[@id="left"]/div[1]/div/h5').should('exist')
    });
})
