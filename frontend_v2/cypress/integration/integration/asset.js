import {fetchGET, setJWT} from "../../support";



describe('Asset integration API tests', () => {
    it('should login in headless mode and setup access token', () => {
        cy.loginHeadless()
            .then(token => {
                setJWT(token)
            })
    })

    it('should fetch all assets', () => {
        fetchGET('asset')
            .then(response => {
                // expect(response.body).not.toBeNull()
                assert.isArray(response.body, 'asset are array')
            })
    });

    it('should fetch all request with chart', () => {
        fetchGET('asset/chart')
            .then(response => {
                expect(response.body).not.toBeNull()
            })
    });

    it('should fetch asset by name', () => {
        fetchGET('asset/apple')
            .then(response => {
                expect(response.body).not.toBeNull()
            })
    });

    it('should fetch asset by ticker', () => {
        fetchGET('asset/ticker/aapl')
            .then(response => {
                expect(response.body).not.toBeNull()
            })
    });

    it('should fetch asset by quote', () => {
        fetchGET('asset/quote/aapl')
            .then(response => {
                expect(response.body).not.toBeNull()
            })
    });

    it('should search asset', () => {
        fetchGET('asset/search/aapl')
            .then(response => {
                cy.log(response.body)
                expect(response.body).not.toBeNull()
            })
    });
})
