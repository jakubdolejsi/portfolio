// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

// Alternatively you can use CommonJS syntax:
// require('./commands')


export const baseUrl = 'http://localhost:3001/api/v2'
let jwtToken = ''


// export const fetchGET = async (endpoint) => {
//     const options = {
//         method: 'GET',
//         url: `${baseUrl}/${endpoint}`,
//     }
//     // return fetch(options);
//
// }

export const fetchGET = async (endpoint, data) => {
    const url = `${baseUrl}/${endpoint}`
    const options = {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${jwtToken}`
        },
    }
    const response = await fetch(url, options)
    return response.json()
}

export const setJWT = (jwt) => {
    jwtToken = jwt
}
