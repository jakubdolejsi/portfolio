// const port = process.env.REACT_APP_BACKEND_PORT || 3001
const url = process.env.REACT_APP_BACKEND_ADDRESS || 'http://localhost:3001'
// const url_prod = process.env.REACT_APP_BACKEND_ADDRESS_PROD
// const url_prod = process.env.REACT_APP_FRONTEND_ADDRESS

let address
if(process.env.NODE_ENV === 'development') {
    address = `${url}`
} else {
    address = process.env.REACT_APP_FRONTEND_ADDRESS_PROD
}

const config = {
    // port,
    url,
    address,
}

export default config
