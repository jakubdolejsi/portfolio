import {Trending} from "../pages/auth/AssetPage";

export interface ComputeResult {
    relativeChange: number,
    relativeChangePercent: number,
    absoluteChange: number,
    trend: Trending,
    start: number,
    end: number
}

type ICompute = (y1: number, y2: number) => ComputeResult


export const round = (n: number, d: number = 2) => {
    return parseFloat(n.toFixed(d))
}

export const computeChange: ICompute = (y1, y2) => {
    const relative = round((y2 - y1) / y1)
    const relativePercent = round(relative * 100)
    let absolute = round(y1 * relative, 0)
    const sign = Math.sign(relative)
    let trend: Trending = sign > 0 ? 'UP' : 'DOWN'
    return {
        relativeChange: relative,
        relativeChangePercent: relativePercent,
        absoluteChange: absolute,
        trend,
        start: y1,
        end: y2
    }
}


