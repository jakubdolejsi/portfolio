import React from 'react';
import './App.css';
import {BrowserRouter as Router, Switch,} from "react-router-dom";
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import {LocalizationProvider} from "@mui/lab";
import Loading from './components/loading/Loading'
import {useAuth0} from '@auth0/auth0-react';

import {routes, SubRoutes} from "./router/routes";


function App() {
    const {isAuthenticated, isLoading} = useAuth0();


    if (isLoading) {
        return (
            <Loading size={40}/>
        )
    }

    console.log(isAuthenticated)
    return (
        <div className="App-header">
            <Router>
                <div>
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <Switch>
                            {routes.map((route, i) => (
                                <SubRoutes key={i} {...route} />
                            ))}
                        </Switch>
                    </LocalizationProvider>
                </div>
            </Router>
        </div>
    )
}

export default App;
