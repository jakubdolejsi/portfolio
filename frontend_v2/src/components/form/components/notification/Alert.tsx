import MuiAlert, {AlertColor, AlertProps} from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';
import {Box} from "@mui/material";
import React from "react";

const CustomAlert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
    props,
    ref,
) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

interface IAlertProps {
    severity: AlertColor,
    open: boolean,
    onClose: (event?: React.SyntheticEvent | Event, reason?: string) => void,
    message: string
}

const Alert: React.FC<IAlertProps> = ({message, severity, open, onClose}) => {

    return (
        <Box>
            <Snackbar
                open={open}
                autoHideDuration={5000}
                onClose={onClose}
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center"
                }}
            >
                <CustomAlert onClose={onClose} severity={severity} sx={{width: '100%'}}>
                    {message}
                </CustomAlert>
            </Snackbar>
        </Box>
    )
}

export default Alert