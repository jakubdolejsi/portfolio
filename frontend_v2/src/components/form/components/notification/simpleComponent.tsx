import React from 'react';


interface SimpleProps {
    count: number
}

const SimpleComponent: React.FC<SimpleProps> = (props) => {
    // const [count, setCount] = useState(props.count)

    // useEffect(() => {
    //     setCount(count);
    //     console.log('change in v-DOM');
    // }, [props.count])

    return (
        <div>
            {props.count}
        </div>
    )
}


export default SimpleComponent