import React, {useEffect} from 'react'
import MuiAlert, {AlertProps, Color} from '@material-ui/lab/Alert';
import {makeStyles} from '@mui/styles';
import {PropTypes, SnackbarProps, StandardProps, Theme} from "@mui/material";


const useStyles = makeStyles((theme: Theme) => ({
    root: {
        margin: theme.spacing(1),
        width: '300px',
    }
}))


const Alert = (props: AlertProps) => {
    return <MuiAlert variant="filled" {...props} />;
}


interface NotificationProps extends StandardProps<SnackbarProps, AlertProps & string> {
    backgroundColor: PropTypes.Color;
    severity: Color
}


const Notification: React.FC<NotificationProps> = (props) => {
    // @ts-ignore
    const {message, severity, backgroundColor, ...other} = props
    const styles = useStyles()


    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
        setOpen(true);
    }

    const handleClose = (event: any, reason?: any) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    // @ts-ignore
    useEffect(() => {
        // @ts-ignore
        if (props.message !== "") {
            handleClick();
        }
    }, [props.message])


    return (
        <div>
            {/*<Snackbar*/}
            {/*    className={styles.root}*/}
            {/*    anchorOrigin={{*/}
            {/*        vertical: 'bottom',*/}
            {/*        horizontal: 'center',*/}
            {/*    }}*/}
            {/*    open={open}*/}
            {/*    autoHideDuration={5000}*/}
            {/*    onClose={handleClose}*/}
            {/*    message={message}*/}
            {/*    action={*/}
            {/*        <React.Fragment>*/}
            {/*            <IconButton size="small" aria-label="close" color={backgroundColor} onClick={handleClose}>*/}
            {/*                <CloseIcon fontSize="small"/>*/}
            {/*            </IconButton>*/}
            {/*        </React.Fragment>*/}
            {/*    }*/}
            {/*>*/}
            {/*    <Alert onClose={handleClose} severity={"info"} className={styles.root}>*/}
            {/*        <AlertTitle>{message}</AlertTitle>*/}
            {/*    </Alert>*/}
            {/*</Snackbar>*/}
        </div>
    )
}
export default Notification