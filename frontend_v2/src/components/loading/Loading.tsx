import {CircularProgress} from "@mui/material";
import {makeStyles} from "@mui/styles";
import React from "react";


const useStyles = makeStyles((theme) => ({
    loadingProgress: {
        display: 'flex',
        marginTop: '10px'

    },
    loadingText: {
        color: '#d7effc',
        textAlign: 'center',
    },
    container: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    row: {
        width: 'auto'
    }
}));

interface LoadingProps {
    size: number
}

const Loading: React.FC<LoadingProps> = ({size}) => {
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <div className={classes.row}>
                <div className={classes.loadingProgress}>
                    <CircularProgress size={size}/>
                </div>
            </div>
        </div>
    )
}

export default Loading