import {Box} from "@mui/material";
import React, {useEffect} from "react";
import {
    Allocations,
    GetPortfolioAssetAllocation,
    GetQuoteVolatility,
    ICorrelation,
    Volatility
} from "../../../api/Stats";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";
import CustomToolsTabComponent from "./CustomToolsTabComponent";
import CorrelationHeathMap from "../../chart/toolsPage/CorrelationHeathMap";
import HistoryCorrelationChart from "../../chart/toolsPage/HistoryCorrelationChart";
import AllocationList from "../../portfolioPage/AllocationList";
import AllocationRadioList from "./AllocationRadioList";
import VolatilityChart from "../../chart/toolsPage/VolatilityChart";

const VolatilityState = () => {
    const [quote, setQuote] = React.useState<string>('')
    const [assets, setAssets] = React.useState<Allocations[]>([])
    const [range, setRange] = React.useState<number>(10)
    const [period, setPeriod] = React.useState<number>(280)
    const [method, setMethod] = React.useState<string>('Stdev')
    const [volatility, setVolatility] = React.useState<Volatility>({
        quote: '',
        volatility: [],
        history: []
    })

    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);

    useEffect(() => {
        const load = async () => {
            const fetchedAssets = await GetPortfolioAssetAllocation(currentPortfolioStore.name)
            setAssets(fetchedAssets)
        }
        load()
    }, [currentPortfolioStore])

    useEffect(() => {
        const load = async () => {
            const fetchedVolatility = await GetQuoteVolatility(currentPortfolioStore.name, quote, range, period, method)
            setVolatility(fetchedVolatility)
        }
        load()
    }, [quote, range, period, method])

    return (
        <Box>
            <CustomToolsTabComponent
                leftUpperComponent={<VolatilityChart
                    title={"Assets price value"}
                    quote={quote}
                    data={volatility.history}
                    color={'#00ebff'}
                    money={true}
                />}
                leftBottomComponent={<VolatilityChart
                    title={"Assets volatility"}
                    quote={quote}
                    data={volatility.volatility}
                    color={'#8300ff'}
                    money={false}
                />}
                rightUpperComponent={<AllocationRadioList
                    sectors={assets}
                    setQuote={setQuote}
                    quote={quote}
                    range={range}
                    setRange={setRange}
                    title={"Asset list"}
                    period={period}
                    setPeriod={setPeriod}
                    method={method}
                    setMethod={setMethod}
                />}
                rightBottomComponent={<Box/>}
            />
        </Box>
    )
}


export default VolatilityState
