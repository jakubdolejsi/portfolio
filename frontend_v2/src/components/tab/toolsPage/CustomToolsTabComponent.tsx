import {Box, Paper} from "@mui/material";
import React from "react";


interface CustomToolsTabComponentProps {
    leftUpperComponent: any,
    leftBottomComponent: any,
    rightUpperComponent: any,
    rightBottomComponent: any,
}

const CustomToolsTabComponent: React.FC<CustomToolsTabComponentProps> = ({leftBottomComponent, leftUpperComponent, rightBottomComponent, rightUpperComponent}) => {

    return(
        <Box sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            height: '85vh',
            flexDirection: 'row',
            gap: '30px'
        }}>
            <Box id={"left"} sx={{
                display: 'flex',
                flexDirection: 'column',
                gap: '13px'
            }}>
                <Paper elevation={10} sx={{
                    width: '40vw',
                    height: '40vh',
                    background: 'rgb(18,29,34)',
                    display: 'flex',
                    justifyContent: 'center',
                    borderRadius: '10px',
                }}>
                    {leftUpperComponent}
                </Paper>

                <Paper elevation={10} sx={{
                    width: '40vw',
                    height: '40vh',
                    background: 'rgb(18,29,34)',
                    display: 'flex',
                    justifyContent: 'center',
                    borderRadius: '10px',
                }}>
                    {leftBottomComponent}
                </Paper>
            </Box>

            <Box id={"right"} sx={{
                display: 'flex',
                flexDirection: 'column',
                gap: '10px',
            }}>
                <Paper elevation={10} sx={{
                    width: '25vw',
                    height: '80vh',
                    background: 'rgb(18,29,34)',
                    display: 'flex',
                    justifyContent: 'center',
                    borderRadius: '10px',
                }}>
                    {rightUpperComponent}
                </Paper>

                {/*<Paper elevation={10} sx={{*/}
                {/*    height: '10vh',*/}
                {/*    width: '25vw',*/}
                {/*    background: 'rgb(18,29,34)',*/}
                {/*    display: 'flex',*/}
                {/*    justifyContent: 'center',*/}
                {/*    borderRadius: '10px',*/}
                {/*}}>*/}
                {/*    {rightBottomComponent}*/}
                {/*</Paper>*/}
            </Box>

        </Box>
    )
}


export default CustomToolsTabComponent
