import React, {useEffect} from "react";
import {Box, Typography} from "@mui/material";
import CustomToolsTabComponent from "./CustomToolsTabComponent";
import {Allocations, GetPortfolioAssetAllocation, GetQuotesCorrelation, ICorrelation} from "../../../api/Stats";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";
import AllocationList from "../../portfolioPage/AllocationList";
import CorrelationHeathMap from "../../chart/toolsPage/CorrelationHeathMap";
import HistoryCorrelationChart from "../../chart/toolsPage/HistoryCorrelationChart";


const MockComponent = () => {

    return (
        <Typography variant={"h6"} color={"#ffff"}/>
    )
}

const CorrelationState = () => {
    const [quotes, setQuotes] = React.useState<string[]>([])
    const [assets, setAssets] = React.useState<Allocations[]>([])
    const [correlation, setCorrelation] = React.useState<ICorrelation>({
        data: [],
        history: [],
        labels: []
    })

    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);

    useEffect(() => {
        const load = async () => {
            const fetchedSectors = await GetPortfolioAssetAllocation(currentPortfolioStore.name)
            setAssets(fetchedSectors)
        }
        load()
    }, [currentPortfolioStore])


    useEffect(() => {
        const load = async () => {
            const fetchedCorrelation = await GetQuotesCorrelation(currentPortfolioStore.name, quotes)
            setCorrelation(fetchedCorrelation)
        }
        load()
    }, [quotes])

    return (
        <Box>
            <CustomToolsTabComponent
                leftUpperComponent={<CorrelationHeathMap
                    quotes={quotes}
                    correlation={correlation}
                />}
                leftBottomComponent={<HistoryCorrelationChart
                    quotes={quotes}
                    correlation={correlation}
                    title={"Asset's 1y comparison"}
                />}
                rightUpperComponent={<AllocationList
                    sectors={assets}
                    title={"Asset list"}
                    setQuotes={setQuotes}
                />}
                rightBottomComponent={<MockComponent/>}
            />
        </Box>
    )
}


export default CorrelationState
