import {Box} from "@mui/material";
import AllocationPieChart from "../../chart/portfolioPage/AllocationPieChart";
import AllocationList from "../../portfolioPage/AllocationList";
import React, {useEffect} from "react";
import {Allocations, GetPortfolioAssetAllocation,} from "../../../api/Stats";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";
import AllocationDetails from "../../portfolioPage/AllocationDetails";
import BaseTabComponent from "../portfolioPage/BaseTabComponent";
import ComparatorChart from "../../chart/toolsPage/ComparatorChart";


const ComparatorState = () => {
    const [assets, setAssets] = React.useState<Allocations[]>([])
    const [quotes, setQuotes] = React.useState<string[]>([])
    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);
    const [totalPL, setPL] = React.useState(0)

    useEffect(() => {
        const load = async () => {
            const fetchedSectors = await GetPortfolioAssetAllocation(currentPortfolioStore.name)
            setAssets(fetchedSectors)
        }
        load()
    }, [currentPortfolioStore])

    return (
        <Box>
            <BaseTabComponent
                type={"sector"}
                leftUpperComponent={<AllocationPieChart
                    sectors={assets}
                    title={"Asset allocation"}
                />}
                leftMiddleComponent={<ComparatorChart
                    quotes={quotes}
                    cashInflow={false}
                    title={"Assets value evaluation comparison"}
                    benchmark={false}
                />}
                leftBottomComponent={<ComparatorChart
                    quotes={quotes}
                    cashInflow={true}
                    title={"Asset's P&L comparison"}
                    benchmark={false}
                />}
                rightUpperComponent={<AllocationList
                    sectors={assets}
                    title={"Asset list"}
                    setQuotes={setQuotes}
                />}
                rightBottomComponent={<AllocationDetails
                    title={"Allocation details"}
                    quotes={quotes}
                    type={"asset"}
                    sectors={assets}
                    pl={totalPL}
                />}
            />
        </Box>
    )
}


export default ComparatorState
