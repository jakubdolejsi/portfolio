import {
    Box,
    Checkbox,
    Divider,
    List,
    ListItem,
    ListItemIcon,
    ListSubheader, MenuItem,
    Radio,
    Select, SelectChangeEvent,
    Typography
} from "@mui/material";
import React, {useEffect} from "react";
import {Allocations} from "../../../api/Stats";

export interface AllocationRadioListProps {
    quote: string,
    setQuote: React.Dispatch<React.SetStateAction<string>>,
    title: string,
    range: number,
    setRange: React.Dispatch<React.SetStateAction<number>>,
    sectors: Allocations[],
    method: string,
    setMethod: React.Dispatch<React.SetStateAction<string>>,
    period: number,
    setPeriod: React.Dispatch<React.SetStateAction<number>>

}

const AllocationRadioList: React.FC<AllocationRadioListProps> = ({
                                                                     quote,
                                                                     setQuote,
                                                                     range,
                                                                     setRange,
                                                                     title,
                                                                     sectors,
                                                                     period,
                                                                     setPeriod,
                                                                     method,
                                                                     setMethod
                                                                 }) => {

    const isChecked = (ticker: string) => {
        return ticker === quote
    }
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setQuote(event.target.value)
    }

    const handleRangeChange = (event: SelectChangeEvent) => {
        // @ts-ignore
        setRange(event.target.value as number)
    }

    const handleMethodChange = (event: SelectChangeEvent) => {
        // @ts-ignore
        setMethod(event.target.value as string)
    }
    const handlePeriodChange = (event: SelectChangeEvent) => {
        // @ts-ignore
        setPeriod(event.target.value as number)
    }

    return (
        <Box sx={{}}>
            <List dense sx={{
                maxWidth: '500px',
                height: '60',
                display: 'flex',
                alignItems: 'center',
                flexDirection: 'column',
            }} subheader={
                <ListSubheader>
                    <Typography color={"primary"} variant={"h5"} sx={{
                        marginBottom: '20px'
                    }}>{title}</Typography>
                </ListSubheader>
            }>
                <Box sx={{
                    background: 'rgb(24,38,44)',
                    width: '300px',
                    height: '53vh',
                    overflowY: 'scroll',
                    overflowX: 'hidden',
                    // overflow: 'auto',
                    '&::-webkit-scrollbar': {
                        width: '0.4em'
                    },
                    '&::-webkit-scrollbar-track': {
                        boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
                        webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
                    },
                    '&::-webkit-scrollbar-thumb': {
                        backgroundColor: 'rgba(128,124,124,0.1)',
                        // outline: '1px solid #F75C03',
                        borderRadius: '2px',
                        // color: '#3f352f'
                    }
                }}>
                    {sectors.map(s => {
                        return (
                            <ListItem>
                                <List sx={{
                                    padding: 0
                                }} dense subheader={
                                    <ListSubheader disableGutters disableSticky sx={{
                                        display: 'flex',
                                        justifyContent: 'start',
                                        padding: 0
                                    }}>
                                        <Radio
                                            checked={isChecked(s.name)}
                                            onChange={handleChange}
                                            value={s.name}
                                            color={"primary"}
                                        />
                                        <Typography
                                            color={isChecked(s.name) ? '#29b6f6' : "primary"}
                                            sx={{
                                                fontSize: '17px',
                                                paddingTop: '8px',
                                            }}>
                                            {s.name}: {s.sum.toFixed(0)}
                                        </Typography>
                                    </ListSubheader>
                                }>
                                    <Divider light/>

                                </List>
                            </ListItem>
                        )
                    })}
                </Box>
                <Box sx={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'center'
                }}>
                    <Box sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        paddingX: '10px',
                    }}>
                        <Typography
                            color={"primary"}
                            fontSize={20}
                            sx={{
                                padding: '10px',
                            }}>
                            Range
                        </Typography>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={range.toString()}
                            label="Select volatility range"
                            onChange={handleRangeChange}
                            sx={{
                                width: '100px'
                            }}
                        >
                            <MenuItem value={10}>10d</MenuItem>
                            <MenuItem value={20}>21d</MenuItem>
                            <MenuItem value={30}>30d</MenuItem>
                        </Select>
                    </Box>

                    <Box sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        paddingX: '10px',

                    }}>
                        <Typography
                            color={"primary"}
                            fontSize={20}
                            sx={{
                                padding: '10px',
                            }}>
                            Period
                        </Typography>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={period.toString()}
                            label="Select volatility range"
                            onChange={handlePeriodChange}
                            sx={{
                                width: '100px'
                            }}
                        >
                            <MenuItem value={30}>3m</MenuItem>
                            <MenuItem value={90}>6m</MenuItem>
                            <MenuItem value={280}>1y</MenuItem>
                            <MenuItem value={600}>2y</MenuItem>
                        </Select>
                    </Box>

                    <Box sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        paddingX: '10px',
                    }}>
                        <Typography
                            color={"primary"}
                            fontSize={20}
                            sx={{
                                padding: '10px',
                            }}>
                            Method
                        </Typography>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={method}
                            label="Select volatility range"
                            onChange={handleMethodChange}
                            sx={{
                                width: '100px'
                            }}
                        >
                            <MenuItem value={"Stdev"}>Stdev</MenuItem>
                            <MenuItem disabled={true} value={"Beta"}>Beta</MenuItem>
                        </Select>
                    </Box>
                </Box>
            </List>

        </Box>
    )
}

export default AllocationRadioList
