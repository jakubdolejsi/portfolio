import {Box} from "@mui/material";
import AllocationPieChart from "../../chart/portfolioPage/AllocationPieChart";
import React, {useEffect} from "react";
import {Allocations, GetPortfolioAssetAllocation,} from "../../../api/Stats";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";
import AllocationDetails from "../../portfolioPage/AllocationDetails";
import BaseTabComponent from "../portfolioPage/BaseTabComponent";
import ComparatorChart from "../../chart/toolsPage/ComparatorChart";
import AllocationListWithBenchmark from "../../toolsPage/AllocationListWithBenchmark";
import BenchMarkComparatorChart from "../../chart/toolsPage/BenchMarkComparatorChart";
import PriceEvaluationChart from "../../chart/toolsPage/PriceEvaluationChart";


const BenchmarkState = () => {
    const [assets, setAssets] = React.useState<Allocations[]>([])
    const [quotes, setQuotes] = React.useState<string[]>([])
    const [benchmark, setBenchmark] = React.useState('SPY');
    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);
    const [totalPL, setPL] = React.useState(0)

    useEffect(() => {
        const load = async () => {
            const fetchedSectors = await GetPortfolioAssetAllocation(currentPortfolioStore.name)
            setAssets(fetchedSectors)
        }
        load()
    }, [currentPortfolioStore])

    return (
        <Box>
            <BaseTabComponent
                type={"sector"}
                leftUpperComponent={<AllocationPieChart
                    sectors={assets}
                    title={"Asset allocation"}
                />}
                leftMiddleComponent={<PriceEvaluationChart
                    quotes={quotes}
                    title={"Assets value evaluation comparison"}
                    benchmark={benchmark}
                />}
                leftBottomComponent={<BenchMarkComparatorChart
                    quotes={quotes}
                    title={"Benchmark's P&L comparison"}
                    benchmark={benchmark}
                />}
                rightUpperComponent={<AllocationListWithBenchmark
                    sectors={assets}
                    title={"Asset list"}
                    setQuotes={setQuotes}
                    benchmark={benchmark}
                    setBenchmark={setBenchmark}
                />}
                rightBottomComponent={<AllocationDetails
                    title={"Allocation details"}
                    quotes={quotes}
                    type={"asset"}
                    sectors={assets}
                    pl={totalPL}
                />}
            />
        </Box>
    )
}


export default BenchmarkState
