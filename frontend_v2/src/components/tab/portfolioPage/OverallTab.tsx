import {Box, Button, Container, Paper, Theme, Typography} from "@mui/material";
import PerformanceChart from "../../chart/portfolioPage/PerformanceChart";
import PortfolioDetails from "../../portfolioPage/PortfolioDetails";
import AddCashModal from "../../modal/AddCashModal";
import NewPortfolioModal from "../../modal/NewPortfolioModal";
import DeletePortfolioModal from "../../modal/DeletePortfolioModal";
import React, {useEffect} from "react";
import {makeStyles} from "@mui/styles";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: 2
    },
    page: {
        marginTop: '5px',
        height: '90vh',
    },
    button: {
        width: '90px'
    },
    card: {
        background: 'rgb(24,28,36)'
    }

}))

const OverallTab = () => {
    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);

    const styles = useStyles()
    const [addCashState, handleCashAdd] = React.useState(false)
    const [quotes, setQuotes] = React.useState<string[]>([])
    const [newPortfolioModalState, handleNewPortfolioModalState] = React.useState(false)
    const [deletePortfolioModalState, handleDeletePortfolioModalState] = React.useState(false)
    const closeAddCashModal = () => {
        handleCashAdd(false)
    }
    const CanBeRemoved = () => {
        return currentPortfolioStore.name === 'Default'
    }

    useEffect(() => {
      const parsedQuotes = currentPortfolioStore.assets.map(a => a.asset.type.ticker.toUpperCase())
      setQuotes(parsedQuotes)
    }, [currentPortfolioStore.name])

    return (
        <Box>
            <Container sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: 'center'
            }}>
                <Typography color={"primary"} variant={"h4"} sx={{
                    paddingTop: '10px'
                }}>
                    Current portfolio
                </Typography>
                <Box/>
                <Box sx={{
                    marginTop: '20px',
                    display: 'flex',
                    justifyContent: 'space-between',
                    gap: '30px',
                }}>
                    <Paper elevation={4} className={styles.card}>
                        <PerformanceChart benchmark={false} quotes={quotes} cashInflow={false} title={"All portfolio value"}/>
                    </Paper>
                    <Paper className={styles.card} elevation={4} sx={{
                        width: '400px',
                    }}>
                        <Box>
                            <PortfolioDetails quotes={quotes}/>
                        </Box>
                    </Paper>
                </Box>
                <Box sx={{
                    height: '50px',
                    width: '400px',
                    display: 'flex',
                    justifyContent: 'end',
                    alignItems: 'end',
                    alignSelf: 'end',
                    gap: '5px',
                    marginRight: '10px'
                }}>
                    <Button className={styles.button} variant={"contained"} color={"info"}
                            onClick={() => handleNewPortfolioModalState(true)}>Add</Button>
                    <Button className={styles.button} variant={"contained"} color={"primary"}
                            onClick={() => handleDeletePortfolioModalState(true)}>Update</Button>
                    <Button className={styles.button} disabled={CanBeRemoved()} variant={"contained"}
                            color={"error"}
                            onClick={() => handleDeletePortfolioModalState(true)}>Delete</Button>
                    {/*<Button variant={"outlined"} onClick={() => handleCashAdd(true)}>Add cash</Button>*/}
                </Box>
            </Container>
            <AddCashModal open={addCashState} handleClose={closeAddCashModal}/>
            <NewPortfolioModal open={newPortfolioModalState} handleClose={() => {
                handleNewPortfolioModalState(false)
            }}/>
            <DeletePortfolioModal open={deletePortfolioModalState} handleClose={() => {
                handleDeletePortfolioModalState(false)
            }}/>
        </Box>
    )
}

export default OverallTab
