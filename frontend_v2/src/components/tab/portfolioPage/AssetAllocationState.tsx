import {Box} from "@mui/material";
import BaseTabComponent from "./BaseTabComponent";
import AllocationPieChart from "../../chart/portfolioPage/AllocationPieChart";
import AllocationList from "../../portfolioPage/AllocationList";
import PerformanceChart from "../../chart/portfolioPage/PerformanceChart";
import React, {useEffect} from "react";
import {Allocations, GetPortfolioAssetAllocation} from "../../../api/Stats";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";
import PLChart from "../../chart/portfolioPage/PLChart";
import AllocationDetails from "../../portfolioPage/AllocationDetails";


const AssetAllocationState = () => {
    const [assets, setAssets] = React.useState<Allocations[]>([])
    const [quotes, setQuotes] = React.useState<string[]>([])
    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);
    const [totalPL, setPL] = React.useState(0)

    useEffect(() => {
        const load = async () => {
            const fetchedSectors = await GetPortfolioAssetAllocation(currentPortfolioStore.name)
            setAssets(fetchedSectors)
        }
        load()
    }, [currentPortfolioStore])

    return (
        <Box>
            <BaseTabComponent
                type={"sector"}
                leftUpperComponent={<AllocationPieChart
                    sectors={assets}
                    title={"Asset allocation"}
                />}
                leftMiddleComponent={<PerformanceChart
                    quotes={quotes}
                    cashInflow={false}
                    title={"Assets value evaluation"}
                    benchmark={false}
                />}
                leftBottomComponent={<PLChart
                    quotes={quotes}
                    setPL={setPL}
                    // cashInflow={true}
                    title={"Asset P&L"}
                />}
                rightUpperComponent={<AllocationList
                    sectors={assets}
                    setQuotes={setQuotes}
                    title={"Asset list"}
                />}
                rightBottomComponent={<AllocationDetails
                    title={"Allocation details"}
                    quotes={quotes}
                    type={"asset"}
                    sectors={assets}
                    pl={totalPL}
                />}
            />
        </Box>
    )
}


export default AssetAllocationState
