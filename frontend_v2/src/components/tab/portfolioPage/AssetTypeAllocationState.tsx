import {Box} from "@mui/material";
import BaseTabComponent from "./BaseTabComponent";
import AllocationPieChart from "../../chart/portfolioPage/AllocationPieChart";
import AllocationList from "../../portfolioPage/AllocationList";
import PerformanceChart from "../../chart/portfolioPage/PerformanceChart";
import React, {useEffect} from "react";
import {
    Allocations,
    GetPortfolioAssetTypeAllocation,
    GetQuotesPerformance,
    GetQuotesPerformanceWithoutInflow, Performance
} from "../../../api/Stats";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";
import PLChart from "../../chart/portfolioPage/PLChart";
import AllocationDetails from "../../portfolioPage/AllocationDetails";


const AssetTypeAllocationState = () => {
    const [assetTypes, setAssetTypes] = React.useState<Allocations[]>([])
    const [totalPL, setPL] = React.useState(0)
    const [performanceWithoutInflow, setPerformanceWithoutInflow] = React.useState<Performance>({} as Performance)
    const [performance, setPerformance] = React.useState<Performance>({} as Performance)
    const [quotes, setQuotes] = React.useState<string[]>([])
    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);

    useEffect(() => {
        const load = async () => {
            const fetchedAssetTypes = await GetPortfolioAssetTypeAllocation(currentPortfolioStore.name)

            setAssetTypes(fetchedAssetTypes)
        }
        load()
    }, [currentPortfolioStore])

    useEffect(() => {
        const load = async () => {
            const fetchedPerformanceWithoutInflow = await GetQuotesPerformanceWithoutInflow(currentPortfolioStore.name, quotes)
            const fetchedPerformance = await GetQuotesPerformance(currentPortfolioStore.name, quotes)
            setPerformanceWithoutInflow(fetchedPerformanceWithoutInflow)
            setPerformance(fetchedPerformance)
        }
        load()
    }, [currentPortfolioStore.name, quotes])

    return (
        <Box>
            <BaseTabComponent
                type={"sector"}
                leftUpperComponent={<AllocationPieChart
                    sectors={assetTypes}
                    title={"Asset type allocation"}
                />}
                leftMiddleComponent={<PerformanceChart
                    quotes={quotes}
                    cashInflow={false}
                    title={"Assets value evaluation"}
                    benchmark={false}
                />}
                leftBottomComponent={<PLChart
                    quotes={quotes}
                    setPL={setPL}
                    // cashInflow={true}
                    title={"Sector P&L"}
                />}
                rightUpperComponent={<AllocationList
                    sectors={assetTypes}
                    setQuotes={setQuotes}
                    title={"Asset types"}
                />}
                rightBottomComponent={<AllocationDetails
                    title={"Allocation details"}
                    quotes={quotes}
                    type={"type"}
                    pl={totalPL}
                    sectors={assetTypes}
                />}
            />
        </Box>
    )
}


export default AssetTypeAllocationState
