import {Box} from "@mui/material";
import BaseTabComponent from "./BaseTabComponent";
import AllocationPieChart from "../../chart/portfolioPage/AllocationPieChart";
import AllocationList from "../../portfolioPage/AllocationList";
import PerformanceChart from "../../chart/portfolioPage/PerformanceChart";
import React, {useEffect} from "react";
import {Allocations, GetPortfolioCountryAllocation} from "../../../api/Stats";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";
import PLChart from "../../chart/portfolioPage/PLChart";
import AllocationDetails from "../../portfolioPage/AllocationDetails";


const CountryAllocationState = () => {
    const [countries, setCountries] = React.useState<Allocations[]>([])
    const [quotes, setQuotes] = React.useState<string[]>([])
    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);
    const [totalPL, setPL] = React.useState(0)

    useEffect(() => {
        const load = async () => {
            const fetchedSectors = await GetPortfolioCountryAllocation(currentPortfolioStore.name)
            setCountries(fetchedSectors)
        }
        load()
    }, [currentPortfolioStore])

    return (
        <Box>
            <BaseTabComponent
                type={"sector"}
                leftUpperComponent={<AllocationPieChart
                    sectors={countries}
                    title={"Country allocation"}
                />}
                leftMiddleComponent={<PerformanceChart
                    quotes={quotes}
                    cashInflow={false}
                    title={"Assets value evaluation"}
                    benchmark={false}
                />}
                leftBottomComponent={<PLChart
                    quotes={quotes}
                    setPL={setPL}
                    // cashInflow={true}
                    title={"Country P&L"}
                />}
                rightUpperComponent={<AllocationList
                    sectors={countries}
                    setQuotes={setQuotes}
                    title={"Countries list"}
                />}
                rightBottomComponent={<AllocationDetails
                    title={"Allocation details"}
                    quotes={quotes}
                    type={"country"}
                    pl={totalPL}
                    sectors={countries}
                />}
            />
        </Box>
    )
}


export default CountryAllocationState
