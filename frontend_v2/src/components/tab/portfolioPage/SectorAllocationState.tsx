import {Box} from "@mui/material";
import BaseTabComponent from "./BaseTabComponent";
import AllocationPieChart from "../../chart/portfolioPage/AllocationPieChart";
import AllocationList from "../../portfolioPage/AllocationList";
import PerformanceChart from "../../chart/portfolioPage/PerformanceChart";
import React, {useEffect} from "react";
import {Allocations, GetPortfolioSectorAllocation} from "../../../api/Stats";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";
import PLChart from "../../chart/portfolioPage/PLChart";
import AllocationDetails from "../../portfolioPage/AllocationDetails";


const SectorAllocationState = () => {
    const [sectors, setSectors] = React.useState<Allocations[]>([])
    const [quotes, setQuotes] = React.useState<string[]>([])
    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);
    const [totalPL, setPL] = React.useState(0)

    useEffect(() => {
        const load = async () => {
            const fetchedSectors = await GetPortfolioSectorAllocation(currentPortfolioStore.name)
            setSectors(fetchedSectors)
        }
        load()
    }, [currentPortfolioStore])

    return (
        <Box>
            <BaseTabComponent
                type={"sector"}
                leftUpperComponent={<AllocationPieChart
                    sectors={sectors}
                    title={"Sector allocation"}
                />}
                leftMiddleComponent={<PerformanceChart
                    quotes={quotes}
                    cashInflow={false}
                    title={"Assets value evaluation"}
                    benchmark={false}
                />}
                leftBottomComponent={<PLChart
                    quotes={quotes}
                    setPL={setPL}
                    // cashInflow={true}
                    title={"Sector P&L"}
                />}
                rightUpperComponent={<AllocationList
                    sectors={sectors}
                    setQuotes={setQuotes}
                    title={"Allocation list"}
                />}
                rightBottomComponent={<AllocationDetails
                    title={"Allocation details"}
                    quotes={quotes}
                    type={"sector"}
                    pl={totalPL}
                    sectors={sectors}
                />}
            />
        </Box>
    )
}


export default SectorAllocationState
