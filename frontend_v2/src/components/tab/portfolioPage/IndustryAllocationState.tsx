import {Box} from "@mui/material";
import BaseTabComponent from "./BaseTabComponent";
import AllocationPieChart from "../../chart/portfolioPage/AllocationPieChart";
import AllocationList from "../../portfolioPage/AllocationList";
import PerformanceChart from "../../chart/portfolioPage/PerformanceChart";
import React, {useEffect} from "react";
import {Allocations, GetPortfolioIndustryAllocation} from "../../../api/Stats";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";
import PLChart from "../../chart/portfolioPage/PLChart";
import AllocationDetails from "../../portfolioPage/AllocationDetails";


const IndustryAllocationState = () => {
    const [industries, setIndustries] = React.useState<Allocations[]>([])
    const [quotes, setQuotes] = React.useState<string[]>([])
    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);
    const [totalPL, setPL] = React.useState(0)

    useEffect(() => {
        const load = async () => {
            const fetchedSectors = await GetPortfolioIndustryAllocation(currentPortfolioStore.name)
            setIndustries(fetchedSectors)
        }
        load()
    }, [currentPortfolioStore])

    return (
        <Box>
            <BaseTabComponent
                type={"sector"}
                leftUpperComponent={<AllocationPieChart
                    sectors={industries}
                    title={"Industry allocation"}
                />}
                leftMiddleComponent={<PerformanceChart
                    quotes={quotes}
                    cashInflow={false}
                    title={"Assets value evaluation"}
                    benchmark={false}
                />}
                leftBottomComponent={<PLChart
                    quotes={quotes}
                    setPL={setPL}
                    // cashInflow={true}
                    title={"Industry P&L"}
                />}
                rightUpperComponent={<AllocationList
                    sectors={industries}
                    setQuotes={setQuotes}
                    title={"Industry list"}
                />}
                rightBottomComponent={<AllocationDetails
                    title={"Allocation details"}
                    quotes={quotes}
                    type={"industry"}
                    pl={totalPL}
                    sectors={industries}
                />}
            />
        </Box>
    )
}


export default IndustryAllocationState
