import {Box, Paper} from "@mui/material";
import React from "react";

type AllocationType = 'sector' | 'country' | 'industry'
interface BaseTabComponentProps {
    leftUpperComponent: any,
    leftMiddleComponent: any,
    leftBottomComponent: any,
    rightBottomComponent: any,
    rightUpperComponent: any,
    type: AllocationType,
}

const BaseTabComponent:React.FC<BaseTabComponentProps> = ({leftUpperComponent, leftBottomComponent, rightUpperComponent, rightBottomComponent, leftMiddleComponent,type}) => {


    return (
        <Box sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            height: '85vh',
            flexDirection: 'row',
            gap: '30px'
        }}>
            <Box id={'left'} sx={{
                display: 'flex',
                flexDirection: 'column',
                gap: '13px'
            }}>
                <Paper elevation={10} sx={{
                    width: '40vw',
                    height: '24vh',
                    background: 'rgb(18,29,34)',
                    display: 'flex',
                    justifyContent: 'center',
                    borderRadius: '10px',
                }}>
                    {leftUpperComponent}
                </Paper>
                <Paper elevation={10} sx={{
                    width: '40vw',
                    height: '28vh',
                    background: 'rgb(18,29,34)',
                    display: 'flex',
                    justifyContent: 'center',
                    borderRadius: '10px',
                }}>
                    {leftMiddleComponent}
                </Paper>
                <Paper elevation={10} sx={{
                    width: '40vw',
                    height: '28vh',
                    background: 'rgb(18,29,34)',
                    display: 'flex',
                    justifyContent: 'center',
                    borderRadius: '10px',

                }}>
                    {leftBottomComponent}
                </Paper>
            </Box>
            <Box id={'right'} sx={{
                display: 'flex',
                flexDirection: 'column',
                gap: '10px',
            }}>
                <Paper elevation={10} sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    height: '53vh',
                    width: '25vw',
                    background: 'rgb(18,29,34)',
                    alignSelf: 'end',
                    borderRadius: '10px',

                }}>
                    {rightUpperComponent}
                </Paper>
                <Paper elevation={10} sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    height: '28vh',
                    width: '25vw',
                    background: 'rgb(18,29,34)',
                    alignSelf: 'end',
                    borderRadius: '10px',
                }}>
                    {rightBottomComponent}
                </Paper>
            </Box>
        </Box>
    )
}

export default BaseTabComponent