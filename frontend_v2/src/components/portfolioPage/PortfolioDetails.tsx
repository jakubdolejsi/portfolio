import {Divider, Box, Typography} from "@mui/material";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../store/slice/CurrentPortfolioSlice";
import {IPortfolioModel} from "../../pages/auth/PortfolioPage";
import React, {useEffect} from "react";
import {GetQuotesPerformanceWithoutInflow} from "../../api/Stats";

interface PortfolioDetailsProps {
    quotes: string[]
}

const PortfolioDetails: React.FC<PortfolioDetailsProps> = ({quotes}) => {
    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);
    const [pl, setPl] = React.useState(0)
    const [capitalInvested, setCapitalInvested] = React.useState(0)


    useEffect(() => {
            const load = async () => {
                const performance = await GetQuotesPerformanceWithoutInflow(currentPortfolioStore.name, quotes)
                if (performance.performance.length !== 0) {
                    setPl(parseFloat(performance.performance[performance.performance.length - 1].value.toFixed(1)))
                }
                let sum = 0
                currentPortfolioStore.assets.forEach(asset => {
                    if (quotes.includes(asset.asset.type.ticker)) {
                        // @ts-ignore
                        sum += asset?.totalCashInvested
                    }
                })
                setCapitalInvested(parseFloat(sum.toFixed(1)))
            }
            load()
        },
        [currentPortfolioStore.name, quotes]
    )


    return (
        <Box sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center'
        }}>
            <Box>
                <Typography color={"primary"} variant={"h5"} sx={{
                    marginY: '3px'
                }}>
                    Portfolio Details
                </Typography>
                <Divider variant="fullWidth"/>
            </Box>
            <Box sx={{
                display: 'flex',
                alignSelf: 'start',
                flexDirection: 'column',
                marginLeft: '10px'
            }}>
                <Typography variant={"body1"}>Name: {currentPortfolioStore.name}</Typography>
                <Typography variant={"body1"}>Currency: {currentPortfolioStore.currency}</Typography>
                <Typography variant={"body1"}>Capital invested: {capitalInvested} ({currentPortfolioStore.currency})</Typography>
                <Typography variant={"body1"}>Total PL: {pl} ({currentPortfolioStore.currency})</Typography>
                <Typography variant={"body1"}>Number of assets: {currentPortfolioStore.assets.length}</Typography>
                <Typography variant={"body1"}>Number of trades: {currentPortfolioStore.tradesCount}</Typography>
            </Box>
        </Box>
    )
}

export default PortfolioDetails
