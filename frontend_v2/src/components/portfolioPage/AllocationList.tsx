import {Divider, Box, Checkbox, List, ListItem, ListItemIcon, ListSubheader, Typography} from "@mui/material";
import React, {useEffect} from "react";
import {Allocations} from "../../api/Stats";


interface AllocationCheckbox {
    ticker: string, // asset's ticker
    value: boolean
}

interface AllocationState {
    name: string,
    values: AllocationCheckbox[]
}

interface AllocationListProps {
    sectors: Allocations[],
    setQuotes: (prev: string[]) => void,
    title: string,
    comparator?: boolean
}

const AllocationList: React.FC<AllocationListProps> = ({sectors, setQuotes, title, comparator=false}) => {

    const [checked, setChecked] = React.useState<AllocationState[]>([]);


    const initializeState = () => {
        const initArr: AllocationState[] = [];
        sectors.forEach(a => {
            const newItem: AllocationState = {
                name: a.name,
                values: []
            }
            a.values.forEach(v => {
                newItem.values.push({
                    ticker: v.ticker,
                    value: false
                })
            })
            initArr.push(newItem)
        })
        setChecked(initArr)
    }
    useEffect(() => {
        initializeState()
    }, [sectors])


    const getCheckedTickers = () => {
        // console.log('checked', checked)
        const tickers: string[] = []
        checked.forEach(c => {
            c.values.forEach(v => {
                if (v.value) tickers.push(v.ticker)
            })
        })
        return tickers
    }

    const _findCheckbox = (name: string, ticker: string) => {
        const found = checked.find(c => c.name === name)
        if (!found) return false
        return found.values.find(v => v.ticker === ticker)
    }

    const subListChecked = (name: string, ticker: string) => {
        const ok = _findCheckbox(name, ticker)
        if (ok) return ok.value
        return false
    }

    const handleCheck = (name: string, ticker: string) => {
        const newA = checked.map(c => {
            if (c.name === name) {
                c.values.map(v => {
                    if (v.ticker === ticker) {
                        v.value = !v.value
                    }
                    return v
                })
            }
            return c
        })
        setChecked(newA)
        const tickers = getCheckedTickers()
        setQuotes(tickers)
    }

    const allocationChecked = (name: string) => {
        const found = checked.find(c => c.name === name)
        if (!found) return false
        return found.values.every(v => v.value)
    }
    const indeterminate = (name: string) => {
        const found = checked.find(c => c.name === name)
        if (!found) return false
        const t = found.values.some(s => s.value)
        const f = found.values.some(s => !s.value)
        return t && f
    }
    const handleAllocationClick = (name: string) => {
        const newA = checked.map(c => {
            if (c.name === name) {
                const someChecked = indeterminate(name)
                console.log('some checked', someChecked)
                if (someChecked) {
                    c.values.map(v => {
                        v.value = true
                        return v
                    })
                } else {
                    const every = allocationChecked(name)
                    if (every) {
                        c.values.map(v => {
                            v.value = false
                            return v
                        })
                    } else {
                        c.values.map(v => {
                            v.value = true
                            return v
                        })
                    }

                }
            }
            return c
        })
        setChecked(newA)
        const tickers = getCheckedTickers()
        setQuotes(tickers)
        // GetQuotesPerformance(currentPortfolioStore.name, tickers)
    }

    return (
        <Box sx={{}}>
            <List dense sx={{
                maxWidth: '500px',
                height: comparator ? '43vh' : '53vh',
                display: 'flex',
                alignItems: 'center',
                flexDirection: 'column',
            }} subheader={
                <ListSubheader>
                    <Typography color={"primary"} variant={"h5"} sx={{
                        marginBottom: '20px'
                    }}>{title}</Typography>
                </ListSubheader>
            }>
                <Box sx={{
                    background: 'rgb(24,38,44)',
                    width: '300px',
                    height: '53vh',
                    overflowY: 'scroll',
                    overflowX: 'hidden',
                    // overflow: 'auto',
                    '&::-webkit-scrollbar': {
                        width: '0.4em'
                    },
                    '&::-webkit-scrollbar-track': {
                        boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
                        webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
                    },
                    '&::-webkit-scrollbar-thumb': {
                        backgroundColor: 'rgba(128,124,124,0.1)',
                        // outline: '1px solid #F75C03',
                        borderRadius: '2px',
                        // color: '#3f352f'
                    }
                }}>
                    {sectors.map(s => {
                        return (
                            <ListItem>
                                <List sx={{
                                    padding: 0
                                }} dense subheader={
                                    <ListSubheader disableGutters disableSticky sx={{
                                        display: 'flex',
                                        justifyContent: 'start',
                                        padding: 0
                                    }}>
                                        <Checkbox
                                            checked={allocationChecked(s.name)}
                                            indeterminate={indeterminate(s.name)}
                                            onChange={() => handleAllocationClick(s.name)}
                                        />
                                        <Typography color={"primary"} sx={{
                                            fontSize: '17px'
                                        }}>{s.name}</Typography>
                                    </ListSubheader>
                                }>
                                    <Divider light />
                                    {s.values.map((v, index) => {
                                        return (
                                            <ListItem sx={{
                                                marginLeft: '30px',
                                                '&:hover': {
                                                    // backgroundColor: '#161318',
                                                },
                                                padding: 0
                                            }}>
                                                <ListItemIcon>
                                                    <Checkbox
                                                        checked={subListChecked(s.name, v.ticker)}
                                                        onChange={() => {
                                                            console.log('click : ', v, s)
                                                            handleCheck(s.name, v.ticker)
                                                        }}
                                                    />
                                                </ListItemIcon>
                                                <Typography
                                                    color={subListChecked(s.name, v.ticker) === true ? "#29b6f6" : '#fff'}
                                                    sx={{
                                                        fontSize: '14px'
                                                    }}>{v.ticker}: {v.weight}</Typography>
                                            </ListItem>
                                        )
                                    })}
                                </List>
                            </ListItem>
                        )
                    })}
                </Box>
            </List>
        </Box>
    )
}


export default AllocationList
