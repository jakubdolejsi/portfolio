import {Box, Grid, Typography} from "@mui/material";
import {Allocations} from "../../api/Stats";
import React, {useEffect} from "react";
import TypographyPair from "../Typography/TypographyPair";
import {IPortfolioModel} from "../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../store/slice/CurrentPortfolioSlice";


type AllocationType = 'sector' | 'country' | 'industry' | 'category' | 'type' | 'asset'

interface AllocationDetailsProps {
    title: string,
    sectors: Allocations[],
    quotes: string[],
    type: AllocationType,
    pl: number
    // performance: Performance
}

interface AllocationDetails {

}

const AllocationDetails: React.FC<AllocationDetailsProps> = ({sectors, title, quotes, type, pl}) => {
    const [cashInvested, setCashInvested] = React.useState(0)
    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);

    const allAssets = () => {
        let count = 0
        sectors.map(s => count += s.values.length)
        return count
    }

    useEffect(() => {
        let sum = 0
        currentPortfolioStore.assets.forEach(asset => {
            if (quotes.includes(asset.asset.type.ticker)) {
                // @ts-ignore
                sum += asset?.totalCashInvested
            }
        })
        setCashInvested(sum)
        console.log('sum', sum)
    }, [quotes])
    return (
        <Box sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'space-between',
            height: '100%',
            width: '100%',
        }}>
            <Typography color={"primary"} variant={"h5"} sx={{
                marginBottom: '20px'
            }}>{title}
            </Typography>
            <Box sx={{
                display: 'flex',
                flexDirection: 'row',
                width: '100%',
            }}>
                <Grid container rowSpacing={1} sx={{
                    paddingLeft: '10px'
                }}>
                    <TypographyPair name={`Total assets in ${type}`} value={quotes.length}/>
                    <TypographyPair name={`Weight in portfolio`} value={10}/>
                    {/*<TypographyPair name={`Max value`} value={allAssets()}/>*/}
                    {/*<TypographyPair name={`Min value`} value={allAssets()}/>*/}
                    <TypographyPair name={`Capital invested`} value={`${cashInvested.toFixed(2)} ($)`}/>
                    <TypographyPair name={`Total P&L`} value={`${pl.toFixed(2)} ($)`}/>
                </Grid>

            </Box>
        </Box>
    )
}

export default AllocationDetails
