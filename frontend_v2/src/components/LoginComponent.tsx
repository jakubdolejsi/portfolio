import React from "react";
import {useAuth0} from "@auth0/auth0-react";
import {makeStyles} from "@mui/styles";
import {createTheme, ThemeProvider} from '@mui/material/styles';
import {ButtonProps} from "@mui/material";
import Button from "@mui/material/Button";
import {deepOrange} from "@mui/material/colors";

const useStyles = makeStyles({
    root: {
        color: 'blue',
    }
});

const theme = createTheme({
    palette: {
        mode: "dark",
        primary: {
            main: deepOrange[600],
        }
    },
    typography: {
        fontSize: 13,
        fontWeightBold: 1,
    }
});


const LoginComponent: React.FC<ButtonProps> = (props) => {

    const {loginWithRedirect} = useAuth0();
    const Login = () => {
        loginWithRedirect({
            screen_hint: "signup",
        })
    }
    const text = props.children;
    const classes = useStyles();


    return (
        <div>
            <div className={classes.root}>
                <ThemeProvider theme={theme}>
                    <Button
                        id={"login-button"}
                        size={"large"}
                        variant={"contained"}
                        color={"primary"}
                        type={"submit"}
                        onClick={Login}
                    >
                        {text}
                    </Button>
                </ThemeProvider>


            </div>
        </div>
    );
};

export default LoginComponent;
