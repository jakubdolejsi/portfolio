import {useAuth0} from "@auth0/auth0-react";
import {AppBar, Button, Paper, Stack, Theme, Toolbar, Typography} from "@mui/material";
import {createStyles, makeStyles} from "@mui/styles";
import {NavLink} from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            display: 'flex',
            height: '70px',
            // maxHeight: '7vh',
            justifyContent: 'flex-end',
            background: 'linear-gradient(109.04deg, #00171F 0%, rgba(25, 25, 35, 0.92) 100%)'
            // border: '1px #0D1217 solid',
        },
        menuButton: {
            marginRight: 2,
        },
        title: {
            flexGrow: 1,
        },
        link: {
            textDecoration: 'none',
            color: '#F75C03',
        },
        logout: {
            textDecoration: 'none',
            marginLeft: 2,
            marginRight: 0,
        },
        active: {
            // color: 'red',
            color: '#29b6f6',
            fontWeight: 'bold!important',
        },
        generalButton: {
            fontWeight: 'inherit',
        },
    }),
);


export const Navbar = () => {
    const {logout} = useAuth0();
    const classes = useStyles();


    return (
        <AppBar position="static" className={classes.root}>
            <Paper>
                <Toolbar className={classes.root}>
                    <Stack spacing={1} direction={"row"}>
                        <NavLink to="/auth" className={classes.link} exact activeClassName={classes.active}>
                            <Button id={"home-navbar"} color={"inherit"} className={classes.generalButton}>
                                Home
                            </Button>
                        </NavLink>
                        <NavLink to="/auth/trade" className={classes.link} activeClassName={classes.active}>
                            <Button id={"trade-navbar"} color={"inherit"} className={classes.generalButton}>
                                Trade
                            </Button>
                        </NavLink>
                        <NavLink to="/auth/asset" className={classes.link} activeClassName={classes.active}>
                            <Button id={"asset-navbar"} color={"inherit"} className={classes.generalButton}>
                                Asset
                            </Button>
                        </NavLink>
                        <NavLink to="/auth/portfolio" className={classes.link} activeClassName={classes.active}>
                            <Button id={"portfolio-navbar"} color={"inherit"} className={classes.generalButton}>
                                Portfolio
                            </Button>
                        </NavLink>
                        <NavLink to="/auth/tools" className={classes.link} activeClassName={classes.active}>
                            <Button id={"tools-navbar"} color={"inherit"} className={classes.generalButton}>
                                Tools
                            </Button>
                        </NavLink>
                        <Button
                            color={"primary"}
                            variant={"outlined"}
                            style={{
                                marginLeft: '30px',
                                marginTop: '7px',
                                height: '37px'
                            }}
                            onClick={() =>
                                logout({
                                    returnTo: window.location.origin,
                                })}>
                            <Typography variant={"subtitle2"}>
                                Logout
                            </Typography>
                        </Button>
                    </Stack>
                </Toolbar>
            </Paper>
        </AppBar>

        // </ThemeProvider>
    );
}
