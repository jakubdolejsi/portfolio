import {AppBar, Button, Paper, Stack, Theme, Toolbar} from "@mui/material";
import {createStyles, makeStyles} from "@mui/styles";
import {NavLink} from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            display: 'flex',
            height: '7vh',
            // maxHeight: '7vh',
            justifyContent: 'flex-end',
            background: 'linear-gradient(109.04deg, #00171F 0%, rgba(25, 25, 35, 0.92) 100%)'
            // border: '1px #0D1217 solid',
        },
        menuButton: {
            marginRight: 2,
        },
        title: {
            flexGrow: 1,
        },
        link: {
            textDecoration: 'none',
            color: '#F75C03',
        },
        logout: {
            textDecoration: 'none',
            marginLeft: 2,
            marginRight: 0,
        },
        active: {
            fontWeight: 'bold',
        },
        generalButton: {
            fontWeight: 'inherit',
        },
    }),
);

export const Navbar = () => {
    const classes = useStyles();


    return (
        // <ThemeProvider theme={navbarTheme}>
        <AppBar position="static" className={classes.root}>
            <Paper>
                <Toolbar className={classes.root}>
                    <Stack spacing={1} direction={"row"}>
                        <NavLink to="/" className={classes.link} activeClassName={classes.active}>
                            <Button id={"home-navbar"} color={"inherit"} className={classes.generalButton}>
                                Home
                            </Button>
                        </NavLink>
                        <NavLink to="/news" className={classes.link} activeClassName={classes.active}>
                            <Button id={"news-navbar"} color={"inherit"} className={classes.generalButton}>
                                News
                            </Button>
                        </NavLink>
                        <NavLink to="/tutorials" className={classes.link} activeClassName={classes.active}>
                            <Button id={"tutorial-navbar"} color={"inherit"} className={classes.generalButton}>
                                Tutorials
                            </Button>
                        </NavLink>
                        <NavLink to="/about" className={classes.link} activeClassName={classes.active}>
                            <Button id={"about-navbar"} color={"inherit"} className={classes.generalButton}>
                                About
                            </Button>
                        </NavLink>
                    </Stack>
                </Toolbar>
            </Paper>
        </AppBar>
        // </ThemeProvider>
    );
}
