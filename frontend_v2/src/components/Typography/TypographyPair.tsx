import {Grid, Theme, Typography} from "@mui/material";
import React from "react";
import {createStyles, makeStyles} from "@mui/styles";



export interface SummaryElementsProps {
    name: string,
    value: string | number | undefined
}

const TypographyPair: React.FC<SummaryElementsProps> = ({name, value}) => {

    if (value === undefined) {
        return (
            <></>
        )
    }
    return (
        <>
            <Grid item xs={5}>
                <Typography fontWeight={"bolder"}>{name}: </Typography>
            </Grid>
            <Grid item xs={7}>
                <Typography color={"#fff"}>{value}</Typography>
            </Grid>
        </>
    )
}

export default TypographyPair
