import React from "react";
import {
    Box,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableFooter,
    TableHead,
    TablePagination,
    TableRow
} from "@mui/material";
import {styled} from '@mui/material/styles';
import {useHistory} from "react-router-dom";
import Button from "@mui/material/Button";
import CloseIcon from '@mui/icons-material/Close';
import {useDispatch} from "react-redux";
import {setCurrentAsset} from "../../store/slice/CurrentAssetSlice";
import {useTrade} from "../../api/query/TradeQuery";
import {ITrade} from "../../pages/auth/TradePage";


interface ITradeTable {

}

// background: 'rgb(24,28,36)' - navbar background color
const StyledTableRow = styled(TableRow)(({theme}) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        borderBottom: '1px solid black',
    },
}));

export const TradesTable: React.FC<ITradeTable> = (props) => {
    const [page, setPage] = React.useState(0);
    const dispatch = useDispatch();
    const history = useHistory()
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const tradeState = useTrade()

    const {mutate: deleteTrade} = tradeState.useDeleteTrade()


    const {data: trades, status} = tradeState.useAllTrades()
    const allTrades: ITrade[] = trades ?? [] as ITrade[]


    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - allTrades.length) : 0;

    const rowsPerPageOptions = [5, 10, 15, 20]
    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number,
    ) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };


    const redirectToAsset = (ticker: string) => {
        history.push(`/auth/asset/`)
    }
    return (
        <Box sx={{
            boxShadow: 10
        }}>
            <TableContainer component={Paper}>
                <Table sx={{
                    minWidth: 650,
                }}
                       aria-label={"trade table"}
                >
                    <TableHead>
                        <TableRow sx={{
                            background: '#20242d',
                        }}>
                            <TableCell sx={{fontWeight: '600'}}>Name</TableCell>
                            <TableCell sx={{fontWeight: '600'}}>Ticker</TableCell>
                            <TableCell sx={{fontWeight: '600'}}>Type</TableCell>
                            <TableCell sx={{fontWeight: '600'}}>Amount</TableCell>
                            <TableCell sx={{fontWeight: '600'}}>Price per one</TableCell>
                            <TableCell sx={{fontWeight: '600'}}>Total price</TableCell>
                            <TableCell sx={{fontWeight: '600'}}>Date</TableCell>
                            <TableCell sx={{fontWeight: '600'}}>Delete</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {(rowsPerPage > 0
                                ? allTrades.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                : allTrades
                        ).map(trade => {
                            return <StyledTableRow key={trade.id}>
                                <TableCell
                                    onClick={() => {
                                        console.log('ID', trade.id)
                                        dispatch(setCurrentAsset({
                                            ticker: trade.ticker,
                                            index: 1
                                        }))
                                        redirectToAsset('/auth/asset')
                                    }}
                                    sx={{
                                        cursor: 'pointer'
                                    }}
                                    component={"th"}
                                    scope={"row"}>
                                    {trade.name}
                                </TableCell>
                                <TableCell align={"left"}>{trade.ticker}</TableCell>
                                <TableCell align={"left"}>{trade.type}</TableCell>
                                <TableCell align={"left"}>{trade.amount}</TableCell>
                                <TableCell align={"left"}>{trade.pricePerOne}</TableCell>
                                <TableCell align={"left"}>{trade.totalPrice}</TableCell>
                                <TableCell align={"left"}>{trade.date}</TableCell>
                                <TableCell align={"left"}>
                                    <Button color={"error"} variant={"outlined"}
                                            startIcon={<CloseIcon sx={{
                                                width: '20px'
                                            }}/>}
                                            onClick={() => deleteTrade(trade.id)}
                                    />
                                </TableCell>
                            </StyledTableRow>
                        })}
                        {emptyRows > 0 && (
                            <StyledTableRow style={{height: 53 * emptyRows}}>
                                <TableCell colSpan={6}/>
                            </StyledTableRow>
                        )}
                    </TableBody>
                    <TableFooter>
                        <TableRow>
                            <TablePagination
                                count={allTrades.length}
                                page={page}
                                onPageChange={handleChangePage}
                                onRowsPerPageChange={handleChangeRowsPerPage}
                                rowsPerPageOptions={rowsPerPageOptions}
                                rowsPerPage={rowsPerPage}/>
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
        </Box>
    )
}