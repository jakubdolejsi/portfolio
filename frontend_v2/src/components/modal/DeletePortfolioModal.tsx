import {
    Box,
    Button,
    Container,
    MenuItem,
    Modal,
    Paper,
    Select,
    SelectChangeEvent,
    Stack,
    TextField
} from "@mui/material";
import React from "react";
import Typography from "@mui/material/Typography";
import CloseIcon from "@mui/icons-material/Close";
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import {ITexBoxValue} from "./NewTradeModal";
import {createTheme} from "@mui/material/styles";
import {ThemeProvider} from "@mui/styles";
import {IPortfolioModel} from "../../pages/auth/PortfolioPage";
import {useDispatch, useSelector} from "react-redux";
import {usePortfolio} from "../../api/query/PortfolioQuery";
import {DeletePortfolio} from "../../api/Portfolio";
import {selectCurrentPortfolio, setCurrentPortfolio} from "../../store/slice/CurrentPortfolioSlice";


const modalTheme = createTheme({
    palette: {
        background: {
            paper: '#232324'
        },
        mode: 'dark'
    }
})

export interface NewPortfolioProps {
    handleClose: () => void
    open: boolean
}

const modalBox = {
    position: 'absolute' as 'absolute',
    top: '55%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    height: 300,
    background: '#1a1a1a',
    p: 4
}
const buttons = {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'end',
    marginTop: '10px',
    gap: '10px'
}

const buttonStyle = {
    width: '100px',
    height: '32px',
}
export type Currency = 'USD' | 'CZK' | 'EUR'


const NewPortfolioModal: React.FC<NewPortfolioProps> = (props) => {
    const {handleClose, open} = props
    const [error, setError] = React.useState(false)
    const [selectedPortfolio, selectPortfolio] = React.useState('Default')
    const nameRef = React.useRef<ITexBoxValue>(null)
    const okHelperText = "Match"
    const errorHelperText = "Names don't match!"
    const [helperText, setHelperText] = React.useState(errorHelperText)
    const portfolioState = usePortfolio()
    const {data: allPortfolio} = portfolioState.useAllPortfolio()
    const {mutate: deletePortfolio} = portfolioState.useDeletePortfolio()
    const portfolios = allPortfolio ?? [] as IPortfolioModel[]
    const dispatch = useDispatch();

    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);


    const handleSubmit = () => {
        if (nameRef.current && !error) {
            const portfolioToDelete: DeletePortfolio = {
                deletedPortfolioName: currentPortfolioStore.name,
                newPortfolioName: selectedPortfolio,
            }
            deletePortfolio(portfolioToDelete)
            const p = portfolios.find(p => p.name === 'Default')
            if (p) {
                console.log('to switch: ', p)
                dispatch(setCurrentPortfolio(p))
            }
            handleClose()
        }
    }

    const OnModalClose = (event: object, reason: string) => {
        if (reason && reason === "backdropClick")
            return;
        handleClose()
    }

    const validateName = () => {
        if (currentPortfolioStore.name === nameRef.current?.value.trim()) {
            setError(false)
            setHelperText(okHelperText)
        } else {
            setError(true)
            setHelperText(errorHelperText)
        }
    }

    const AvailablePortfolioSelect = () => {
        const items = portfolios
            .filter(p => p.name !== currentPortfolioStore.name)
            .map(p => {
                return (<MenuItem value={p.name}>{p.name}</MenuItem>)
            })

        return (
            <Select
                sx={{
                    width: '200px',
                    height: '55px'
                }}
                value={selectedPortfolio}
                onChange={(event: SelectChangeEvent) => selectPortfolio(event.target.value as string)}
                id="outlined-basic"
                label="Portfolio"
                variant="outlined"
            >
                {items}

            </Select>
        )
    }

    return (
        <Paper>
            <Modal
                open={open}
                onClose={OnModalClose}
            >
                <Paper elevation={10} square={false} sx={modalBox}>
                    <Typography color={"white"} id="modal-modal-title" variant="h6" component="h2" sx={{
                        display: 'flex',
                        justifyContent: 'center'
                    }}>
                        Remove portfolio
                    </Typography>
                    <Typography color={"white"} id="modal-modal-description" sx={{mt: 2}}>
                        Type name of the portfolio: {currentPortfolioStore.name}
                    </Typography>
                    <ThemeProvider theme={modalTheme}>
                        <Box sx={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            gap: '5px',
                            flexDirection: 'row',
                            width: '500px',
                            // alignItems: 'center'
                        }}>
                            <TextField
                                label={"Portfolio Name"}
                                onChange={validateName}
                                inputRef={nameRef}
                                error={error}
                                helperText={helperText}

                            />
                            <AvailablePortfolioSelect/>

                        </Box>
                    </ThemeProvider>
                    <Container sx={buttons}>
                        <Container sx={{
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'start',
                            marginLeft: '50px',
                        }}>
                        </Container>
                        <Box>
                            <Stack direction={"row"} spacing={"2"}>
                                <Button sx={buttonStyle} color={"primary"} variant={"outlined"}
                                        startIcon={<CloseIcon/>}
                                        onClick={handleClose}>Cancel</Button>
                            </Stack>
                        </Box>
                        <Box>
                            <Stack direction={"row"} spacing={"2"}>
                                <Button sx={buttonStyle} color={"error"} variant={"contained"}
                                        startIcon={<DeleteForeverIcon/>}
                                        onClick={handleSubmit}>Delete</Button>
                            </Stack>
                        </Box>
                    </Container>
                </Paper>
            </Modal>
        </Paper>
    )
}

export default NewPortfolioModal