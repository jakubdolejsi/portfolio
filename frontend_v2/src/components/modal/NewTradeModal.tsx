import Typography from "@mui/material/Typography";
import React from "react";

import {
    Autocomplete,
    Box,
    Button, Checkbox,
    Collapse,
    Container,
    Divider, FormControlLabel,
    MenuItem,
    Modal,
    Paper,
    Select,
    SelectChangeEvent,
    Stack,
    TextField,
    ThemeProvider,
    FormControl
} from "@mui/material";
import axios from "axios";
import {DatePicker} from "@mui/lab";
import AddOutlinedIcon from '@mui/icons-material/AddOutlined';
import CloseIcon from '@mui/icons-material/Close';
import SettingsIcon from '@mui/icons-material/Settings';
import {GetDate} from "../../utils/DateService";
import {createTheme} from "@mui/material/styles";
import config from "../../utils/config";
import {useTrade} from "../../api/query/TradeQuery";
import {usePortfolio} from "../../api/query/PortfolioQuery";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../store/slice/CurrentPortfolioSlice";
import Alert from "../form/components/notification/Alert";
import {round} from "../../utils/Compute";

// #01fea3 -- svetle zelena barva
// #F75C03 -- oranzova
const modalMox = {
    position: 'absolute' as 'absolute',
    top: '55%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    height: 600,
    // background: '#070707',
    background: '#1a1a1a',

    // background: '#cbcbcb',
    border: '1px solid #2c2c2c',
    // borderRadius: '5px',
    // boxShadow: 24,
    p: 4
}
const buttonStyle = {
    width: '100px',
    height: '32px',
}
const advancedButtonStyle = {
    width: '125px',
    height: '32px',
    textTransform: 'none'
}

const buttons = {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'end',
    marginTop: '10px',
    gap: '10px'
}

const inputBox = {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    color: 'white',
    width: '300px'
}


const inputField = {
    // color: 'white',
    // borderColor: 'white',
    input: {
        // color: '#F75C03'
        color: '#fff',
    },
    '& p': {
        color: '#fff',
    },
    // variant: 'outlined'
}

const autoCompleteTheme = createTheme({
    palette: {
        background: {
            paper: '#232324'
        },
        mode: 'dark'
    }
})

export interface INewTradeModal {
    handleClose: () => void
    open: boolean
}


export interface ITexBoxValue {
    value: string
}


export interface INewTrade {
    ticker: string,
    amount: string | number,
    overallPrice: number,
    pricePerAsset: number,
    portfolio: string,
    date?: string,
    occurring?: number
}


const showCurrencySymbol = (currency: string) => {

}

const NewTradeModal: React.FC<INewTradeModal> = (props) => {
    const {handleClose, open} = props
    const [tickerArray, setTicker] = React.useState<string[]>([])
    const [totalPrice, setTotalPrice] = React.useState(0)
    const [price, setPrice] = React.useState(0)
    const [currency, setCurrency] = React.useState('')
    const [advanced, SetAdvanced] = React.useState(false)
    const [tradeDate, setTradeDate] = React.useState<Date | null>(null);
    const [occurring, setOccurring] = React.useState<boolean>(false)
    const [occurringTime, setOccurringTime] = React.useState<number>(0)


    const currentPortfolio = useSelector(selectCurrentPortfolio)

    const [selectedPortfolio, selectPortfolio] = React.useState<string>(currentPortfolio.name)
    const tradeState = useTrade()
    const portfolioState = usePortfolio()

    const tickerValueRef = React.useRef<ITexBoxValue>(null)
    const amountValueRef = React.useRef<ITexBoxValue>(null)

    const {mutate: addTrade} = tradeState.useAddTrade()
    const {data: allPortfolio} = portfolioState.useAllPortfolio()
    const portfolios = allPortfolio ?? []

    const [tradeSuccessOpen, setTradeSuccessOpen] = React.useState<boolean>(false)

    const handleCloseAlert = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setTradeSuccessOpen(false);
    };

    const openSuccessTradeAlert = () => {
        setTradeSuccessOpen(!tradeSuccessOpen)
    }

    const handleOccurringChange = (event: SelectChangeEvent) => {
        // @ts-ignore
        setOccurringTime(event.target.value as number);
    };
    const OnAdvancedClick = () => {
        SetAdvanced(!advanced)
    }
    const fetchTickerOnKeyPress = () => {

        if (tickerValueRef.current === null) return
        axios.get(`${config.address}/api/v2/asset/search/${tickerValueRef.current.value}`)
            .then(response => {
                // @ts-ignore
                let arr: string[] = []
                response.data.body.quotes
                    .filter((item: any) => item.quoteType !== "FUTURE")
                    .filter((item: any) => item.index === 'quotes')
                    .map((item: any) => arr.push(item.symbol))
                if (arr.length === 0) return

                setTicker(arr)
                console.log('quotes: ', tickerArray)
            })
            .catch(err => {
                console.log('err', err)
            })
    }

    const fetchAssetPrice = () => {
        if (amountValueRef.current === null || tickerValueRef.current === null) return

        // const localStorageAsset = localStorage.getItem(tickerValueRef.current.value)
        // if (localStorageAsset) {
        //     const fPrice = parseInt(JSON.parse(localStorageAsset).type.price)
        //     const fetchedPrice = round((fPrice * parseFloat(amountValueRef.current.value)), 2)
        //     setTotalPrice(fetchedPrice)
        //     setPrice(fPrice)
        //     return;
        // }
        //
        axios.get(`${config.address}/api/v2/asset/ticker/${tickerValueRef.current.value}`)
            .then(response => {
                console.log('fetched asset data', response)
                setCurrency(response.data.body.type.currency)
                const pricePerOne = parseInt(response.data.body.type.price)
                // @ts-ignore
                const fetchedPrice = pricePerOne * parseFloat(amountValueRef.current.value)
                setPrice(pricePerOne)
                setTotalPrice(fetchedPrice)

                if (!localStorage.getItem(response.data.body.type.ticker)) {
                    // set asset object to local storage for further use
                    localStorage.setItem(response.data.body.type.ticker, JSON.stringify(response.data.body))
                }
            })
            .catch(err => {
                console.log('err', err)
            })
    }
    const fetchAssetHistory = (date: string) => {
        // @ts-ignore
        if (amountValueRef.current === null || tickerValueRef.current === null) return

        axios.get(`${config.address}/api/v2/asset/history/${tickerValueRef.current.value}/${date}`)
            .then(response => {
                const historicalPrice = parseFloat(response.data.body.open);
                console.log('fetched price, ', response.data.body.open)
                setPrice(historicalPrice)
                // @ts-ignore
                setTotalPrice(historicalPrice * parseInt(amountValueRef.current.value))

            })
            .catch(err => {
                console.log('zde bude chyba hlaska: ', err)
            })
    }
    const addNewTrade = async () => {
        const trade: INewTrade = {
            amount: amountValueRef.current!.value,
            ticker: tickerValueRef.current!.value,
            pricePerAsset: price,
            overallPrice: totalPrice,
            portfolio: selectedPortfolio,
        }
        if (occurring) {
            trade.occurring = occurringTime
        }

        console.log('trade', trade)
        if (tradeDate) {
            const date = GetDate(tradeDate)
            if (date) trade['date'] = date
        }
        addTrade(trade)
        setTradeSuccessOpen(true)
        handleClose()

    }

    const OnButtonSubmit = () => {
        addNewTrade()
    }

    const OnButtonClose = () => {
        handleClose()
        clearPrices()
    }
    const clearPrices = () => {
        setPrice(0)
        setTotalPrice(0)
    }

    const OnModalClose = (event: object, reason: string) => {
        if (reason && reason === "backdropClick")
            return;
        handleClose()
        clearPrices()
    }
    return (
        <Paper>
            <Modal
                open={open}
                onClose={OnModalClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={modalMox} boxShadow={"20"} borderRadius={"13px"}>
                    <Typography color={"white"} id="modal-modal-title" variant="h6" component="h2" sx={{
                        display: 'flex',
                        justifyContent: 'center'
                    }}>
                        Add new trade
                    </Typography>
                    <Typography color={"white"} id="modal-modal-description" sx={{mt: 2}}>
                        Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
                    </Typography>
                    <Container sx={inputBox}>
                        <Box sx={{
                            display: 'flex',
                            flexDirection: 'column',
                            gap: '10px',
                            marginTop: '20px',
                        }}>
                            <ThemeProvider theme={autoCompleteTheme}>
                                <Autocomplete
                                    disablePortal
                                    id="combo-box-demo"
                                    options={tickerArray}
                                    sx={{width: 300}}
                                    renderInput={(params) =>
                                        <TextField
                                            {...params}
                                            id="outlined-basic"
                                            label="Name/ticker"
                                            variant="outlined"
                                            // helperText={"Enter name or ticker symbol"}e
                                            sx={inputField}
                                            inputRef={tickerValueRef}
                                            onKeyUp={fetchTickerOnKeyPress}
                                        />
                                    }
                                />
                            </ThemeProvider>
                            <TextField
                                sx={inputField}
                                id="outlined-basic"
                                label="Amount"
                                variant="outlined"
                                inputRef={amountValueRef}
                                onKeyUp={fetchAssetPrice}
                                type="number"
                                // helperText={"Enter amount"}
                            />
                            <TextField
                                // focused
                                sx={inputField}
                                id="outlined-basic"
                                label="Cash input"
                                variant="outlined"
                            />

                        </Box>
                    </Container>
                    <Container sx={{
                        height: '80px',
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        gap: '10px'
                    }}>
                        <Box/>

                        <Box>
                            <Stack direction={"row"} spacing={"2"}>
                                <Button sx={advancedButtonStyle} color={"info"} variant={"outlined"}
                                        startIcon={<SettingsIcon/>}
                                        onClick={OnAdvancedClick}>Advanced</Button>
                            </Stack>
                        </Box>
                    </Container>
                    <Collapse in={advanced} timeout={'auto'} unmountOnExit>
                        <Container sx={{
                            marginBottom: '10px',
                        }}>
                            <Stack direction={"row"} spacing={"2"}>
                                <ThemeProvider theme={autoCompleteTheme}>
                                    <Box sx={{
                                        display: 'flex',
                                        justifyContent: 'space-between',
                                        gap: '5px',
                                    }}>
                                        <FormControl sx={{width: 180}}>
                                            <DatePicker
                                                label={"Set a trade day"}
                                                value={tradeDate}
                                                onChange={(newValue) => {
                                                    setTradeDate(newValue)
                                                    const date = GetDate(newValue)
                                                    if (date) {
                                                        fetchAssetHistory(date)
                                                    }
                                                }}
                                                renderInput={(params) => <TextField
                                                    {...params}
                                                />}
                                            />
                                        </FormControl>
                                        {/*<Select*/}
                                        {/*    sx={{*/}
                                        {/*        width: '200px',*/}
                                        {/*    }}*/}
                                        {/*    value={selectedPortfolio}*/}
                                        {/*    onChange={(event: SelectChangeEvent) => selectPortfolio(event.target.value as string)}*/}
                                        {/*    id="outlined-basic"*/}
                                        {/*    label="Portfolio"*/}
                                        {/*    variant="outlined"*/}
                                        {/*>*/}
                                        {/*    {portfolios.map(p => {*/}
                                        {/*        return (<MenuItem value={p.name}>{p.name}</MenuItem>)*/}
                                        {/*    })}*/}
                                        {/*</Select>*/}
                                        <FormControlLabel labelPlacement="top"
                                                          control={<Checkbox defaultChecked={false} value={occurring}
                                                                             onChange={() => setOccurring(!occurring)}/>}
                                                          label="Make occuring"
                                        />
                                        <FormControl variant={"outlined"} sx={{width: 180}}>
                                            <Select
                                                id={"occruing-select"}
                                                value={occurringTime.toString()}
                                                label={"Occurring period"}
                                                disabled={!occurring}
                                                onChange={handleOccurringChange}
                                                sx={{
                                                    width: '50x'
                                                }}
                                            >
                                                <MenuItem value={1}>Once a day</MenuItem>
                                                <MenuItem value={7}>Once a week</MenuItem>
                                                <MenuItem value={30}>Once a month</MenuItem>
                                            </Select>
                                        </FormControl>

                                    </Box>

                                </ThemeProvider>

                            </Stack>
                        </Container>
                    </Collapse>
                    <Divider/>
                    <Container sx={buttons}>
                        <Container sx={{
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'start',
                            marginLeft: '40px',
                        }}>
                            <Box>Price per one: {price} ({currency})</Box>
                            <Box>Total price: {totalPrice} ({currency})</Box>
                        </Container>
                        <Box>
                            <Stack direction={"row"} spacing={"2"}>
                                <Button sx={buttonStyle} color={"error"} variant={"outlined"}
                                        startIcon={<CloseIcon/>}
                                        onClick={OnButtonClose}>Cancel</Button>
                            </Stack>
                        </Box>
                        <Box>
                            <Stack direction={"row"} spacing={"2"}>
                                <Button sx={buttonStyle} color={"info"} variant={"contained"}
                                        startIcon={<AddOutlinedIcon/>}
                                        onClick={OnButtonSubmit}>Submit</Button>
                            </Stack>
                        </Box>
                    </Container>
                </Box>
            </Modal>
            <Alert message={"Trade successfully added"} severity={"success"} open={tradeSuccessOpen}
                   onClose={handleCloseAlert}/>
        </Paper>
    );
}

export default NewTradeModal
