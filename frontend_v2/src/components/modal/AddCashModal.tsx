import {Box, Modal, Paper} from "@mui/material";
import Button from "@mui/material/Button";
import React from "react";

export interface AddCashProps {
    open: boolean,
    handleClose: () => void
}

const AddCashModal: React.FC<AddCashProps> = (props) => {
    const {open, handleClose} = props


    return (
        <Paper>
            <Modal
                open={open}
                onClose={handleClose}
            >
                <Box>
                    <Button>Add cash</Button>
                </Box>
            </Modal>
        </Paper>
    )
}

export default AddCashModal