import {Box, Button, Container, FormHelperText, MenuItem, Modal, Paper, Select, Stack, TextField} from "@mui/material";
import React from "react";
import Typography from "@mui/material/Typography";
import CloseIcon from "@mui/icons-material/Close";
import AddOutlinedIcon from "@mui/icons-material/AddOutlined";
import {ITexBoxValue} from "./NewTradeModal";
import {usePortfolio} from "../../api/query/PortfolioQuery";
import {NewPortfolio} from "../../api/Portfolio";
import {createTheme} from "@mui/material/styles";
import {ThemeProvider} from "@mui/styles";


const modalTheme = createTheme({
    palette: {
        background: {
            paper: '#232324'
        },
        mode: 'dark'
    }
})

export interface NewPortfolioProps {
    handleClose: () => void
    open: boolean
}

const modalBox = {
    position: 'absolute' as 'absolute',
    top: '55%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    height: 300,
    background: '#1a1a1a',
    p: 4
}
const buttons = {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'end',
    marginTop: '10px',
    gap: '10px'
}

const buttonStyle = {
    width: '100px',
    height: '32px',
}
export type Currency = 'USD' | 'CZK' | 'EUR'


const NewPortfolioModal: React.FC<NewPortfolioProps> = (props) => {
    const {handleClose, open} = props
    const [error, setError] = React.useState(false)
    const nameRef = React.useRef<ITexBoxValue>(null)
    const okHelperText = "Fill portfolio name"
    const errorHelperText = "Portfolio already exists"
    const [helperText, setHelperText] = React.useState(okHelperText)

    const [currency, setCurrency] = React.useState('USD')
    const portfolioState = usePortfolio()

    const {data: allPortfolio} = portfolioState.useAllPortfolio()
    const {mutate: addPortfolio} = portfolioState.useAddPortfolio()
    const allPortfolioStore = allPortfolio ?? []


    const handleSubmit = () => {
        if (nameRef.current) {
            console.log('submiting', nameRef.current.value)
            const newPortfolio: NewPortfolio = {
                currency: currency,
                portfolioName: nameRef.current?.value.trim()
            }
            addPortfolio(newPortfolio)
            handleClose()
        }
    }

    const OnModalClose = (event: object, reason: string) => {
        if (reason && reason === "backdropClick")
            return;
        handleClose()
    }

    const validateName = () => {
        if (allPortfolioStore.find(p => p.name === nameRef.current?.value.trim())) {
            setError(true)
            setHelperText(errorHelperText)
        } else {
            setError(false)
            setHelperText(okHelperText)
        }
    }

    return (
        <Paper>
            <Modal
                open={open}
                onClose={OnModalClose}
            >
                <Paper elevation={10} square={false} sx={modalBox}>
                    <Typography color={"white"} id="modal-modal-title" variant="h6" component="h2" sx={{
                        display: 'flex',
                        justifyContent: 'center'
                    }}>
                        Add new Portfolio
                    </Typography>
                    <Typography color={"white"} id="modal-modal-description" sx={{mt: 2}}>
                        Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
                    </Typography>
                    <ThemeProvider theme={modalTheme}>
                        <Box sx={{
                            display: 'flex',
                            justifyContent: 'center',
                            gap: '5px'
                        }}>
                            <TextField
                                label={"Portfolio Name"}
                                onChange={validateName}
                                inputRef={nameRef}
                                error={error}
                                helperText={helperText}

                            />
                            <Box sx={{
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'center'
                            }}>
                                <Select
                                    labelId="currency-select-label"
                                    id="select-currency"
                                    value={currency}
                                    label="Currency"
                                    variant={"outlined"}
                                    onChange={(event) => setCurrency(event.target.value)}
                                    sx={{
                                        width: '150px',
                                        height: '55px'
                                    }}
                                >
                                    <MenuItem value={"CZK"}>CZK</MenuItem>
                                    <MenuItem value={"EUR"}>EUR</MenuItem>
                                    <MenuItem value={"USD"}>USD</MenuItem>
                                </Select>
                                <FormHelperText>Select currency</FormHelperText>
                            </Box>
                        </Box>
                    </ThemeProvider>
                    <Container sx={buttons}>
                        <Container sx={{
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'start',
                            marginLeft: '50px',
                        }}>
                        </Container>

                        <Box>
                            <Stack direction={"row"} spacing={"2"}>
                                <Button sx={buttonStyle} color={"error"} variant={"outlined"}
                                        startIcon={<CloseIcon/>}
                                        onClick={handleClose}>Cancel</Button>
                            </Stack>
                        </Box>
                        <Box>
                            <Stack direction={"row"} spacing={"2"}>
                                <Button sx={buttonStyle} color={"info"} variant={"contained"}
                                        startIcon={<AddOutlinedIcon/>}
                                        onClick={handleSubmit}>Submit</Button>
                            </Stack>
                        </Box>
                    </Container>
                </Paper>
            </Modal>
        </Paper>
    )
}

export default NewPortfolioModal