import {Box, Divider, List, ListItemButton, ListItemText, Paper, Stack} from "@mui/material";
import React from "react";
import {IPortfolioModel} from "../../pages/auth/PortfolioPage";
import {usePortfolio} from "../../api/query/PortfolioQuery";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../store/slice/CurrentPortfolioSlice";

export interface PortfolioSidebarProps {
    onChange: (portfolio: IPortfolioModel) => void
}


const PortfolioSidebar: React.FC<PortfolioSidebarProps> = (props) => {
    const {onChange} = props
    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);

    const portfolioState = usePortfolio()
    const {data: portfolios, status} = portfolioState.useAllPortfolio()

    const allPortfolio = portfolios ?? []
    if (status === "loading") {
        return (
            <Paper>
                Loading
            </Paper>
        )
    }

    if (status === "error") {
        return (
            <Paper>
                Error
            </Paper>
        )
    }
    console.log('data', allPortfolio)

    return (
        <Paper style={{
            backgroundColor: '#00161e',
            // backgroundColor: '#000000',
        }}
               elevation={8}
               variant={"elevation"}

        >
            <Box style={{
                height: '90vh',
                // borderRight: '1px solid #00171F',
            }}>
                <List sx={{
                    height: '90vh'
                }}>
                    <Stack spacing={1} direction={"column"} alignItems={"stretch"} divider={<Divider/>}>
                        {allPortfolio.map((p: any) => {
                            return (
                                <ListItemButton
                                    onClick={(e) => {
                                        onChange(p)
                                    }}
                                    sx={{
                                        borderRadius: '5px',
                                        marginX: '5px',
                                        // width: '100%',
                                        color: currentPortfolioStore.name === p.name ? '#29b6f6' : '#F75C03',
                                        fontWeight: currentPortfolioStore.name === p.name ? 'bold' : 'normal',
                                        backgroundColor: currentPortfolioStore.name === p.name ? '#3f3f3f' : ''
                                    }}
                                >
                                    <ListItemText
                                        primary={`${p.name} `}
                                        secondary={`${p.currency} (${p.assets.length})`}
                                    />
                                </ListItemButton>
                            )
                        })}
                    </Stack>
                </List>
            </Box>
        </Paper>
    )
}

export default PortfolioSidebar