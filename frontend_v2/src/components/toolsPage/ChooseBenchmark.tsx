import {Box, InputLabel, MenuItem, Select, SelectChangeEvent, Typography} from "@mui/material";
import React from "react";

export interface IBenchmark {
    benchmark: string,
    setBenchmark: (value: (((prevState: string) => string) | string)) => void
}

const ChooseBenchmark: React.FC<IBenchmark> = ({benchmark, setBenchmark}) => {

    const handleChange = (event: SelectChangeEvent) => {
        setBenchmark(event.target.value as string);
    };

    return (
        <Box sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column'
        }}>
            <Box>
            </Box>
            <Box sx={{}}>
                <InputLabel id="demo-simple-select-label">
                    <Typography variant={"h6"} color={"primary"}>Benchmark</Typography>
                </InputLabel>
                <Select
                    labelId="benchmark-select-label"
                    id="benchmark-select"
                    value={benchmark}
                    label="Benchmark"
                    onChange={handleChange}
                    sx={{
                        width: '100px',
                        height: '4vh',
                    }}
                >
                    <MenuItem value={"SPY"}>SPY</MenuItem>
                    <MenuItem disabled={true} value={"DJI"}>DJI</MenuItem>
                </Select>
            </Box>
        </Box>
    )
}

export default ChooseBenchmark
