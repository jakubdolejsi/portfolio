import React from "react";
import AllocationList from "../portfolioPage/AllocationList";
import {Allocations} from "../../api/Stats";
import {Box} from "@mui/material";
import ChooseBenchmark, {IBenchmark} from "./ChooseBenchmark";


export interface AllocationBenchmarkListProps extends IBenchmark {
    sectors: Allocations[],
    setQuotes: (prev: string[]) => void,
    title: string,
}

const AllocationListWithBenchmark: React.FC<AllocationBenchmarkListProps> =
    ({
         sectors,
         setQuotes,
         title,
         benchmark,
         setBenchmark
     }) => {

        return (
            <Box sx={{
                height: '30vh'
            }}>
                <AllocationList
                    sectors={sectors}
                    setQuotes={setQuotes}
                    title={title}
                    comparator={true}
                />
                <Box sx={{
                    height: '10vh',
                }}>
                    <ChooseBenchmark setBenchmark={setBenchmark} benchmark={benchmark}/>
                </Box>
            </Box>
        )
    }

export default AllocationListWithBenchmark
