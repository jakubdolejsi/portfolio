import {makeStyles} from "@mui/styles";
import React from "react";
import {Typography} from "@mui/material";


const useStyles = makeStyles((theme) => ({
    loadingProgress: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        gap: '5px',
    },
    loadingText: {
        color: '#d7effc',
        textAlign: 'center',
    },
    container: {
        height: '500px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    row: {
        width: 'auto'
    }
}));

interface ServerNotRespondingProps {
}

const ServerNotResponding: React.FC<ServerNotRespondingProps> = () => {
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <div className={classes.row}>
                <div className={classes.loadingProgress}>
                    <Typography variant={"h5"} color={"primary"}>There are some technical issues.</Typography>
                    <Typography variant={"h5"} color={"primary"}>We are working on it...</Typography>
                </div>
            </div>
        </div>
    )
}

export default ServerNotResponding