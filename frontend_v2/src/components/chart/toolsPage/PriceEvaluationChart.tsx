import {Box, Typography} from "@mui/material";
import React, {useEffect} from "react";
import {BenchmarkPerformanceChartProps} from "./BenchMarkComparatorChart";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";
import {GetComparableAssetsWithoutCashInflow, GetQuotesPriceCompareWithBenchmark} from "../../../api/Stats";
import {quotesToString} from "../../../utils/utils";
import {Legend, Line, LineChart, Tooltip, XAxis, YAxis} from "recharts";
import {colors} from "./ComparatorChart";


const PriceEvaluationChart: React.FC<BenchmarkPerformanceChartProps> = ({benchmark, quotes, title}) => {
    const [realData, setRealData] = React.useState<any[]>([])

    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);
    useEffect(() => {
        const load = async () => {
            const comparables = await GetQuotesPriceCompareWithBenchmark(currentPortfolioStore.name, quotes, benchmark)
            setRealData(comparables)
        }
        load()
    }, [quotes, currentPortfolioStore.name])

    return (
        <Box sx={{
            width: '700px',
            height: '220px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        }}>
            <Typography variant={"h6"} color={"primary"}>{title}: {quotesToString(quotes)}</Typography>
            <Box sx={{
                fontSize: '11px'
            }}>
                {/*<ResponsiveContainer width={"100%"} height={"100%"}>*/}
                <LineChart width={700} height={220} data={realData}>
                    <XAxis dataKey="date"/>
                    <YAxis/>
                    <Tooltip/>
                    <Legend/>
                    {
                        realData[0] &&
                        Object.keys(realData[0])
                            .filter(k => k !== 'date')
                            .filter(k => k !== 'zero')
                            .map((k, i) => {
                                console.log('dateKey', k)
                                return (
                                    <Line
                                        type="monotone"
                                        dataKey={k}
                                        stroke={k === benchmark ? '#ff0734': colors[i]}
                                        strokeWidth={3}
                                        dot={false}
                                        connectNulls
                                    />
                                )
                            })
                    }
                    <Line
                        type="monotone"
                        dataKey={'zero'}
                        stroke={'#fff'}
                        strokeWidth={1}
                        dot={false}
                        connectNulls
                        strokeDasharray="3 3"
                    />
                </LineChart>
                {/*</ResponsiveContainer>*/}
            </Box>

        </Box>
    )
}


export default PriceEvaluationChart
