import React from 'react'
import {HeatMapGrid} from 'react-grid-heatmap'
import {Box, Typography} from "@mui/material";
import {CorrelationItem, GetQuotesCorrelation, ICorrelation} from "../../../api/Stats";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";


interface CorrelationHeathMapProps {
    quotes: string[],
    correlation: ICorrelation
}

const dynamicSize = (quotes: string[]) => {
    const size = 15
    console.log('size: ', size)
    return `${size}px`
}

const dynamicCell = (quotes: string[]) => {
    let coeff = 9
    if (quotes.length > 5) {
        coeff = 12
    }
    const size = 100 - (quotes.length * coeff)
    return `${size}px`
}

const CorrelationHeathMap: React.FC<CorrelationHeathMapProps> = ({quotes, correlation}) => {
    // const [correlation, setCorrelation] = React.useState<ICorrelation>({
    //     labels: [],
    //     data: [],
    //     history: []
    // })
    // const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);
    // React.useEffect(() => {
    //     const load = async () => {
    //         const fetchedCorrelations = await GetQuotesCorrelation(currentPortfolioStore.name, quotes)
    //         setCorrelation(fetchedCorrelations)
    //     }
    //     load()
    // }, [quotes])

    return (
        <Box sx={{
            width: '700px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        }}>
            <Typography
                variant={"h5"}
                color={"primary"}
                sx={{
                    paddingLeft: '10px'
                }}
            >Correlation matrix</Typography>

            <Box sx={{
                paddingTop: '10px',
                paddingRight: '30px',
            }}>
                <HeatMapGrid
                    data={correlation.data}
                    xLabels={correlation.labels}
                    yLabels={correlation.labels}
                    // Reder cell with tooltip

                    xLabelsStyle={(index) => ({
                        fontSize: dynamicSize(quotes),
                        color: '#29b6f6',
                        fontWeight: 'bold',
                    })}
                    yLabelsStyle={() => ({
                        fontSize: dynamicSize(quotes),
                        color: '#29b6f6',
                        fontWeight: 'bold',
                    })}
                    cellStyle={(x, y, ratio) => ({
                        background: `rgb(15, 185, 219, ${ratio})`,
                        // background: rationBG(x, y),

                    })}
                    cellHeight={dynamicCell(quotes)}
                    xLabelsPos='bottom'
                    yLabelsPos='left'
                    square
                    cellRender={(x, y, value) => (
                        <div style={{
                            color: value <= 0.1 ? 'rgba(255,255,255,0.73)' : 'black',
                            fontSize: 14,
                            fontWeight: 'normal',
                        }}>{value}</div>
                    )}
                />
            </Box>
        </Box>
    )
}


export default CorrelationHeathMap
