import React, {useEffect} from "react";
import {GetQuotesCompareWithBenchmark} from "../../../api/Stats";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";
import {Box, Typography} from "@mui/material";
import {quotesToString} from "../../../utils/utils";
import {Legend, Line, LineChart, Tooltip, XAxis, YAxis} from "recharts";

export interface BenchmarkPerformanceChartProps {
    quotes: string[],
    title: string,
    benchmark: string
}

export const benchmarkColors = [
    '#00EAFF',
    '#ff0734',
]

const BenchMarkComparatorChart: React.FC<BenchmarkPerformanceChartProps> = ({quotes, title, benchmark}) => {

    const [realData, setRealData] = React.useState<any[]>([])

    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);
    useEffect(() => {
        const load = async () => {
            const performance = await GetQuotesCompareWithBenchmark(currentPortfolioStore.name, quotes, benchmark)
            setRealData(performance)
        }
        load()
    }, [quotes, currentPortfolioStore.name])

    // @ts-ignore
    const CustomTooltip = (props) => {
        if (props.payload === null) return (<></>)
        if (props.active)
            return (
                <div style={{
                    padding: '6px',
                    backgroundColor: '#060c0c',
                    border: '1px solid #c3c3c3',
                    borderRadius: '4px',
                    color: '#F75C03',
                    fontSize: '12px',
                    display: 'flex',
                    flexDirection: 'column'

                }}>
                    <span
                        className="value-home-tooltip">Value: {props.payload[0].value.toLocaleString()} {currentPortfolioStore.currency}
                    </span>
                </div>
            );
        return <></>;
    };

    return (
        <Box sx={{
            width: '700px',
            height: '220px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        }}>
            <Typography variant={"h6"} color={"primary"}>{title}: {quotesToString(quotes)}</Typography>
            <Box sx={{
                fontSize: '11px'
            }}>
                {/*<ResponsiveContainer width={"100%"} height={"100%"}>*/}
                <LineChart width={700} height={220} data={realData}>
                    <XAxis dataKey="date"/>
                    <YAxis/>
                    <Tooltip cursor={{
                        fill: '#1f1f1f'
                    }} content={<CustomTooltip money={true}/>} animationDuration={0}/>
                    <Legend/>
                    {
                        realData[0] &&
                        Object.keys(realData[0])
                            .filter(k => k !== 'date')
                            .filter(k => k !== 'zero')
                            .map((k, i) => {
                                console.log('dateKey', k)
                                return (
                                    <Line
                                        type="monotone"
                                        dataKey={k}
                                        stroke={benchmarkColors[i]}
                                        strokeWidth={3}
                                        dot={false}
                                        connectNulls
                                    />
                                )
                            })
                    }
                    {
                        <Line
                            type="monotone"
                            dataKey={'zero'}
                            stroke={'#fff'}
                            strokeWidth={1}
                            dot={false}
                            connectNulls
                            strokeDasharray="3 3"
                        />
                    }
                </LineChart>
                {/*</ResponsiveContainer>*/}
            </Box>

        </Box>
    )
}

export default BenchMarkComparatorChart
