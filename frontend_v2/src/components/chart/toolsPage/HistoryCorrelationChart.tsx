import React from "react";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";
import {Box, Typography} from "@mui/material";
import {quotesToString} from "../../../utils/utils";
import {
    LineChart,
    Brush,
    CartesianGrid,
    ComposedChart,
    Legend,
    Line,
    ResponsiveContainer,
    Tooltip,
    XAxis,
    YAxis,
} from 'recharts';
import {CorrelationItem, ICorrelation} from "../../../api/Stats";
import {colors} from "./ComparatorChart";

interface HistoryCorrelationChartProps {
    quotes: string[],
    title: string,
    correlation: ICorrelation
}


const HistoryCorrelationChart: React.FC<HistoryCorrelationChartProps> = ({quotes, title, correlation}) => {

    return (
        <Box sx={{
            width: '700px',
            height: '220px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        }}>
            <Typography variant={"h6"} color={"primary"}>{title}: {quotesToString(quotes)}</Typography>
            <Box sx={{
                fontSize: '11px',
                paddingTop: '50px'
            }}>
                <LineChart width={700} height={300} data={correlation.history}>
                    <XAxis dataKey="date"/>
                    <YAxis/>
                    <Tooltip/>
                    <Legend/>
                    {
                        correlation.history[0] &&
                        Object.keys(correlation.history[0])
                            .filter(k => k !== 'date')
                            .filter(k => k !== 'zero')
                            .map((k, i) => {
                                return (
                                    <Line
                                        type="monotone"
                                        dataKey={k}
                                        stroke={colors[i]}
                                        strokeWidth={3}
                                        dot={false}
                                        connectNulls
                                    />
                                )
                            })
                    }
                    <Line
                        type="monotone"
                        dataKey={'zero'}
                        stroke={'#fff'}
                        strokeWidth={1}
                        dot={false}
                        connectNulls
                        strokeDasharray="3 3"
                    />
                </LineChart>
            </Box>
        </Box>
    )

}


export default HistoryCorrelationChart
