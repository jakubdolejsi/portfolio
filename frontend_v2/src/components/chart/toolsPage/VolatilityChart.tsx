import {Box, Typography} from "@mui/material";
import {quotesToString} from "../../../utils/utils";
import {Legend, Line, LineChart, Tooltip, XAxis, YAxis} from "recharts";
import React from "react";
import {Volatility, VolatilityItem} from "../../../api/Stats";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";


interface VolatilityChartProps {
    quote: string,
    title: string,
    data: VolatilityItem[],
    color: string,
    money: boolean
}

const VolatilityChart: React.FC<VolatilityChartProps> = ({quote, data, title, color, money}) => {

    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);

    // @ts-ignore
    const CustomTooltip = (props) => {
        const {money} = props
        const currency = money ? currentPortfolioStore.currency : ''
        if (props.payload === null) return (<></>)
        if (props.active)
            return (
                <div style={{
                    padding: '6px',
                    backgroundColor: '#060c0c',
                    border: '1px solid #c3c3c3',
                    borderRadius: '4px',
                    color: '#F75C03',
                    fontSize: '12px'

                }}>
                    <span
                        className="value-home-tooltip">Value: {props.payload[0].value.toLocaleString()} {currency}</span>
                </div>
            );
        return <></>;
    };


    return(
        <Box sx={{
            width: '700px',
            height: '220px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        }}>
            <Typography variant={"h6"} color={"primary"}>{title}: {quote}</Typography>
            <Box sx={{
                fontSize: '11px',
                paddingTop: '50px'
            }}>
                <LineChart width={700} height={300} data={data} >
                    <XAxis dataKey="date"/>
                    <YAxis/>
                    <Tooltip cursor={{
                        fill: '#1f1f1f'
                    }} content={<CustomTooltip money={money}/>} animationDuration={0}
                    />
                    <Legend/>
                    {
                        data[0] &&
                        Object.keys(data[0])
                            .filter(k => k !== 'date')
                            .filter(k => k !== 'zero')
                            .map((k, i) => {
                                return (
                                    <Line
                                        type="monotone"
                                        dataKey={k}
                                        stroke={color}
                                        strokeWidth={3}
                                        dot={false}
                                        connectNulls
                                    />
                                )
                            })
                    }
                </LineChart>
            </Box>
        </Box>
    )
}


export default VolatilityChart
