import React, {useEffect} from "react";
import {GetComparableAssets, GetComparableAssetsWithoutCashInflow} from "../../../api/Stats";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";
import {Box, Typography} from "@mui/material";
import {quotesToString} from "../../../utils/utils";
import {Legend, Line, LineChart, Tooltip, XAxis, YAxis} from "recharts";
import {PerformanceChartProps} from "../portfolioPage/PerformanceChart";


export const colors = [
    '#FF0077',
    '#37FF00',
    '#ffcb00',
    '#00EAFF',
    '#FF7B00',
    '#B300FF',
    '#FF0000'
]


const ComparatorChart: React.FC<PerformanceChartProps> = ({quotes, title, cashInflow, benchmark}) => {
    const [realData, setRealData] = React.useState<any[]>([])

    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);
    useEffect(() => {
        const load = async () => {
            if (cashInflow) {
                const comparables = await GetComparableAssetsWithoutCashInflow(currentPortfolioStore.name, quotes)
                setRealData(comparables)
            } else {
                const comparables = await GetComparableAssets(currentPortfolioStore.name, quotes)
                setRealData(comparables)
            }
        }
        load()
    }, [quotes, currentPortfolioStore.name])

    const randomColor = () => {
        return `#${Math.floor(Math.random() * 16777215).toString(16)}`
    }
    return (
        <Box sx={{
            width: '700px',
            height: '220px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        }}>
            <Typography variant={"h6"} color={"primary"}>{title}: {quotesToString(quotes)}</Typography>
            <Box sx={{
                fontSize: '11px'
            }}>
                {/*<ResponsiveContainer width={"100%"} height={"100%"}>*/}
                <LineChart width={700} height={220} data={realData}>
                    <XAxis dataKey="date"/>
                    <YAxis/>
                    <Tooltip/>
                    <Legend/>
                    {
                        realData[0] &&
                        Object.keys(realData[0])
                            .filter(k => k !== 'date')
                            .filter(k => k !== 'zero')
                            .map((k, i) => {
                                console.log('dateKey', k)
                                return (
                                    <Line
                                        type="monotone"
                                        dataKey={k}
                                        stroke={colors[i]}
                                        strokeWidth={3}
                                        dot={false}
                                        connectNulls
                                    />
                                )
                            })
                    }
                    {
                        cashInflow &&
                        <Line
                            type="monotone"
                            dataKey={'zero'}
                            stroke={'#fff'}
                            strokeWidth={1}
                            dot={false}
                            connectNulls
                            strokeDasharray="3 3"
                        />
                    }
                </LineChart>
                {/*</ResponsiveContainer>*/}
            </Box>

        </Box>
    )
}

export default ComparatorChart
