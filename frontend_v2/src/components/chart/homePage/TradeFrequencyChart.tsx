import {Box, Typography} from "@mui/material";
import React from "react";
import {GroupedTrade} from "../../../api/Stats";
import {Bar, BarChart, Tooltip, XAxis, YAxis} from 'recharts';


export interface TradeFrequencyChartProps {
    title: string,
    data: GroupedTrade[]
}


// @ts-ignore
const CustomTooltip = (props) => {
    console.log('props', props)
    const {money} = props
    const renderMoney = money ? '$' : 'shares'
    if (props.payload === null) return (<></>)
    if (props.active)
        return (
            <div style={{
                padding: '6px',
                backgroundColor: '#060c0c',
                border: '1px solid #c3c3c3',
                borderRadius: '4px',
                color: '#F75C03',
                fontSize: '12px'

            }}>
                <span className="value-home-tooltip">Value: {props.payload[0].value.toLocaleString()} {renderMoney}</span>
            </div>
        );
    return <></>;
};

const TradeFrequencyChart: React.FC<TradeFrequencyChartProps> = ({title, data}) => {


    return (
        <Box sx={{
            width: '700px',
            height: '220px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        }}>
            <Typography variant={"h5"} color={"primary"}>{title}</Typography>
            <Box sx={{
                fontSize: '11px'
            }}>
                <Typography sx={{
                    fontSize: 19
                }} color={"#fff"}>Traded cash per month</Typography>
                <BarChart
                    width={700}
                    height={220}
                    data={data}
                    syncId="barId"
                    margin={{
                        top: 5,
                        right: 30,
                        left: 20,
                        bottom: 5,
                    }}
                >
                    <XAxis dataKey="month"/>
                    <YAxis/>
                    <Tooltip cursor={{
                        fill: '#1f1f1f'
                    }} content={<CustomTooltip money={true}/>} animationDuration={0}/>

                    <Bar type="monotone" dataKey="price" stroke="#000" strokeWidth={2} fill="#F24236"/>
                </BarChart>
                <Typography sx={{
                    fontSize: 19
                }}  color={"#fff"}>Traded shares per month</Typography>
                <BarChart
                    width={700}
                    height={220}
                    data={data}
                    syncId="barId"
                    margin={{
                        top: 5,
                        right: 30,
                        left: 20,
                        bottom: 5,
                    }}
                >
                    <XAxis dataKey="month"/>
                    <YAxis/>
                    <Tooltip cursor={{
                        fill: '#1f1f1f'
                    }} content={<CustomTooltip money={false}/>} animationDuration={0}/>
                    <Bar type="monotone" dataKey="amount" stroke="#000" strokeWidth={2} fill="#00EAFF"/>
                </BarChart>
            </Box>
        </Box>
    )
}


export default TradeFrequencyChart
