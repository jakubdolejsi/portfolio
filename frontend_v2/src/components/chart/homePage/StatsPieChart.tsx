import {Box, Typography} from "@mui/material";
import {GeneralStatistics, StatsItem} from "../../../api/Stats";
import React from "react";
import {Pie, PieChart, ResponsiveContainer} from 'recharts';
import renderActiveShape from "./RenderActiveShape";



interface StatsPieChartProps {
    header: string,
    stats: StatsItem[]
}

const StatsPieChart: React.FC<StatsPieChartProps> = ({stats, header}) => {
    const [activeIndex, setActiveIndex] = React.useState<number>(0)


    const onPieEnter = (_: any, index: number) => {
        setActiveIndex(index)
    }

    const test = [
        {
            name: 'A',
            value: 10
        },
        {
            name: 'B',
            value: 3
        },
        {
            name: 'C',
            value: 16
        }


    ]
    return (
        <Box sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            height: '50vh',
            width: '40vw',
        }}>
            <Typography color={"primary"} variant={"h5"}>
                {header}
            </Typography>
            <ResponsiveContainer width="100%" height="100%">

                <PieChart width={1000} height={1000}>
                    <Pie
                        data={stats}
                        activeIndex={activeIndex}
                        activeShape={renderActiveShape}
                        cx="50%"
                        cy="50%"
                        innerRadius={60}
                        outerRadius={80}
                        fill="#F75C03"
                        dataKey="value"
                        onMouseEnter={onPieEnter}
                    />
                </PieChart>
            </ResponsiveContainer>
        </Box>
    )
}

export default StatsPieChart