import {Box, Typography} from "@mui/material";
import {Allocations} from "../../../api/Stats";
import React from "react";
import {Pie, PieChart, ResponsiveContainer} from "recharts";
import RenderActiveAllocationShape from "./RenderActiveAllocationShape";


interface AllocationPieChartProps {
    sectors: Allocations[],
    title: string
}

const AllocationPieChart: React.FC<AllocationPieChartProps> = ({sectors, title}) => {
    const [activeIndex, setActiveIndex] = React.useState<number>(0)

    const onPieEnter = (_: any, index: number) => {
        setActiveIndex(index)
    }

    return (
        <Box sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
            width: '40vw',
        }}>
            <Typography variant={"h5"} color={"primary"}>{title}</Typography>
            <ResponsiveContainer width="100%" height="100%">
                <PieChart width={300} height={250}>
                    <Pie
                        data={sectors}
                        activeIndex={activeIndex}
                        activeShape={RenderActiveAllocationShape}
                        cx="50%"
                        cy="50%"
                        innerRadius={60}
                        outerRadius={80}
                        fill="#F75C03"
                        dataKey="sum"
                        onMouseEnter={onPieEnter}
                    />
                </PieChart>
            </ResponsiveContainer>
        </Box>
    )
}

export default AllocationPieChart