import {GetQuotesPerformance, GetQuotesPerformanceWithoutInflow, Performance} from "../../../api/Stats";
import React, {useEffect} from "react";
import {Box, Typography} from "@mui/material";
import {Legend, Line, LineChart, Tooltip, XAxis, YAxis} from 'recharts';
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";
import {quotesToString} from '../../../utils/utils'

export interface PerformanceChartProps {
    quotes: string[],
    title: string,
    cashInflow: boolean,
    benchmark: boolean
}


const PerformanceChart: React.FC<PerformanceChartProps> = ({quotes, title, cashInflow, benchmark}) => {
    const [data, setData] = React.useState<Performance>({} as Performance)
    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);
    useEffect(() => {
        const load = async () => {
            if (cashInflow) {
                const performance = await GetQuotesPerformanceWithoutInflow(currentPortfolioStore.name, quotes)
                setData(performance)
            } else {
                const performance = await GetQuotesPerformance(currentPortfolioStore.name, quotes)
                setData(performance)
            }
        }
        load()
    }, [quotes, currentPortfolioStore.name])

    // @ts-ignore
    const CustomTooltip = (props) => {
        if (props.payload === null) return (<></>)
        if (props.active)
            return (
                <div style={{
                    padding: '6px',
                    backgroundColor: '#060c0c',
                    border: '1px solid #c3c3c3',
                    borderRadius: '4px',
                    color: '#F75C03',
                    fontSize: '12px'

                }}>
                    <span
                        className="value-home-tooltip">Value: {props.payload[0].value.toLocaleString()} {currentPortfolioStore.currency}</span>
                </div>
            );
        return <></>;
    };

    return (
        <Box sx={{
            width: '700px',
            height: '220px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        }}>
            <Typography variant={"h6"} color={"primary"}>{title}: {quotesToString(quotes)}</Typography>
            <Box sx={{
                fontSize: '11px'
            }}>
                {/*<ResponsiveContainer width={"100%"} height={"100%"}>*/}
                <LineChart width={700} height={220} data={data.performance}>
                    <XAxis dataKey="date"/>
                    <YAxis/>
                    <Tooltip cursor={{
                        fill: '#1f1f1f'
                    }} content={<CustomTooltip money={true}/>} animationDuration={0}/>
                    <Legend/>
                    <Line type="monotone"
                          dataKey="value"
                          stroke="#00bbff"
                          strokeWidth={3}
                          dot={false}
                    />
                </LineChart>
                {/*</ResponsiveContainer>*/}
            </Box>

        </Box>
    )
}

export default PerformanceChart
