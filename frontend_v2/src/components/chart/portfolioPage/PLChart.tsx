import {Box, Typography} from "@mui/material";
import React, {useEffect} from "react";
import {GetQuotesPerformanceWithoutInflow, Performance} from "../../../api/Stats";
import {IPortfolioModel} from "../../../pages/auth/PortfolioPage";
import {useSelector} from "react-redux";
import {selectCurrentPortfolio} from "../../../store/slice/CurrentPortfolioSlice";
import {Area, AreaChart, Tooltip, XAxis, YAxis} from 'recharts';
import {quotesToString} from '../../../utils/utils'

interface PLChartProps {
    quotes: string[],
    title: string,
    setPL: React.Dispatch<React.SetStateAction<number>>
}


const PLChart: React.FC<PLChartProps> = ({quotes, title, setPL}) => {
    const [data, setData] = React.useState<Performance>({} as Performance)
    const [off, setOff] = React.useState<number>(0)
    const currentPortfolioStore: IPortfolioModel = useSelector(selectCurrentPortfolio);

    useEffect(() => {
        const load = async () => {
            const performance = await GetQuotesPerformanceWithoutInflow(currentPortfolioStore.name, quotes)
            if (performance.performance.length !== 0) {
                setPL(performance.performance[performance.performance.length - 1].value)
            }
            setData(performance)
            setOff(gradientOffset(performance));
        }
        load()
    }, [quotes, currentPortfolioStore.name])

    const gradientOffset = (performance: Performance) => {
        const dataMax = Math.max(...performance.performance.map(p => p.value))
        const dataMin = Math.min(...performance.performance.map(p => p.value))

        if (dataMax <= 0) return 0
        if (dataMin >= 0) return 1

        return dataMax / (dataMax - dataMin)
    }

    // @ts-ignore
    const CustomTooltip = (props) => {
        if (props.payload === null) return (<></>)
        if (props.active)
            return (
                <div style={{
                    padding: '6px',
                    backgroundColor: '#060c0c',
                    border: '1px solid #c3c3c3',
                    borderRadius: '4px',
                    color: '#F75C03',
                    fontSize: '12px'

                }}>
                    <span
                        className="value-home-tooltip">Value: {props.payload[0].value.toLocaleString()} {currentPortfolioStore.currency}</span>
                </div>
            );
        return <></>;
    };


    return (
        <Box sx={{
            width: '700px',
            height: '220px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        }}>
            <Typography variant={"h6"} color={"primary"}>{title}: {quotesToString(quotes)}</Typography>
            <Box sx={{
                fontSize: '11px'
            }}>

                <AreaChart
                    width={700}
                    height={220}
                    data={data.performance}
                    margin={{
                        top: 10,
                        right: 30,
                        left: 0,
                        bottom: 0,
                    }}
                >
                    {/*<CartesianGrid strokeDasharray="3 3"/>*/}
                    <XAxis dataKey="date"/>
                    <YAxis/>
                    <Tooltip cursor={{
                        fill: '#1f1f1f'
                    }} content={<CustomTooltip money={true}/>} animationDuration={0}/>
                    <defs>
                        <linearGradient id="splitColor" x1="0" y1="0" x2="0" y2="1">
                            <stop offset={off} stopColor="green" stopOpacity={1}/>
                            <stop offset={off} stopColor="red" stopOpacity={1}/>
                        </linearGradient>
                    </defs>
                    <Area type="monotone" dataKey="value" stroke="#000" strokeWidth={3} fill="url(#splitColor)"/>
                </AreaChart>
            </Box>
        </Box>
    )
}

export default PLChart
