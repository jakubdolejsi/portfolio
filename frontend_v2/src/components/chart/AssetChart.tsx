import React, {useEffect} from "react";
import {Box, Checkbox, FormControlLabel, ToggleButton, ToggleButtonGroup, Typography} from "@mui/material";
import {AssetWithChart} from "../../pages/auth/AssetPage";
import moment from 'moment';
import {
    Area,
    Brush,
    CartesianGrid,
    ComposedChart,
    Legend,
    Line,
    ResponsiveContainer,
    Tooltip,
    XAxis,
    YAxis,
} from 'recharts';
import {computeChange, ComputeResult} from "../../utils/Compute";
import Loading from "../loading/Loading";


interface AssetChartProps {
    asset: AssetWithChart,
}

const toggleStyle = {
    width: '57px'
}

const customToolip = {
    padding: '6px',
    backgroundColor: '#060c0c',
    border: '1px solid #c3c3c3',
    borderRadius: '4px',
    color: '#F75C03',
    fontSize: '12px'

}

type IBrushEvent = (start: number, end: number) => void

const AssetChart: React.FC<AssetChartProps> = ({asset}) => {
    const [average, setAverage] = React.useState<boolean>(false)
    const [buyPoint, setBuyPoints] = React.useState<boolean>(false)
    const [loadingPoints, setLoadingPoints] = React.useState<boolean>(false)
    const [timeInterval, setTimeInterval] = React.useState<number>(1)
    const [computeResults, setComputeResults] = React.useState<ComputeResult>(computeChange(
        asset.chart.history[0].open,
        asset.chart.history[asset.chart.history.length - 1].open
    ))


    useEffect(() => {
        setComputeResults(computeChange(
            asset.chart.history[0].open,
            asset.chart.history[asset.chart.history.length - 1].open
        ))
    }, [asset])

    const toPercent = (decimal: any, fixed = 0) => `${(decimal * 100).toFixed(fixed)}%`;

    const getPercent = (value: any, total: any) => {
        const ratio = total > 0 ? value / total : 0;

        return toPercent(ratio, 2);
    };


    const lineData = [
        {
            date: '06-07-2021',
            open: 10,
            average: 15,
            buy: 15,
        },
        {
            date: '07-07-2021',
            open: 20,
            // average: 15,
            // buy: 10,

        },
        {
            date: '08-07-2021',
            open: 14,
            // average: 15,
            // buy: 15,

        },
        {
            date: '09-07-2021',
            open: 17,
            average: 15,
            buy: 17,

        }
    ]

    // @ts-ignore
    const CustomizedAxisTick = ({x, y, payload}) => {
        const dateTip = moment(payload.value)
            .format("ll")
            .slice(0, 6);
        return (
            <g transform={`translate(${x},${y})`}>
                <text x={23} y={0} dy={14} fontSize="0.90em" fontFamily="bold" textAnchor="end" fill="#363636">
                    {dateTip}</text>
            </g>
        );
    }

    // @ts-ignore
    const CustomTooltip = (props) => {
        if (props.payload === null) return (<></>)
        if (props.active)
            return (
                <div style={customToolip}>
                    <p className="label-tooltip">{`Date: ${props.label}`}</p>
                    <p className="desc-tooltip">
                        <span className="value-tooltip">Value: {props.payload[0].value.toLocaleString()} $</span>
                    </p>
                </div>
            );
        return <></>;
    };

    // @ts-ignore
    const xAxisTickFormatter = (timestamp_measured) => {
        return moment(timestamp_measured)
            .format("ll")
            .slice(0, 6)
    }

    // const BuyDots = (props: any) => {
    //     const {cx, cy, payload} = props;
    //     let dot = (<></>)
    //     asset.chart.buySeries.forEach(b => {
    //         if (b.date === payload.date && buyPoint) {
    //             console.log('buy Series : ', b.date, ' Asset: ', payload.date)
    //             dot = (
    //                 <circle cx={cx} r="7" cy={cy} fill={"#00afff"} stroke={"#0dab99"}>
    //                 </circle>
    //             );
    //         }
    //     })
    //     return dot;
    // };

    if (!asset.chart.history) {
        console.log('Fail to load history chart: ', asset.chart)
        return (
            <Box>
            </Box>
        )
    }

    const chartData = () => {
        console.log('rendering chart data')
        return asset.chart.history.filter((val, i, arr) => i % timeInterval === 0)
    }
    const handleChange = (
        event: React.MouseEvent<HTMLElement>,
        newInterval: number,
    ) => {
        setTimeInterval(newInterval);
    };

    const RenderDots = () => {
        return lineData.map(d => {
            if (d.buy) {
                return (<Line dot={true} activeDot={true} stroke={"#c800ff"}
                              strokeWidth={10} dataKey={"buy"}/>)
            } else {
                return (<></>)
            }
        })
    }


    const handleBrushChange = (e: any) => {
        const start = asset.chart.history[e.startIndex].open
        const end = asset.chart.history[e.endIndex].open

        // const start = lineData[e.startIndex].open
        // const end = lineData[e.endIndex].open

        const result = computeChange(start, end)
        // if (result.trend === "DOWN") {
        //     result.relativeChangePercent *= (-1)
        // }
        setComputeResults(result)
    }
    // @ts-ignore
    return (
        <Box sx={{
            width: '900px',
            height: '30vh',
            fontSize: '11px',
        }}>
            <ResponsiveContainer width="100%" height="100%">
                <ComposedChart
                    width={700}
                    height={600}
                    // data={chartData()}
                    data={asset.chart.history}
                    // data={lineData}
                    margin={{
                        top: 10,
                        right: 30,
                        left: 20,
                        bottom: 30,
                    }}
                >
                    <defs>
                        <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                            <stop offset="30%" stopColor="#F75C03" stopOpacity={1}/>
                            <stop offset="95%" stopColor="#F75C03" stopOpacity={0}/>
                        </linearGradient>
                    </defs>
                    <CartesianGrid strokeDasharray="3 3" opacity={"0.4"}/>
                    <Tooltip content={<CustomTooltip/>} animationDuration={0}/>
                    <Legend/>
                    <XAxis dataKey="date" tick={CustomizedAxisTick}/>
                    <YAxis dataKey="open"/>
                    <Area type="monotone" dataKey="open" stroke="#fff" strokeWidth={2} fillOpacity={1} fill="url(#colorUv)"
                    />
                    <Brush
                        tickFormatter={xAxisTickFormatter}
                        dataKey="date"
                        fill={'rgb(24,28,36)'}
                        onChange={(e) => handleBrushChange(e)}
                    />
                    {average && <Line dot={false} activeDot={true} connectNulls dataKey="average" stroke={"#c800ff"}
                                      strokeWidth={2}/>}
                    {buyPoint && <Line dot={true} activeDot={true} stroke={"#00bbff"}
                                       strokeWidth={10} dataKey={"buy"}/>}

                </ComposedChart>

            </ResponsiveContainer>
            <Box sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'end',
            }}>
                <Box sx={{
                    marginRight: '40px',
                    alignSelf: 'start',
                }}>
                    <Typography>Relative change: <span style={{
                        color: computeResults.trend === 'UP' ? '#22d324' : 'red',
                        fontWeight: 'bold',
                    }}>
                        {computeResults.relativeChangePercent} %
                    </span></Typography>
                    <Typography>Nominal change: <span style={{
                        color: computeResults.trend === 'UP' ? '#22d324' : 'red',
                        fontWeight: 'bold',
                    }}>
                        {computeResults.absoluteChange} $
                    </span></Typography>
                </Box>
                <Box sx={{
                    marginRight: '40px',
                    alignSelf: 'start',
                }}>
                    <Typography>Begin: {computeResults.start}</Typography>
                    <Typography>End: {computeResults.end}</Typography>

                </Box>
                <Box sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'start',
                    alignItems: 'center',
                    height: '40px',
                    marginRight: '10px',
                    marginTop: '3   px',
                }}>
                    <Typography color={"primary"} variant={"h6"}>
                        Time precision
                    </Typography>
                    <ToggleButtonGroup
                        color="primary"
                        value={timeInterval}
                        exclusive
                        onChange={handleChange}
                        sx={{
                            display: 'flex',
                            flexDirection: 'row',
                        }}
                    >
                        <ToggleButton sx={toggleStyle} value={1}>D</ToggleButton>
                        <ToggleButton sx={toggleStyle} value={7}>W</ToggleButton>
                        <ToggleButton sx={toggleStyle} value={14}>14 D</ToggleButton>
                        <ToggleButton sx={toggleStyle} value={30}>M</ToggleButton>
                        <ToggleButton sx={toggleStyle} value={180}>6 M</ToggleButton>
                    </ToggleButtonGroup>

                </Box>
                <Box sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'start'
                }}>
                    <FormControlLabel
                        label={"Show average buy price"}
                        control={
                            <Checkbox color={"primary"} size="medium" onChange={() => {
                                setAverage(!average)
                                if (!buyPoint) return

                                setLoadingPoints(true)
                                setTimeout(() => {
                                    setLoadingPoints(false)
                                }, 1500)
                            }}/>
                        }/>
                    <Box sx={{
                        display: 'flex',
                    }}>
                        <FormControlLabel
                            label={"Show buy dates"}
                            control={
                                <Checkbox color={"primary"} size="medium" onChange={() => {
                                    setBuyPoints(!buyPoint)
                                    if (buyPoint) return

                                    setLoadingPoints(true)
                                    setTimeout(() => {
                                        setLoadingPoints(false)
                                    }, 1500)
                                }}/>
                            }/>
                        {loadingPoints && <Loading size={20}/>}
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}
export default AssetChart
