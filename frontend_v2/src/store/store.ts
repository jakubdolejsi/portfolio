import {configureStore} from '@reduxjs/toolkit'
import CurrentPortfolio from './slice/CurrentPortfolioSlice'
import AllPortfolioSlice from '../store/slice/AllPortfolioSlice'
import CurrentAssetSlice from '../store/slice/CurrentAssetSlice'


export default configureStore({
    reducer: {
        currentPortfolio: CurrentPortfolio,
        allPortfolio: AllPortfolioSlice,
        currentAsset: CurrentAssetSlice
    }
})