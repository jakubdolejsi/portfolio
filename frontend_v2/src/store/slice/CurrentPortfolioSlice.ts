import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IPortfolioModel} from "../../pages/auth/PortfolioPage";


export interface CurrentPortfolioState {
    value: IPortfolioModel
}

const initialState: IPortfolioModel = {
    name: 'Default',
    currency: 'USD',
    tradesCount: 0,
    balance: 0,
    assets: []
}


export const CurrentPortfolioSlice = createSlice({
    name: 'currentPortfolio',
    initialState: {
        value: initialState
    },
    reducers: {
        setCurrentPortfolio: (state, payload: PayloadAction<IPortfolioModel>) => {
            state.value = payload.payload
        }
    }
})

export const {setCurrentPortfolio} = CurrentPortfolioSlice.actions

export const selectCurrentPortfolio = (state: any): IPortfolioModel => state.currentPortfolio.value

export default CurrentPortfolioSlice.reducer