import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IPortfolioModel} from "../../pages/auth/PortfolioPage";

export interface AllPortfolioState {
    value: IPortfolioModel[]
}

const initialSate: IPortfolioModel[] = []

export const AllPortfolioSlice = createSlice({
    name: 'allPortfolio',
    initialState: {
        value: initialSate
    },
    reducers: {
        setAllPortfolios: (state, payload: PayloadAction<IPortfolioModel[]>) => {
            state.value = payload.payload
        }
    }
})

export const {setAllPortfolios} = AllPortfolioSlice.actions

export const selectPortfolios = (state: any): IPortfolioModel[] => state.allPortfolio.value

export default AllPortfolioSlice.reducer