import {createSlice, PayloadAction} from '@reduxjs/toolkit'

export interface AssetNameState {
    ticker: string,
    index: number
}


const initialState: AssetNameState = {
    ticker: '',
    index: 0
}

export const currentAssetNameSlice = createSlice({
    name: 'currentAsset',
    initialState: {
        value: initialState
    },
    reducers: {
        setCurrentAsset: (state, payload: PayloadAction<AssetNameState>) => {
            state.value = payload.payload
        }
    }
})

export const {setCurrentAsset} = currentAssetNameSlice.actions

export const selectAsset = (state: any): AssetNameState => state.currentAsset.value

export default currentAssetNameSlice.reducer