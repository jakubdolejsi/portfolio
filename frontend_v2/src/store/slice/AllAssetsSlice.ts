import {IAsset} from "../../pages/auth/AssetPage";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";


export interface AllAssetsState {
    value: IAsset[]
}

const initialState: IAsset[] = []

export const AllAssetsSlice = createSlice({
    name: 'allAssets',
    initialState: {
        value: initialState
    },
    reducers: {
        setAllAssets: (state, payload: PayloadAction<IAsset[]>) => {
            state.value = payload.payload
        }
    }
})


export const {setAllAssets} = AllAssetsSlice.actions

export const selectAllAssets = (state: any): IAsset[] => state.allAssets.value

export default AllAssetsSlice.reducer