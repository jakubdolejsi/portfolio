import React from "react";
import {useHistory} from "react-router-dom";
import {Auth0Provider} from "@auth0/auth0-react";

const Auth0ProviderWithHistory = (props: any) => {
    const domain = process.env.REACT_APP_AUTH0_DOMAIN || 'dev-dih4eqln.eu.auth0.com';
    const clientId = process.env.REACT_APP_AUTH0_CLIENT_ID || 'RMdSjQmeeSpnXZwvrJkebSsGeHX19t34';
    const audience = process.env.REACT_APP_AUTH0_ADUIENCE || 'https://api.portfolio.auth.com';
    const children = props.children;
    const history = useHistory();

    const onRedirectCallback = (appState: any) => {
        history.push(appState?.returnTo || window.location.pathname);
    };

    return (
        <Auth0Provider
            domain={domain}
            clientId={clientId}
            redirectUri={window.location.origin}
            onRedirectCallback={onRedirectCallback}
            audience={audience}
            scope='openid profile email'
            cacheLocation='localstorage'
            useRefreshTokens
        >
            {children}
        </Auth0Provider>
    );
};

export default Auth0ProviderWithHistory;
