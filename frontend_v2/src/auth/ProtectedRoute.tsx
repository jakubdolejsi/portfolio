import React from "react";
import {Route} from "react-router-dom";
import {withAuthenticationRequired} from "@auth0/auth0-react";
import Loading from "../components/loading/Loading";


const ProtectedRoute = (props: any) => {
    const {component, ...args} = props
    return (
        <Route
            component={withAuthenticationRequired(component, {
                onRedirecting: () => <Loading size={40}/>,
            })}
            {...args}
        />
    )
};

export default ProtectedRoute;