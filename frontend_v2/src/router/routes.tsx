import React from "react";
import ProtectedRoute from "../auth/ProtectedRoute";
import {Redirect, Route, Switch} from "react-router-dom";
import {useAuth0} from "@auth0/auth0-react";
import InvalidRoute from "../pages/auth/InvalidRoute";
import AssetPage from "../pages/auth/AssetPage";
import PortfolioPage from "../pages/auth/PortfolioPage";
import {CssBaseline, Theme} from "@mui/material";
import {makeStyles} from "@mui/styles";
import {Navbar as AuthNavbar} from "../components/navbar/auth/Navbar";
import {Navbar} from "../components/navbar/noAuth/Navbar";
import {createTheme, ThemeProvider} from '@mui/material/styles';
import HomePage from "../pages/auth/HomePage";
import TradePage from "../pages/auth/TradePage";
import WelcomePage from "../pages/noAuth/WelcomePage";
import ToolsPage from "../pages/auth/ToolsPage";
import TutorialsPage from "../pages/noAuth/TutorialsPage";
import AboutPage from "../pages/noAuth/AboutPage";
import newsPage from "../pages/noAuth/NewsPage";


const useStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: theme.spacing(2)
    }
}))

const theme = createTheme({
    palette: {
        background: {
            // default: 'linear-gradient(109.04deg, #00171F 0%, rgba(25, 25, 35, 0.92) 100%)',
            paper: 'linear-gradient(109.04deg, #00171F 0%, rgba(25, 25, 35, 0.92) 100%)',
            default: '#fff',
        },
        primary: {
            main: '#F75C03'
        },
        mode: 'dark'
    }
})

// @ts-ignore
const AuthRoutes = ({routes}) => {
    const heightStyle = {
        minHeight: 'inherit',
        // width: 'inherit'
    }
    return (
        <div>
            <ThemeProvider theme={theme}>
                <CssBaseline/>
                <AuthNavbar/>
                <Switch>
                    {routes.map((route: object, i: number) => (
                        <ProtectedRoute key={i} {...route} />
                    ))}
                </Switch>
            </ThemeProvider>

        </div>

    );
}


export const SubRoutes = (route: any) => {
    const {isAuthenticated} = useAuth0();
    if (isAuthenticated && !route.path.includes('auth')) return (
        <Route>
            <Redirect to={'/auth'}/>
        </Route>
    )

    return (
        <div>
            <ThemeProvider theme={theme}>
                {!isAuthenticated && <Navbar/>}
            </ThemeProvider>
            <Route
                path={route.path}
                render={props => (
                    <route.component {...props} routes={route.routes}/>
                )}
            />
        </div>
    );
}


export const routes = [
    {
        path: '/auth',
        component: AuthRoutes,
        routes: [
            {
                path: '/auth',
                exact: 'true',
                component: HomePage
            },
            {
                path: '/auth/trade',
                component: TradePage
            },
            {
                path: '/auth/asset',
                component: AssetPage
            },
            {
                path: '/auth/portfolio',
                component: PortfolioPage
            },
            {
                path: '/auth/tools',
                component: ToolsPage
            },
            {
                path: '/auth/404',
                component: InvalidRoute
            },
            {
                path: '*',
                component: InvalidRoute
            }
        ],
    },
    {
        path: "/",
        exact: 'true',
        component: WelcomePage
    },
    {
        path: "/about",
        component: AboutPage
    },
    {
        path: "/tutorials",
        component: TutorialsPage
    },
    {
        path: "/news",
        component: newsPage
    },
    {
        path: '*',
        component: InvalidRoute
    }

]