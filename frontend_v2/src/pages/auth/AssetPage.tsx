import {
    Avatar,
    Box,
    Container,
    Grid,
    List,
    ListItemAvatar,
    ListItemButton,
    ListItemText,
    Paper,
    Theme,
    Typography
} from "@mui/material";
import {makeStyles} from "@mui/styles";
import React, {useEffect} from "react";
import {useLocation} from "react-router-dom";
import config from "../../utils/config";
import AssetChart from "../../components/chart/AssetChart";
import {AverageBuySeries, IPortfolioItem, IPortfolioModel} from "./PortfolioPage";
import {useDispatch, useSelector} from "react-redux";
import {selectAsset, setCurrentAsset} from "../../store/slice/CurrentAssetSlice";
import {selectCurrentPortfolio, setCurrentPortfolio} from "../../store/slice/CurrentPortfolioSlice";
import PortfolioSidebar from "../../components/sidebar/PortfolioSidebar";
import {useAsset} from "../../api/query/AssetQuery";
import Loading from "../../components/loading/Loading";
import TrendingDownIcon from '@mui/icons-material/TrendingDown';
import TrendingUpIcon from '@mui/icons-material/TrendingUp';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: 2
    },
    page: {
        marginTop: '5px',
        height: '90vh',

    },
    headerBox: {
        height: '120px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    assetDetailsText: {
        marginRight: '40px',
        fontWeight: '600',
    },
    assetDetailsContainer: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    card: {
        background: 'rgb(24,28,36)'
    }

}))

export interface IAssetType {
    name: string,
    ticker: string,
    price: number,
    marketCap: number,
    pe: number,
    pb: number,
    beta: number,
    dividend: string,
    sector: string,
    country: string,
    industry: string,
    currency: string

}

export interface IHistory {
    date: string,
    open: number,
    close: number,
    volume: number,
    average: number
}

export interface IAsset {
    name: string,
    type: IAssetType,
    history: IHistory[],
}

export type Trending = 'UP' | 'DOWN'

export interface Chart {
    history: IHistory[],
    buySeries: AverageBuySeries[],
    trending: Trending
}

export interface AssetWithChart {
    name: string,
    type: IAssetType,
    chart: Chart

}

export interface TrendingIconProps {
    item: IPortfolioItem,
}

const AssetPage = () => {
    const dispatch = useDispatch();

    console.log('address', config.address)
    const currentPortfolio = useSelector(selectCurrentPortfolio)

    const styles = useStyles()
    const location = useLocation()
    const [currentAsset, settCurrentAsset] = React.useState<AssetWithChart>()
    const useAssets = useAsset()

    const {data: allAssets, status} = useAssets.useAllAssets()
    const assets = allAssets ?? []


    const assetTicker = location.pathname.split('/')[3]
    console.log('assetTicker', assetTicker)

    const handleListItemClick = (
        event: React.MouseEvent<HTMLDivElement, MouseEvent>,
        index: number,
    ) => {
        dispatch(setCurrentAsset({
            ticker: '',
            index: index
        }))
    };


    const switchPortfolio = (portfolio: IPortfolioModel) => {
        dispatch(setCurrentPortfolio(portfolio))
        settCurrentAsset(undefined)
    }

    const TrendingIcon: React.FC<TrendingIconProps> = ({item}) => {
        const a = assets.find(a => a.type.ticker === item.asset.type.ticker)
        if (a?.chart.trending === "UP") {
            return <TrendingUpIcon color={"success"}/>
        }
        return <TrendingDownIcon color={"error"}/>
    }

    const NotAssets = () => {
        return (
            <Typography variant={"h5"} sx={{
                display: 'flex',
                justifyContent: 'center',
                width: '25vw'
            }}>
                No Assets
            </Typography>
        )
    }


    const RenderAssets = () => {
        if (status === "loading") {
            return <Loading size={40}/>
        }
        if (assets.length === 0) {
            return <NotAssets/>
        }
        return (
            <Box>
                <List sx={{
                    width: '25vw',
                    // bgcolor: 'background.paper',
                    height: '68vh',
                    overflow: 'auto',
                    '&::-webkit-scrollbar': {
                        width: '0.4em'
                    },
                    '&::-webkit-scrollbar-track': {
                        boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
                        webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)'
                    },
                    '&::-webkit-scrollbar-thumb': {
                        backgroundColor: 'rgba(0,0,0,.1)',
                        outline: '1px solid slategrey'
                    }
                }}
                >
                    {currentPortfolio.assets.map((portfolioItem, index) => {
                        return (<ListItemButton
                                sx={{
                                    borderRadius: '5px',
                                    marginX: '5px',
                                    backgroundColor: currentAsset?.type.ticker === portfolioItem.asset.type.ticker ? '#3f3f3f' : '',
                                    color: currentAsset?.type.ticker === portfolioItem.asset.type.ticker ? '#29b6f6' : '#F75C03'
                                }}
                                // selected={currentAssetStore.index === index}
                                onClick={(event) => {
                                    const a = assets.find(a => a.type.ticker === portfolioItem.asset.type.ticker)
                                    if (a) {
                                        settCurrentAsset(a)
                                        dispatch(setCurrentAsset({
                                            ticker: a.type.ticker,
                                            index: index
                                        }))
                                        handleListItemClick(event, index)
                                    }
                                }}
                            >
                                <ListItemAvatar>
                                    <Avatar>
                                        <TrendingIcon item={portfolioItem}/>
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={portfolioItem.asset.name}
                                              secondary={portfolioItem.asset.type.ticker}/>
                            </ListItemButton>
                        )
                    })}
                </List>
            </Box>
        )
    }
    return (
        <Paper className={styles.page}>
            <Grid container direction={"row"} spacing={"2"}>
                <Grid item xs={1}>
                    <PortfolioSidebar onChange={switchPortfolio}/>
                </Grid>
                <Grid item xs={11}>
                    <Box className={styles.headerBox}>
                        <Typography color={"primary"} variant={"h4"}>
                            Your assets
                        </Typography>
                    </Box>
                    <Box sx={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginX: '70px',
                    }}>
                        <Paper elevation={10} className={styles.card} sx={{
                            height: '68vh'
                        }}>
                            <RenderAssets/>
                        </Paper>
                        <Box sx={{
                            display: 'flex',
                            flexDirection: 'column',
                            gap: 5
                        }}>
                            <Paper sx={{
                                width: '53vw',
                                minWidth: '500px',
                                display: 'flex',
                                justifyContent: 'center',
                            }}
                                   className={styles.card}
                                   elevation={10}
                            >
                                <Box sx={{
                                    marginTop: '10px',
                                    height: '300px',
                                    display: 'flex',
                                    justifyContent: 'center',
                                    flexDirection: 'column',
                                    alignItems: 'center'
                                }}>
                                    <Typography color={"primary"} variant={"h5"}>
                                        Asset details
                                    </Typography>
                                    {currentAsset && <Container sx={{
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignContent: 'start',
                                        flexDirection: 'column',
                                        marginLeft: '10px',
                                        marginTop: '20px',
                                    }}>
                                        <Typography color={"#fff"} variant={"body1"}
                                                    className={styles.assetDetailsContainer}>
                                            <span className={styles.assetDetailsText}>Name: </span>
                                            <span>{currentAsset?.name}</span>
                                        </Typography>
                                        <Typography color={"#fff"} variant={"body1"}
                                                    className={styles.assetDetailsContainer}>
                                            <span className={styles.assetDetailsText}>Ticker: </span>
                                            <span>{currentAsset?.type.ticker}</span>
                                        </Typography>
                                        <Typography color={"#fff"} variant={"body1"}
                                                    className={styles.assetDetailsContainer}>
                                                <span
                                                    className={styles.assetDetailsText}>Country: </span> {currentAsset?.type.country}
                                        </Typography>
                                        <Typography color={"#fff"} variant={"body1"}
                                                    className={styles.assetDetailsContainer}>
                                                <span
                                                    className={styles.assetDetailsText}>Industry: </span> {currentAsset?.type.industry}
                                        </Typography>
                                        <Typography color={"#fff"} variant={"body1"}
                                                    className={styles.assetDetailsContainer}>
                                                <span
                                                    className={styles.assetDetailsText}>Sector: </span> {currentAsset?.type.sector}
                                        </Typography>
                                        <Typography color={"#fff"} variant={"body1"}
                                                    className={styles.assetDetailsContainer}>
                                                <span
                                                    className={styles.assetDetailsText}>Market cap ($): </span> {currentAsset?.type.marketCap}
                                        </Typography>
                                        <Typography color={"#fff"} variant={"body1"}
                                                    className={styles.assetDetailsContainer}>
                                                <span
                                                    className={styles.assetDetailsText}>P/B: </span> {currentAsset?.type.pb}
                                        </Typography>
                                        <Typography color={"#fff"} variant={"body1"}
                                                    className={styles.assetDetailsContainer}>
                                                <span
                                                    className={styles.assetDetailsText}>P/E: </span> {currentAsset?.type.pe}
                                        </Typography>
                                        <Typography color={"#fff"} variant={"body1"}
                                                    className={styles.assetDetailsContainer}>
                                                <span
                                                    className={styles.assetDetailsText}>Beta: </span> {currentAsset?.type.beta}
                                        </Typography>
                                        <Typography color={"#fff"} variant={"body1"}
                                                    className={styles.assetDetailsContainer}>
                                                <span
                                                    className={styles.assetDetailsText}>Price: </span> {currentAsset?.type.price}
                                        </Typography>
                                    </Container>}
                                </Box>
                            </Paper>
                            <Paper className={styles.card} elevation={10} sx={{
                                display: 'flex',
                                justifyContent: 'center',
                            }}>
                                {currentAsset && <AssetChart asset={currentAsset}/>}
                            </Paper>
                        </Box>

                    </Box>

                </Grid>
            </Grid>
        </Paper>
    )
}

export default AssetPage
