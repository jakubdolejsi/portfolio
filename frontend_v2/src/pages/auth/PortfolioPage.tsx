import {Box, Grid, Paper, Tab, Tabs, Theme} from "@mui/material";
import {makeStyles, ThemeProvider} from "@mui/styles";
import {createTheme} from "@mui/material/styles";
import React from "react";
import {IAsset} from "./AssetPage";
import {useDispatch} from "react-redux";
import {setCurrentPortfolio} from "../../store/slice/CurrentPortfolioSlice";
import PortfolioSidebar from "../../components/sidebar/PortfolioSidebar";
import OverallTab from "../../components/tab/portfolioPage/OverallTab";
import AssetTypeTab from "../../components/tab/portfolioPage/AssetTypeTab";
import AssetAllocationTab from "../../components/tab/portfolioPage/AssetAllocationTab";
import CountryAllocationTab from "../../components/tab/portfolioPage/CountryAllocationTab";
import BaseTabComponent from "../../components/tab/portfolioPage/BaseTabComponent";
import IndustryAllocationTab from "../../components/tab/portfolioPage/IndustryAllocationTab";
import PerformanceChart from "../../components/chart/portfolioPage/PerformanceChart";
import AllocationPieChart from "../../components/chart/portfolioPage/AllocationPieChart";
import AllocationList from "../../components/portfolioPage/AllocationList";
import SectorAllocationState from "../../components/tab/portfolioPage/SectorAllocationState";
import CountryAllocationState from "../../components/tab/portfolioPage/CountryAllocationState";
import IndustryAllocationState from "../../components/tab/portfolioPage/IndustryAllocationState";
import AssetTypeAllocationState from "../../components/tab/portfolioPage/AssetTypeAllocationState";
import AssetAllocationState from "../../components/tab/portfolioPage/AssetAllocationState";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: 2
    },
    page: {
        marginTop: '5px',
        height: '90vh',
    },
    button: {
        width: '90px'
    },
    card: {
        background: 'rgb(24,28,36)'
    }

}))

const theme = createTheme({
    palette: {
        background: {
            paper: 'linear-gradient(200.04deg, #00171F 0%, rgba(25, 25, 35, 0.92) 100%)'
        },
    }
})

export interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

export type PortfolioCurrency = 'CZK' | 'USD' | 'EUR' // Supported currencies


export interface IPortfolioModel {
    assets: IPortfolioItem[],
    currency?: PortfolioCurrency,
    balance?: number,
    name: string, // the name of a portfolio - user may have multiple portfolios (crypto, dividend, speculative)
    tradesCount: number,
}

export interface AverageBuySeries {
    date: string, // date of trade
    averageAssetPrice: number, // the price of given asset in that date
    sharesBought: number, // number of shared bought that date
    amountInvested: number // amount money invested in that trade

}

export interface IPortfolioItem {
    asset: IAsset,     // asset object
    amount: string,         // amount of holding asset,
    totalCashInvested?: number, // total money actually invested in that asset
    averageBuy?: number, // average buy level
    averageBuySeries?: AverageBuySeries[] // list of all buy time series as it were continually updated
    averageCost?: number, // average cost level
    overallCost?: number, // total cost on this assets (poplatky, TER, menny kurs)
    tradesPerAsset?: number // total number of trades of certain asset
}


export interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

export const TabPanel = (props: TabPanelProps) => {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box>
                    {children}
                </Box>
            )}
        </div>
    );
}

export const a11yProps = (index: number) => {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

const PortfolioPage = () => {
    const [tab, setTab] = React.useState(0);

    const styles = useStyles()
    const dispatch = useDispatch();

    const handleTabChange = (event: React.SyntheticEvent, newValue: number) => {
        setTab(newValue);
    };

    return (
        <Paper className={styles.page}>
            <Grid container direction={"row"} spacing={"2"}>
                <Grid item xs={1}>
                    <ThemeProvider theme={theme}>
                        <PortfolioSidebar onChange={(portfolio) => {
                            dispatch(setCurrentPortfolio(portfolio))
                        }}/>
                    </ThemeProvider>
                </Grid>
                <Grid item xs={11}>
                    <Box sx={{
                        width: '100%',
                        bgcolor: 'rgba(4,7,7,0.36)',
                        display: 'flex',
                        justifyContent: 'end',
                        marginRight: '100px',
                    }}>
                        <Tabs value={tab} onChange={handleTabChange} centered sx={{
                            fontSize: '10px'
                        }}>
                            <Tab label="Overall" {...a11yProps(0)} />
                            <Tab label="Asset allocation" {...a11yProps(1)} />
                            <Tab label="Asset type" {...a11yProps(2)} />
                            <Tab label="Country allocation" {...a11yProps(3)} />
                            <Tab label="Sector allocation" {...a11yProps(4)} />
                            <Tab label="Industry allocation" {...a11yProps(5)} />
                        </Tabs>
                    </Box>
                    <TabPanel value={tab} index={0}>
                        <OverallTab/>
                    </TabPanel>
                    <TabPanel value={tab} index={1}>
                        <AssetAllocationState/>
                    </TabPanel>
                    <TabPanel value={tab} index={2}>
                        <AssetTypeAllocationState/>
                    </TabPanel>
                    <TabPanel value={tab} index={3}>
                        <CountryAllocationState/>
                    </TabPanel>
                    <TabPanel value={tab} index={4}>
                        <SectorAllocationState/>
                    </TabPanel>
                    <TabPanel value={tab} index={5}>
                        <IndustryAllocationState/>
                    </TabPanel>

                </Grid>
            </Grid>

        </Paper>
    )
}

export default PortfolioPage;
