import {Box, Button, Container, Paper, Theme, Typography} from "@mui/material";
import {makeStyles} from "@mui/styles";
import NewTradeModal from "../../components/modal/NewTradeModal";
import React, {useEffect} from "react";
import {TradesTable} from "../../components/table/TradesTable";
import {useHistory} from "react-router-dom";
import {useTrade} from "../../api/query/TradeQuery";
import Loading from "../../components/loading/Loading";
import axios from "axios";
import {useAuth0} from "@auth0/auth0-react";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: 2
    },
    page: {
        height: '90vh'
    },
    headerBox: {
        height: '120px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainTradeBox: {
        width: '500px',
    },
    tradeActionsBox: {
        height: '130px',
        display: 'flex',
        justifyContent: 'end',
        alignItems: 'end',
        // background: 'blue',
    },
    tradeTable: {
        // height: '550px',
        // border: '1px solid black'
    }
}))

export interface ITrade {
    id: string,
    name: string,
    ticker: string,
    amount: number,
    date: string,
    pricePerOne: number,
    totalPrice: number,
    type: string, // equity | crypto | fund
}

const TradePage = () => {
    const styles = useStyles()
    const [open, setOpen] = React.useState(false);
    const tradeState = useTrade()
    const {status} = tradeState.useAllTrades()
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const history = useHistory()


    const RenderTradeTable = () => {
        if (status === "loading") {
            return <Loading size={40}/>
        }
        return (
            <Container>
                <Box className={styles.tradeActionsBox}>
                    <Button
                        color={"primary"}
                        variant={"contained"}
                        sx={{
                            marginBottom: '10px'
                        }}
                        onClick={handleOpen}>
                        Add new trade
                    </Button>
                </Box>
                <Box className={styles.tradeTable}>
                    <TradesTable/>
                </Box>
            </Container>
        )
    }

    return (
        <Paper className={styles.page}>
            <Box className={styles.headerBox}>
                <Typography color={"primary"} variant={"h4"}>
                    All trades
                </Typography>
            </Box>
            <RenderTradeTable/>
            <NewTradeModal handleClose={handleClose} open={open}/>
        </Paper>
    );
}

export default TradePage
