import React, {useEffect} from "react";
import {useAuth0} from "@auth0/auth0-react";
import axios from "axios";
import {Box, Grid, Paper, Theme, Typography} from "@mui/material";
import {makeStyles} from "@mui/styles";
import config from "../../utils/config";
import {useQueryClient} from "react-query";
import {fetchAllTrades} from "../../api/Trade";
import {fetchAllPortfolioAPI} from "../../api/Portfolio";
import TypographyPair from "../../components/Typography/TypographyPair";
import {useStats} from "../../api/query/StatsQuery";
import Loading from "../../components/loading/Loading";
import {fetchAllStatistics, GeneralStatistics} from "../../api/Stats";
import {fetchAllAssetsWithChart} from "../../api/Asset";
import ServerNotResponding from "../../components/error/ServerNotResponding";
import TradeFrequencyChart from "../../components/chart/homePage/TradeFrequencyChart";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: 2
    },
    page: {
        height: '90vh'
    },
    headerBox: {
        height: '120px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        background: 'rgb(24,28,36)',
        borderRadius: '7px'
    },
    innerCard: {
        // background: 'rgb(20,23,30)',
        background: 'rgb(24,28,36)',
        border: '1px solid rgb(24,28,36)',
        borderRadius: '7px',
        display: 'flex',
        justifyContent: 'center',
    },
    cardText: {
        color: '#fff'
    },
    gridLine: {
        "&:hover, &:focus": {
            backgroundColor: 'rgb(20,23,30)'
        }
    },
}))


const HomePage = () => {
    const styles = useStyles()
    const queryClient = useQueryClient()
    const {user, isLoading, getAccessTokenSilently} = useAuth0();
    const statsState = useStats()
    const {data, status} = statsState.useStats()
    const stats = data as GeneralStatistics

    const setJwt = () => {
        getAccessTokenSilently()
            .then(token => {
                axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;

                // todo This will be replace be Auth0 'Action hook' when app (at least backend) will be deployed
                axios.post(`${config.address}/api/v2/user`, {
                    email: user?.email,
                    dateRegistered: user?.updated_at
                })
                    .then(response => {
                        console.log('user recorded')
                        console.log(response.data.body)
                    })
                    .catch(err => {
                        console.log(err)
                    })
            })
    }
    // setJwt()

    // // @ts-ignore
    // if (window.Cypress) {
    //     // eslint-disable-next-line react-hooks/rules-of-hooks
    //     useEffect(() => {
    //         const auth0_token = JSON.parse(localStorage.getItem('auth0Cypress')!)
    //         console.log('local storage', localStorage)
    //         axios.defaults.headers.common['Authorization'] = `Bearer ${auth0_token}`;
    //     }, [])
    //
    // } else {
    //     // eslint-disable-next-line react-hooks/rules-of-hooks
    //     useEffect(() => {
    //         const prefetch = async () => {
    //             const token = await getAccessTokenSilently()
    //             axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    //
    //             await queryClient.prefetchQuery('trades', fetchAllTrades)
    //             await queryClient.prefetchQuery('portfolios', fetchAllPortfolioAPI)
    //             await queryClient.prefetchQuery('assets', fetchAllAssetsWithChart)
    //             await queryClient.prefetchQuery('stats', fetchAllStatistics)
    //         }
    //         prefetch()
    //     }, [])
    //
    // }

    useEffect(() => {
        const prefetch = async () => {
            const token = await getAccessTokenSilently()
            axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;

            axios.post(`${config.address}/api/v2/user`, {
                email: user?.email,
                dateRegistered: user?.updated_at
            })
                .then(response => {
                    console.log('user recorded')
                    console.log(response.data.body)
                })
                .catch(err => {
                    console.log(err)
                })

            // @ts-ignore
            if (!window.Cypress) { // dont prefetch while under cypress tests
                await queryClient.prefetchQuery('trades', fetchAllTrades)
                await queryClient.prefetchQuery('portfolios', fetchAllPortfolioAPI)
                await queryClient.prefetchQuery('assets', fetchAllAssetsWithChart)
                await queryClient.prefetchQuery('stats', fetchAllStatistics)
            }

        }
        prefetch()
    }, [])


    if (isLoading) {
        return <div>Loading ...</div>;
    }


    const Overview = () => {
        if (status === "loading") {
            return <Loading size={40}/>
        }
        if (status === "error") {
            return <ServerNotResponding/>
        }

        return (
            <Box sx={{
                display: 'flex',
                justifyContent: 'center',
                alignContent: 'center',
                gap: '20px',
            }}>

                <Paper elevation={10} className={styles.card}>
                    <Box sx={{
                        width: '40vw',
                        height: '550px',
                        display: 'flex',
                        justifyContent: 'begin',
                        alignItems: 'center',
                        flexDirection: 'column',
                        gap: '10px'

                    }}>
                        <Typography color={"primary"} variant={"h5"}>
                            Summary info
                        </Typography>
                        <Paper className={styles.innerCard} elevation={3} sx={{
                            // border: '1px solid rgb(24,28,36)',
                            // borderRadius: '5px',
                            width: '30vw',
                            padding: '10px',
                        }}>
                            <Grid container rowSpacing={1}>
                                <TypographyPair name={"Email"} value={user?.email}/>
                                <TypographyPair name={"Name"} value={user?.name}/>
                            </Grid>
                            {/*<Typography>Name: {user?.name}</Typography>*/}
                            {/*<Typography>Last update: {user?.updated_at}</Typography>*/}
                        </Paper>
                        <Paper className={styles.innerCard} elevation={3} sx={{
                            // border: '1px solid rgb(24,28,36)',
                            // borderRadius: '10px',
                            width: '30vw',
                            padding: '10px',
                        }}>
                            <Grid container rowSpacing={1}>
                                <TypographyPair name={"Number of portfolios"}
                                                value={stats.concreteStats.stats.length}/>
                                <TypographyPair name={"Total number of trades"}
                                                value={stats.tradesCount}/>
                                <TypographyPair name={"Total number of assets"} value={stats.assetsCount}/>
                                <TypographyPair name={"The most traded assets"}
                                                value={`${stats.theMostTradedAsset.name} (${stats.theMostTradedAsset.value})`}/>
                                <TypographyPair name={"The most expensive asset"}
                                                value={`${stats.theMostExpensiveAsset.name} (${stats.theMostExpensiveAsset.value})`}/>
                                <TypographyPair name={"The least expensive asset"}
                                                value={`${stats.theLeastExpensiveAsset.name} (${stats.theLeastExpensiveAsset.value})`}/>
                                <TypographyPair name={"Asset with most shares"}
                                                value={`${stats.assetWithMostShares.name} (${stats.assetWithMostShares.value})`}/>
                            </Grid>
                        </Paper>
                    </Box>
                </Paper>
                <Paper elevation={10} className={styles.card}>
                    <Box sx={{
                        width: '40vw',
                        display: 'flex',
                        justifyContent: 'begin',
                        alignItems: 'center',
                        flexDirection: 'column',
                    }}>
                        <TradeFrequencyChart title={"Your activity"} data={stats.trades}/>
                    </Box>
                </Paper>
            </Box>
        )
    }


    return (
        <Paper className={styles.page}>
            <Box className={styles.headerBox}>
                <Typography color={"primary"} variant={"h4"} id={"overview"}>
                    Overview
                </Typography>
            </Box>
            <Overview/>
            {/*<Button onClick={() => setState(true)}>OPEN</Button>*/}
        </Paper>
    )
};

export default HomePage;
