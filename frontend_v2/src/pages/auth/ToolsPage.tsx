import {Box, Grid, Paper, Tab, Tabs, Theme, Typography} from "@mui/material";
import React from "react";
import {useDispatch} from "react-redux";
import {makeStyles, ThemeProvider} from "@mui/styles";
import PortfolioSidebar from "../../components/sidebar/PortfolioSidebar";
import {setCurrentPortfolio} from "../../store/slice/CurrentPortfolioSlice";
import {a11yProps, TabPanel} from "./PortfolioPage";
import {createTheme} from "@mui/material/styles";
import ComparatorState from "../../components/tab/toolsPage/ComparatorState";
import BenchmarkState from "../../components/tab/toolsPage/BenchmarkState";
import CorrelationHeathMap from "../../components/chart/toolsPage/CorrelationHeathMap";
import CorrelationState from "../../components/tab/toolsPage/CorrelationState";
import VolatilityState from "../../components/tab/toolsPage/VolatilityState";


const useStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: 2
    },
    page: {
        marginTop: '5px',
        height: '90vh',
    },
    button: {
        width: '90px'
    },
    card: {
        background: 'rgb(24,28,36)'
    }

}))

const theme = createTheme({
    palette: {
        background: {
            paper: 'linear-gradient(200.04deg, #00171F 0%, rgba(25, 25, 35, 0.92) 100%)'
        },
    }
})

const ToolsPage = () => {
    const [quotes, setQuotes] = React.useState<string[]>([])
    const [tab, setTab] = React.useState(0);

    const styles = useStyles()
    const dispatch = useDispatch();

    const handleTabChange = (event: React.SyntheticEvent, newValue: number) => {
        setTab(newValue);
    };

    const handleClick = (ticker: string) => {
        console.log('quotes', quotes, ' ticker: ', ticker)
        if (quotes.find(q => q === ticker)) return
        const _q = quotes
        _q.push(ticker)
        setQuotes(_q)
    }

    return (
        <Paper className={styles.page}>
            <Grid container direction={"row"} spacing={"2"}>
                <Grid item xs={1}>
                    <ThemeProvider theme={theme}>
                        <PortfolioSidebar onChange={(portfolio) => {
                            dispatch(setCurrentPortfolio(portfolio))
                        }}/>
                    </ThemeProvider>
                </Grid>
                <Grid item xs={11}>
                    <Box sx={{
                        width: '100%',
                        bgcolor: 'rgba(4,7,7,0.36)',
                        display: 'flex',
                        justifyContent: 'end',
                        marginRight: '100px',
                    }}>
                        <Tabs value={tab} onChange={handleTabChange} centered sx={{
                            fontSize: '10px'
                        }}>
                            <Tab label="Comparator" {...a11yProps(0)} />
                            <Tab label="Benchmark" {...a11yProps(1)} />
                            <Tab label="Corelation" {...a11yProps(2)} />
                            <Tab label="Volatility" {...a11yProps(3)} />
                            {/*<Tab label="CAPM" {...a11yProps(4)} />*/}
                        </Tabs>
                    </Box>
                    <TabPanel value={tab} index={0}>
                        <ComparatorState/>
                    </TabPanel>
                    <TabPanel index={1} value={tab}>
                        <BenchmarkState/>
                    </TabPanel>
                    <TabPanel index={2} value={tab}>
                        <CorrelationState/>
                    </TabPanel>
                    <TabPanel index={3} value={tab}>
                        <VolatilityState/>
                    </TabPanel>
                    {/*<TabPanel index={4} value={tab}>*/}
                    {/*    <Typography>CAPM</Typography>*/}
                    {/*</TabPanel>*/}
                </Grid>
            </Grid>
        </Paper>
    )
}

export default ToolsPage
