import LoginComponent from '../../components/LoginComponent';
import {makeStyles, ThemeProvider} from "@mui/styles";
import {Card, CardActions, CardContent, Grid, Typography} from "@mui/material";
import {createTheme} from "@mui/material/styles";


const useStyles = makeStyles({
    root: {
        maxWidth: '20em',
        height: '20em',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'column',
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        paddingTop: '23px',
        fontSize: 30,
        fontWeight: 650,
    },
    pos: {
        marginBottom: 12,
    },
});

const theme = createTheme({
    palette: {
        text: {
            primary: '#e1e1e1'
        }
    }
})

const WelcomePage = () => {
    const classes = useStyles();
    const bull = <span className={classes.bullet}>•</span>;

    const bold = (word: string) => {
        return <span style={{
            fontWeight: 600,
            color: '#c94e08'
        }}>
            {word}
        </span>
    }
    return (
        <Grid
            container
            direction={"column"}
            alignItems={"center"}
            style={{
                minHeight: '100vh',
                justifyContent: 'center'
            }}
        >
            <ThemeProvider theme={theme}>
                <Grid item xs={3}>
                    <Card className={classes.root} elevation={10} variant="elevation" sx={{
                        backgroundColor: 'rgba(4,7,7,0.36)',
                        color: theme.palette.text.primary
                    }}>
                        <CardContent>
                            <Typography className={classes.title} variant="h3" component="h3">
                                {bull} Aly<span style={{
                                    color: '#c94e08',
                                    fontWeight: 600,
                            }}>ster</span> {bull}
                            </Typography>
                        </CardContent>
                        <CardContent>
                            <Typography className={classes.pos} sx={{
                                color: theme.palette.text.primary,
                                fontSize: 18
                            }}>
                                Alyster is an interactive solution for investment portfolio analysis. It provides multiple
                                ways for analysing your portfolio including portfolio {bold('decomposition')}, visualization assets {bold('correlation')} matrix,
                                {bold('volatility')} analysis and many other features.
                            </Typography>
                            <Typography variant="body2" component="p">
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <LoginComponent>
                                Get Started
                            </LoginComponent>
                        </CardActions>
                    </Card>
                </Grid>
            </ThemeProvider>
        </Grid>

    );
}


export default WelcomePage;
