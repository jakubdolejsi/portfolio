import {useMutation, useQuery, useQueryClient} from "react-query";
import {IPortfolioModel} from "../../pages/auth/PortfolioPage";
import {addPortfolio, deletePortfolio, fetchAllPortfolioAPI} from "../Portfolio";
import {useAsset} from "./AssetQuery";
import {useStats} from "./StatsQuery";


export const usePortfolio = () => {

    const queryClient = useQueryClient()
    const assetState = useAsset()
    const statstState = useStats()

    const cacheKey = 'portfolios'
    const assetCacheKey = assetState.cacheKey
    const statsCacheKey = statstState.cacheKey

    const useAllPortfolio = () => {
        return useQuery<IPortfolioModel[], Error>(cacheKey, fetchAllPortfolioAPI, {
            refetchOnWindowFocus: false,
        })
    }

    const useDeletePortfolio = () => {
        return useMutation(deletePortfolio, {
            onSuccess: () => {
                queryClient.invalidateQueries(cacheKey)
                queryClient.invalidateQueries(assetCacheKey)
                queryClient.invalidateQueries(statsCacheKey)
            }
        })
    }

    const useAddPortfolio = () => {
        return useMutation(addPortfolio, {
            onSuccess: () => {
                queryClient.invalidateQueries(cacheKey)
                queryClient.invalidateQueries(statsCacheKey)
            }
        })
    }

    return {
        cacheKey,
        useAddPortfolio,
        useAllPortfolio,
        useDeletePortfolio
    }
}