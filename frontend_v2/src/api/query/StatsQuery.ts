import {useQuery} from "react-query";
import {fetchAllStatistics, GeneralStatistics} from "../Stats";


export const useStats = () => {

    const cacheKey = 'stats'

    const useStats = () => {
        return useQuery<GeneralStatistics, Error>(cacheKey, fetchAllStatistics)
    }

    return {
        cacheKey,
        useStats
    }
}