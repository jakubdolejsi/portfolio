import {useQuery, useQueryClient} from "react-query";
import {AssetWithChart} from "../../pages/auth/AssetPage";
import {fetchAllAssetsWithChart} from "../Asset";
import {useStats} from "./StatsQuery";


export const useAsset = () => {

    const cacheKey = 'assets'


    const useAllAssets = () => {
        return useQuery<AssetWithChart[], Error>(cacheKey, fetchAllAssetsWithChart, {
            onSuccess: (assets) => {
                console.log('successfully fetched assets witch chart')
            },
            onError: (e) => {
                console.log('ERROR', e)
            }
        })
    }


    return {
        useAllAssets,
        cacheKey
    }
}


