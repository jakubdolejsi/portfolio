import {useMutation, useQuery, useQueryClient} from "react-query";
import {ITrade} from "../../pages/auth/TradePage";
import {addTrade, deleteTrade, fetchAllTrades} from "../Trade";
import {useAsset} from "./AssetQuery";
import {usePortfolio} from "./PortfolioQuery";
import {useStats} from "./StatsQuery";


export const useTrade = () => {
    const queryClient = useQueryClient()
    const cacheKey = 'trades'
    const statsCacheKey = useStats().cacheKey
    const assetCacheKey = useAsset().cacheKey
    const portfolioCacheKey = usePortfolio().cacheKey

    const useAllTrades = () => {
        return useQuery<ITrade[], Error>(cacheKey, fetchAllTrades)
    }

    const useAddTrade = () => {
        return useMutation(addTrade, {
            onSuccess: () => {
                console.log('invalidating queries after add')
                queryClient.invalidateQueries(cacheKey)
                queryClient.invalidateQueries(assetCacheKey)
                queryClient.invalidateQueries(portfolioCacheKey)
                queryClient.invalidateQueries(statsCacheKey)
            }
        })
    }

    const useDeleteTrade = () => {
        return useMutation(deleteTrade, {
            onSuccess: (d, v) => {
                console.log('invalidating queries after delete')
                console.log('D', d, ' V', v)
                queryClient.invalidateQueries(cacheKey)

                queryClient.refetchQueries(assetCacheKey)

                queryClient.invalidateQueries(assetCacheKey)
                queryClient.invalidateQueries(portfolioCacheKey)
                queryClient.invalidateQueries(statsCacheKey)

            }
        })
    }
    return {
        useAllTrades,
        useAddTrade,
        useDeleteTrade
    }

}

