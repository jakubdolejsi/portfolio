import config from "../utils/config";
import {AssetWithChart, IAsset} from "../pages/auth/AssetPage";
import axios from "axios";


export const fetchAllAssets = async () => {

    return axios.get(`${config.address}/api/v2/asset`)
        .then(response => {
            const assets: IAsset[] = response.data.body
            return assets
        })
        .catch(err => {
            console.log('err', err)
            return [] as IAsset[]
        })
}


export const fetchAllAssetsWithChart = async () => {

    return axios.get(`${config.address}/api/v2/asset/chart`)
        .then(response => {
            const assets: AssetWithChart[] = response.data.body
            return assets
        })
        .catch(err => {
            console.log('err', err)
            return [] as AssetWithChart[]
        })
}


export const fetchAssetByTicker = async (ticker: string) => {

}



