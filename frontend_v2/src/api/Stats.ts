import {PortfolioCurrency} from "../pages/auth/PortfolioPage";
import axios from "axios";
import config from "../utils/config";


export interface StatsItem {
    name: string,
    value: number
}


export interface MultipleStatsItems {
    name: string,
    values: StatsItem[]
}


interface Statistics {
    assetsCount: number,
    tradesCount: number,
    totalBalanceInUSD: number,
    currencies: PortfolioCurrency[],

    theMostExpensiveAsset: StatsItem,
    theLeastExpensiveAsset: StatsItem,

    theMostTradedAsset: StatsItem,

    assetWithMostShares: StatsItem,

    theMostTradedSector: MultipleStatsItems,
    theMostTradedCountry: MultipleStatsItems,

    performance: number
}

export interface PortfolioStatistics extends Statistics {
    name: string
}

export interface ConcreteStat {
    stats: PortfolioStatistics[]
}


export interface GroupedTrade {
    price: number,
    amount: number,
    month: string
}

export interface GeneralStatistics extends Statistics {
    concreteStats: ConcreteStat,
    trades: GroupedTrade[]
}

export interface PerformanceItem {
    date: string,
    value: number
}


export interface Performance {
    name: string,
    performance: PerformanceItem[]
}

interface Allocation {
    ticker: string, // ticker of asset
    weight: number // asset's wight
}

export interface Allocations {
    name: string, // the name of certain allocation (e.g. 'US' if the allocation type is Sector)
    values: Allocation[],
    sum: number
}

export type CorrelationItem = number[]

export interface ICorrelation {
    labels: string[],
    data: CorrelationItem[],
    history: any[]
}

export interface VolatilityItem {
    date: string,
    value: number,

}

export interface Volatility {
    quote: string,
    volatility: VolatilityItem[],
    history: VolatilityItem[],
}


export const fetchAllStatistics = async (): Promise<GeneralStatistics> => {
    const response = await axios.get(`${config.address}/api/v2/stats`)
    return response.data.body
}

export const getPortfolioPerformance = async (): Promise<Performance> => {
    const response = await axios.get(`${config.address}/api/v2/stats/performance/Default`)
    return response.data.body

}


export const GetQuotesPerformance = async (portfolioName: string, quotes: string[]): Promise<Performance> => {
    const response = await axios.post(`${config.address}/api/v2/stats/performance/${portfolioName}/asset`, {
        quotes
    })
    return response.data.body
}

export const GetQuotesPerformanceWithoutInflow = async (portfolioName: string, quotes: string[]): Promise<Performance> => {
    const response = await axios.post(`${config.address}/api/v2/stats/performance/${portfolioName}/nominal/asset`, {
        quotes
    })
    return response.data.body
}

export const GetPortfolioSectorAllocation = async (portfolioName: string): Promise<Allocations[]> => {
    const response = await axios.get(`${config.address}/api/v2/stats/allocation/${portfolioName}/sector`)
    return response.data.body
}

export const GetPortfolioCountryAllocation = async (portfolioName: string): Promise<Allocations[]> => {
    const response = await axios.get(`${config.address}/api/v2/stats/allocation/${portfolioName}/country`)
    return response.data.body
}

export const GetPortfolioIndustryAllocation = async (portfolioName: string): Promise<Allocations[]> => {
    const response = await axios.get(`${config.address}/api/v2/stats/allocation/${portfolioName}/industry`)
    return response.data.body
}

export const GetPortfolioAssetTypeAllocation = async (portfolioName: string): Promise<Allocations[]> => {
    const response = await axios.get(`${config.address}/api/v2/stats/allocation/${portfolioName}/type`)
    return response.data.body
}

export const GetPortfolioAssetAllocation = async (portfolioName: string): Promise<Allocations[]> => {
    const response = await axios.get(`${config.address}/api/v2/stats/allocation/${portfolioName}/asset`)
    return response.data.body
}


export const GetComparableAssets = async (portfolioName: string, quotes: string[]): Promise<Allocations[]> => {
    const response = await axios.post(`${config.address}/api/v2/stats/performance/${portfolioName}/asset/compare`, {
        quotes
    })
    return response.data.body
}

export const GetComparableAssetsWithoutCashInflow = async (portfolioName: string, quotes: string[]): Promise<Allocations[]> => {
    const response = await axios.post(`${config.address}/api/v2/stats/performance/${portfolioName}/asset/nominal/compare`, {
        quotes
    })
    return response.data.body
}

export const GetQuotesCompareWithBenchmark = async (portfolioName: string, quotes: string[], benchmark: string): Promise<Allocations[]> => {
    const response = await axios.post(`${config.address}/api/v2/stats/benchmark/${portfolioName}`, {
        quotes: quotes,
        benchmark: benchmark
    })
    return response.data.body
}

export const GetQuotesPriceCompareWithBenchmark = async (portfolioName: string, quotes: string[], benchmark: string): Promise<Allocations[]> => {
    const response = await axios.post(`${config.address}/api/v2/stats/benchmark/${portfolioName}/price`, {
        quotes: quotes,
        benchmark: benchmark
    })
    return response.data.body
}

export const GetQuotesCorrelation = async (portfolioName: string, quotes: string[]): Promise<ICorrelation> => {
    const response = await axios.post(`${config.address}/api/v2/stats/correlation/${portfolioName}`, {
        quotes: quotes,
    })
    return response.data.body
}

export const GetQuoteVolatility = async (portfolioName: string, quote: string, range: number, period: number, method: string): Promise<Volatility> => {
    const response = await axios.post(`${config.address}/api/v2/stats/volatility/${portfolioName}`, {
        quote,
        period,
        method,
        range,
    })
    return response.data.body
}

