import axios from "axios";
import config from "../utils/config";
import {IPortfolioModel} from "../pages/auth/PortfolioPage";


export interface NewPortfolio {
    portfolioName: string,
    currency: string
}

export interface DeletePortfolio {
    deletedPortfolioName: string,
    newPortfolioName?: string
}

export const fetchAllPortfolioWithHistory = async (): Promise<IPortfolioModel[]> => {
    const response = await axios.get(`${config.address}/api/v2/portfolio/history`)
    return response.data.body
}

export const fetchAllPortfolioAPI = async (): Promise<IPortfolioModel[]> => {
    const response = await axios.get(`${config.address}/api/v2/portfolio`)
    return response.data.body
}

export const addPortfolio = async (portfolio: NewPortfolio) => {
    const response = await axios.post(`${config.address}/api/v2/portfolio`, {
        ...portfolio
    })
    return response.data.body
}

export const deletePortfolio = async (portfolio: DeletePortfolio) => {
    const transferredPortfolio = portfolio.newPortfolioName ?? 'Default'
    const response = await axios.delete(`${config.address}/api/v2/portfolio/${portfolio.deletedPortfolioName}/${transferredPortfolio}`)
    return response.data.body
}


