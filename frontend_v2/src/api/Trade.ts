import axios from "axios";
import config from "../utils/config";
import {ITrade} from "../pages/auth/TradePage";
import {INewTrade} from "../components/modal/NewTradeModal";


export const fetchAllTrades = async () => {
    return axios.get(`${config.address}/api/v2/trade`)
        .then(response => {
            const tradeArr: ITrade[] = []
            response.data.body.forEach((trade: any) => {
                tradeArr.push({
                    id: trade._id,
                    name: trade.asset.name,
                    ticker: trade.asset.type.ticker,
                    amount: parseInt(trade.amount),
                    date: (trade.executionDate),
                    pricePerOne: parseInt(trade.pricePerAsset),
                    totalPrice: parseInt(trade.overallPrice),
                    type: trade.asset.type.name
                })
            })
            return tradeArr
        })
        .catch(err => {
            console.log('err', err)
            return [] as ITrade[]
        })
}

export const deleteTrade = async (id: string) => {
    axios.delete(`${config.address}/api/v2/trade/${id}`)
        .then(response => {
            //setTrades(allTrades.filter(t => t.id !== id))
            const removed: ITrade = response.data.body
            return removed
        })
        .catch(err => {
            console.log('err', err)
            return {} as ITrade
        })
}


export const addTrade = async (trade: INewTrade) => {
    return axios.post(`${config.address}/api/v2/trade`, {
        ...trade
    })
        .then(response => {
            console.log('new trade', response.data.body)
            return response.data.body
        })
        .catch(err => {
            console.log('err', err)
            return {} as ITrade
        })
}

