import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter} from "react-router-dom";
import Auth0ProviderWithHistory from "./auth/providerWithHistory";
import {Provider} from "react-redux";
import store from "./store/store";
import {QueryClient, QueryClientProvider} from "react-query";
import {ReactQueryDevtools} from 'react-query/devtools'

// import * as dotenv from "dotenv";
// dotenv.config();

// Load from env
const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            staleTime: 1000 * 60 * 3, // 3 minutes
            cacheTime: 1000 * 60 * 5 // 5 minutes
        },
    }
})

ReactDOM.render(
    <BrowserRouter>
        <QueryClientProvider client={queryClient}>
            <Provider store={store}>
                <Auth0ProviderWithHistory>
                    <App/>
                </Auth0ProviderWithHistory>
            </Provider>
            <ReactQueryDevtools initialIsOpen={false}/>
        </QueryClientProvider>
    </BrowserRouter>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
