# --------------------------------------------------------------------
# Author: Jakub Dolejsi
#
# Automate docker-compose service
# --------------------------------------------------------------------

# build and run development version
dev:
	docker-compose -f docker-compose.dev.yml up --build --remove-orphans

# build and run production version
prod:
	docker-compose -f docker-compose.prod.yml up --build --remove-orphans

# build development version
dev-build:
	docker-compose -f docker-compose.dev.yml build

# run development version
dev-run:
	docker-compose -f docker-compose.dev.yml up

# build production version
prod-build:
	docker-compose -f docker-compose.prod.yml build --no-cache

# run production version
prod-run:
	docker-compose -f docker-compose.prod.yml up

# clean system
prune:
	docker system prune
	docker volume prune