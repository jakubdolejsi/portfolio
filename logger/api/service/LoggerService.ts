import * as fs from 'fs';


export interface ILogs {
    message: string,
    level: string,
    timestamp: string
}

const RemoveNonASCI = (str: string) => {
    return str.slice(5, -5)
}

const logDir = 'logs'
const logFile = 'all.log'

const CollectLogs = (logs: ILogs) => {
    logs.message = RemoveNonASCI(logs.message)
    logs.level = RemoveNonASCI(logs.level)
    const message = `${logs.timestamp} (${logs.level}): ${logs.message}\n`

    fs.appendFile(`./${logDir}/${logFile}`, message, err => {
        if (err) {
            console.log('!!!!!!!!!!!!!!')
            console.log('cannot write logs')
            console.log('!!!!!!!!!!!!!!')
        }
    })
}


export const LoggerService = {
    CollectLogs,
    RemoveNonASCI
}
