import {NextFunction, Request, Response} from "express";
import {Success} from "../response/response";
import {LoggerService} from "../service/LoggerService";


export type IController = (req: Request, res: Response, next: NextFunction) => Promise<Response>


const ProcessLogs: IController = async (req, res, next) => {
    const logs = req.body

    LoggerService.CollectLogs(logs)
    return Success(res, 'Logged')
}

export {
    ProcessLogs
}
