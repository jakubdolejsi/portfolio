import express from 'express';
import cors from 'cors';
import {ProcessLogs} from "./api/controller/LoggerController";

const app = express();
const port = 3002;

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use('/collect', ProcessLogs)

app.listen(port, () => {
    console.log(`Timezones by location application is running on port ${port}.`);
});
